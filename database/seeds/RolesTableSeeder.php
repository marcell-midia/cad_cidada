<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'master',
            'level' => 20
        ]);
        Role::create([
            'name' => 'admin',
            'level' => 15
        ]);
        Role::create([
            'name' => 'gestor',
            'level' => 10
        ]);
        Role::create([
            'name' => 'atendente',
            'level' => 5
        ]);

    }
}
