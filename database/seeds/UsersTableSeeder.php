<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrador',
            'cpf' => '99999999999',
            'email' => 'admin@master.com',
            'telefone_1' => '96991200000',
            'ativo' => true,
            'role_id' => Role::where('name', 'master')->first()->id,
            'password' => bcrypt('123321')
        ]);
    }
}
