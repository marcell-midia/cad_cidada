<?php

use Illuminate\Database\Seeder;
use App\Cartao;

class CartoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $x;
        for($x=1;$x<=21450;$x++){
            Cartao::create(['id'=>$x]);
        }
    }
}
