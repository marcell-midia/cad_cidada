<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCidadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cadunico2020', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cod_familiar', 12)->nullable();
            $table->string('nome_benef_titular', 100)->nullable();
            $table->string('num_nis_titular', 11)->nullable();
            $table->string('num_cpf_titular', 11)->nullable();
            $table->date('data_nascimento')->nullable();
            $table->string('nome_conjuge', 100)->nullable();
            $table->string('num_cpf_conjuge', 11)->nullable();
            $table->string('cep', 8)->nullable();
            $table->string('cod_ibge')->nullable();
            $table->string('municipio', 80)->nullable();
            $table->string('logradouro_bairro', 80)->nullable();
            $table->string('tipo_logradouro', 80)->nullable();
            $table->string('logradouro', 80)->nullable();
            $table->string('complemento', 15)->nullable();
            $table->string('telefone_contato', 14)->nullable();
            $table->text('busca_rel_mp_prodap')->nullable();
            $table->boolean('apto_cartao')->nullable();
            $table->string('escola')->nullable();
            $table->string('end_escola')->nullable();
            $table->date('cronograma')->nullable();
            $table->string('turno')->nullable();
            $table->string('foto')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cidadas');
    }
}
