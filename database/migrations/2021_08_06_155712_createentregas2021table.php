<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Createentregas2021table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entregas2021', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cadunico2020_id')->nullable();
            $table->foreign('cadunico2020_id')->references('id')->on('cadunico2020');
            $table->string('cartao_id');
            $table->boolean('duplicidade')->nullable();
            $table->unsignedBigInteger('local_id')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('local_id')->references('id')->on('local');
            $table->date('data_entrega');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entregas2021');
    }
}
