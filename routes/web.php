<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\DashboardController;

Auth::routes();


Route::get('/suspenso', function () {
    return abort(503);
})->name('suspenso');

Route::group(['prefix' => 'administrativo', 'middleware' => ['auth','active']], function(){
    Route::get('/', 'DashboardController@dashboard')->name('dashboard');

    Route::group(['prefix' => 'usuarios','middleware' =>'role:master,admin'], function(){
        Route::get('/', 'UsuariosController@index')->name('usuarios.index');
        Route::get('/create', 'UsuariosController@create')->name('usuarios.create');
        Route::post('/store', 'UsuariosController@store')->name('usuarios.store');
        Route::get('/edit/{id}', 'UsuariosController@edit')->name('usuarios.edit');
        Route::get('/resetarsenha/{id}', 'UsuariosController@resetarsenha')->name('usuarios.resetarsenha');
        Route::put('/update/{id}', 'UsuariosController@update')->name('usuarios.update');
        //Route::get('/destroy/{id}', 'UsuariosController@destroy')->name('usuarios.destroy');
    });

    Route::get('/cadastros', 'CidadasController@index')->name('cidadas.index');
    Route::get('/cadastros/show/{id}', 'CidadasController@show')->name('cidadas.show');
    Route::group(['prefix' => 'cadastros', 'middleware' =>'role:master,admin,corretor'], function(){
        Route::get('/create', 'CidadasController@create')->name('cidadas.create');
        Route::post('/store', 'CidadasController@store')->name('cidadas.store');
        Route::get('/edit/{id}', 'CidadasController@edit')->name('cidadas.edit');
        Route::put('/update/{id}', 'CidadasController@update')->name('cidadas.update');
        //Route::get('/destroy/{id}', 'CidadasController@destroy')->name('cidadas.destroy');
    });

        Route::get('/entregas', 'EntregasController@index')->name('entregas.index');
        Route::get('/entregas/show/{id}', 'EntregasController@show')->name('entregas.show');
        Route::group(['prefix' => 'entregas/{entrega_id}/anexos'], function () {
            Route::get('/', ['as' => 'anexo.get', 'uses' => 'AnexoController@getAnexos']);
            Route::post('/salvar', ['as' => 'anexo.criar', 'uses' => 'AnexoController@salvarAnexos']);
            Route::get('/{id}/exibir', ['as' => 'anexo.exibir', 'uses' => 'AnexoController@exibirAnexo']);
            Route::get('/{id}/deletar', ['as' => 'anexo.deletar', 'uses' => 'AnexoController@deletarAnexo']);
        });
    Route::group(['prefix' => 'entregas', 'middleware' => 'role:master,admin,atendente,corretor'], function(){
        //Route::get('/create', 'EntregasController@create')->name('entregas.create');
        //Route::post('/store', 'EntregasController@store')->name('entregas.store');
        //Route::get('/edit/{id}', 'EntregasController@edit')->name('entregas.edit')->middleware('owner');
        //Route::put('/update/{id}', 'EntregasController@update')->name('entregas.update')->middleware('owner');
        //Route::get('/destroy/{id}', 'EntregasController@destroy')->name('entregas.destroy');
    });
        Route::get('/entregas21', 'Entregas21Controller@index')->name('entregas21.index');
        Route::get('/entregas21/show/{id}', 'Entregas21Controller@show')->name('entregas21.show');
    Route::group(['prefix' => 'entregas21', 'middleware' => 'role:master,admin,atendente,corretor'], function(){
        Route::get('/create', 'Entregas21Controller@create')->name('entregas21.create');
        Route::post('/store', 'Entregas21Controller@store')->name('entregas21.store');
        Route::get('/edit/{id}', 'Entregas21Controller@edit')->name('entregas21.edit')->middleware('owner');
        Route::put('/update/{id}', 'Entregas21Controller@update')->name('entregas21.update')->middleware('owner');
        //Route::get('/destroy/{id}', 'EntregasController@destroy')->name('entregas.destroy');
    });
        Route::get('/alterarsenha', 'UsuariosController@alterarSenha')->name('alterarsenha');
        Route::post('/alterarsenha/confirmar', 'UsuariosController@confirmarSenha')->name('confirmarsenha');
});
    Route::get('/', 'ConsultaController@index')->name('consulta.index');
    Route::post('/resultado', 'ConsultaController@resultado')->name('consulta.consulta');
    Route::get('/estabelecimentos', 'CredenciadosController@lista')->name('credenciados.lista');
