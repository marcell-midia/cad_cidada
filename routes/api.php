<?php

use Illuminate\Http\Request;
use App\Http\Resources\CidadaResource;
use App\Cidada;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('pessoa', function(Request $request){
    $cidada = Cidada::where('num_cpf_titular', $request->cpf)->first();
    $res = ['id' => $cidada->id, 'text' => $cidada->nome_benef_titular . ' ' . '('.  $cidada->estado() . ')', 'disabled' => ! $cidada->apto()];
    return response()->json(['results' => [$res]]);
})->name('pessoaJson');
