<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Entrega extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $guarded = ['_token','foto'];
    protected $dates = ['data_entrega'];

    public function recipiente(){
        return $this->belongsTo('App\Cidada','cidada_id');
    }

    public function cartao(){
        return $this->belongsTo('App\Cartao');
    }

    public function creator(){
        return $this->belongsTo('App\User', 'created_by');
    }
    public function updater(){
        return $this->belongsTo('App\User', 'updated_by');
    }
    public function local(){
        return $this->belongsTo('App\Local');
    }
}
