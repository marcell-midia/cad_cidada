<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartao extends Model
{
    protected $table='cartoes';
    public $timestamps = false;


    public function Formatado(){
        return str_pad($this->id,5,0,0);
    }
}
