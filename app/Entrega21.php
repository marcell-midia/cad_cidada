<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Entrega21 extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table = 'entregas2021';
    protected $guarded = ['_token','cartao_id'];
    protected $dates = ['data_entrega'];

    public function recipiente(){
        return $this->belongsTo('App\Cidada','cadunico2020_id');
    }

    /*
    public function cartao(){
        return $this->belongsTo('App\Cartao');
    }
    */

    public function creator(){
        return $this->belongsTo('App\User', 'created_by');
    }
    public function updater(){
        return $this->belongsTo('App\User', 'updated_by');
    }
    public function local(){
        return $this->belongsTo('App\Local');
    }
    public function cartao_formatado(){
        return str_pad($this->cartao_id,5,0,0);
    }
}
