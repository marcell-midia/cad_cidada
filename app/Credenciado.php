<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credenciado extends Model
{
    protected $table = 'credenciados';
}
