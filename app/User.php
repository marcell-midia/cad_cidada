<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Artesaos\Defender\Traits\HasDefender;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;
use App\Local;

class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'cpf','celular', 'session_id', 'first_access', 'last_acess', 'active', 'created_by', 'updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtoupper($value);
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = mb_strtolower($value);
    }

    public function local() {
        return $this->belongsTo('App\Local');
      }

    public function role(){
        return $this->belongsTo('App\Role');
    }
    public function has_role($roles){
        return in_array($this->role->name, $roles);
    }
    public function has_role_level($required_level){
        return ($this->role->level >= $required_level);
    }
    public function is_admin_or_owner($entrega){
        if($this->has_role(['atendente','gestor'])){
            if($entrega){
                return ($entrega->created_by == $this->id);
            }else{
                    return true;
            }
        }else{
                return true;
        }
    }
}
