<?php
namespace App\Helpers;
class dateHelper{
    static function getMes($mes) {
    $meses = [
    'Jan' => 'Janeiro',
    'Feb' => 'Fevereiro',
    'Mar' => 'Marco',
    'Apr' => 'Abril',
    'May' => 'Maio',
    'Jun' => 'Junho',
    'Jul' => 'Julho',
    'Aug' => 'Agosto',
    'Nov' => 'Novembro',
    'Sep' => 'Setembro',
    'Oct' => 'Outubro',
    'Dec' => 'Dezembro'
    ];
        return isset($meses[$mes]) ? $meses[$mes] : false;
    }
}
