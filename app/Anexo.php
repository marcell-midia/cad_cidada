<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anexo extends Model
{
    public function setDescricaoAttribute($value)
    {
        $this->attributes['descricao'] = $value ? mb_strtoupper($value) : null;
    }

    public function scopeSearch($query, $search = null)
    {
        return $query->where('descricao', 'like', '%' . mb_strtoupper($search) . '%');
    }

    public function entrega21(){
        return $this->belongsTo('App/Entrega21','entrega21_id');
    }
}
