<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use OwenIt\Auditing\Contracts\Auditable;
use App\Entrega;
use App\Entrega21;

class Cidada extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table = 'cadunico2020';
    protected $filepath= '/fotos/';
    protected $dates = ['data_nascimento', 'cronograma'];
    protected $fillable = [
        'nome_benef_titular',
        'data_nascimento',
        'nome_conjuge',
        'num_cpf_conjuge',
        'num_cpf_titular',
        'cod_familiar',
        'num_nis_titular',
        'data_nascimento',
        'cep',
        'cod_ibge',
        'municipio',
        'logradouro_bairro',
        'tipo_logradouro',
        'logradouro',
        'complemento',
        'telefone_contato',
        'busca_rel_mp_prodap',
        'apto_cartao',
        'escola',
        'end_escola',
        'cronograma',
        'turno',
        'foto',
    ];


    //apenas onde nome nao nulo
    protected static function boot(){
      parent::boot();
      static::addGlobalScope('nome', function (Builder $builder) {
          $builder->whereNotNull('nome_benef_titular');
          $builder->orderBy('nome_benef_titular');
      });
    }

    public function scopeApto($query){
        $query->where('apto_cartao',true);
    }


    public function entregas(){
        return $this->hasMany(Entrega::class);
    }
    public function entregas21(){
        return $this->hasMany(Entrega21::class,'cadunico2020_id');
    }

    public function endereco(){
        return $this->municipio . ', ' . $this->logradouro_bairro . '. ' . $this->tipo_logradouro . ' ' . $this->logradouro . ' ' . $this->complento;
    }

  public function mapsURL(){
      $pesquisa = str_replace(' ','+',$this->endereco());
      return 'https://www.google.com.br/maps/search/' . $pesquisa;
  }
  public function mapsEscola(){
      $escola = str_replace(' ','+',$this->escola);
      $municipio = str_replace(' ','+', $this->municipio);
      return 'https://www.google.com.br/maps/search/' . $municipio . '+' . $escola;
  }
  public function genFilename($foto){
      return $this->id . '_foto.' . $foto->extension();
  }
  public function foto(){
      return $this->filepath . $this->foto;
  }

  public function CPFValido(){
    $cpf = $this->num_cpf_titular;
     
    $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
    if (strlen($cpf) != 11) {
        return false;
    }
    if (preg_match('/(\d)\1{10}/', $cpf)) {
        return false;
    }
    for ($t = 9; $t < 11; $t++) {
        for ($d = 0, $c = 0; $c < $t; $c++) {
            $d += $cpf{$c} * (($t + 1) - $c);
        }
        $d = ((10 * $d) % 11) % 10;
        if ($cpf{$c} != $d) {
            return false;
        }
    }
    return true;
    }  

  public function apto(){
      return ($this->apto_cartao && $this->entregas21->count() < 1 && $this->bolsa_familia != true);
     }
  public function estado(){
      if(! $this->apto_cartao){
          return 'Não atende os requisitos para receber o cartão';
      }
      if($this->bolsa_familia){
          return 'Não disponível para os beneficiários do programa Bolsa Familía';
      }
      if($this->entregas21->count() == 0){
          return 'Primeira Entrega';
      }
          return 'Entrega realizada';
  }
}
