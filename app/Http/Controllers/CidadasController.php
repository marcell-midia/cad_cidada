<?php

namespace App\Http\Controllers;

use App\Cidada;
use Illuminate\Http\Request;
use App\Http\Requests\CidadaStoreRequest;

class CidadasController extends Controller
{
    protected $titleController = 'Cadastros';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = !empty(request()->get('per_page')) ? request()->get('per_page') : 10;
        $meta = ['title' => $this->titleController, 'action' => 'Registros'];
        if($request->search){
            if($request->search == "=entregue"){
                $records = Cidada::Has('entregas21')->paginate($perPage);
            }elseif($request->search == "=pendente"){
                $records = Cidada::Has('entregas21', '<', 1)->where('apto_cartao',true)->where('bolsa_familia',false)->paginate($perPage);
            }else{
            $records = Cidada::whereLike(['nome_benef_titular','num_nis_titular','num_cpf_titular','cod_familiar','escola'], mb_strtoupper($request->search))->paginate($perPage);
            }
        }else{
            $records = Cidada::paginate($perPage);
        }
        return view('painel.cidadas.index', compact('meta', 'records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = ['title' => $this->titleController, 'action' => 'Cadastrar'];
        return view('painel.cidadas.create', compact('meta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CidadaStoreRequest $request)
    {
        $cidadas = new Cidada();
        $cidadas->fill($request->post());
        if($cidadas->save()){
        if($request->hasFile('foto')){
            $foto = $request->foto->storeAs('public/fotos', $cidadas->genFilename($request->foto));
            $cidadas->foto = $foto;
            $cidadas->save();
        }
            return redirect()->route('cidadas.index')->with(['messagem' => 'Dados cadastrado com sucesso']);
        }else{
            return redirect()->route('cidadas.index')->with(['messagem-erro' => 'Falha ao cadastrar os dados']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $meta = ['title' => $this->titleController, 'action' => 'Cadastrar'];
        $record = Cidada::find($id);
        return view('painel.cidadas.show', compact('meta','record'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meta = ['title' => $this->titleController, 'action' => 'Cadastrar'];
        $record = Cidada::find($id);
        return view('painel.cidadas.edit', compact('meta','record'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CidadaStoreRequest $request, $id)
    {
        $cidadas = Cidada::find($id);
        $cidadas->fill($request->post());
        $cidadas->updated_by = auth()->user()->id;
        if($cidadas->save()){
        if($request->hasFile('foto')){
            $foto = $request->foto->storeAs('public/fotos', $cidadas->genFilename($request->foto));
            $cidadas->foto = $foto;
            $cidadas->save();
        }
            return redirect()->route('cidadas.index');
        }else{
        return redirect()->back()->with(['messagem-erro' => 'Ops! Erro ao atualizar as informações dos dados.']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Cidada::find($id);
            if ($record->delete()) {
                request()->session()->flash('messagem', 'Feito! dados deletado com sucesso.');
                return redirect()->route('cidadas.index');
            }else{
                request()->session()->flash('messagem-erro', 'Erro ao remover.');
            }

    }
}


