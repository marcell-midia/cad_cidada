<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entrega21;
use App\Cidada;
use App\Anexo;
use App\Helpers\mimeHelper;

class AnexoController extends Controller
{
    protected $titleController = 'Anexos';
    public function getAnexos($entrega_id){
        $meta = ['title' => $this->titleController, 'action' => 'Anexos'];
        $entrega21 = Entrega21::find($entrega_id);
        $records =  Anexo::where('entrega21_id', $entrega_id)->orderBy('id', 'desc')->get();
        return view('painel.anexos.index', compact('meta','records','entrega21'));
      }
        
    public function salvarAnexos(Request $request, $entrega_id)
    {
    $arquivo = $request->file('arquivo');
    $arquivo_extensao = $arquivo->getMimeType();
    $data = file_get_contents($arquivo);
    
    $novo_anexo = Anexo::where('descricao',$request->descricao)->where('entrega21_id',$request->entrega21_id)->get();
    if(! count($novo_anexo)){
    $novo_anexo = $novo_anexo->first();
    $novo_anexo = new Anexo();
    }
    $novo_anexo->entrega21_id = $entrega_id;
    $novo_anexo->tipo = $arquivo_extensao;
    $novo_anexo->arquivo = base64_encode($data);
    $novo_anexo->descricao = $request->descricao;
    $novo_anexo->created_by = auth()->user()->id;
    $novo_anexo->updated_by = auth()->user()->id;
    if ($novo_anexo->save()) {
        request()->session()->flash('mensagem', 'Anexado com sucesso');
        return redirect()->back();
    }
    return redirect()->back()->with(['mensagem-erro' => 'Ops! Erro ao cadastrar o anexo']);
    }
    
    
    public function exibirAnexo($entrega_id, $id)
    {
    $record = Anexo::where([['id', $id], ['entrega21_id', $entrega_id]])->get();
    
    if (count($record)) {
        $record = $record->first();
        
        $ext = mimeHelper::getExt($record->tipo);
        $tempfile = $record->descricao . '.' . $ext;
        
        header("Content-Type: application/".$ext);
        header('Content-Disposition: attachment; filename="'.$tempfile.'"');
        return 'data://application/'.$ext.';base64,'. $record->arquivo;

    }
    
    return redirect()->back()->with(['messagem-erro' => 'Ops! Arquivo não existe.']);
    }
    
    public function deletarAnexo($entraga_id, $anexo_id)
    {
    $record = Anexo::where('id',$anexo_id)->first();
    
    if ($record) {
        if ($record->delete()) {
        request()->session()->flash('messagem', 'Feito! Anexo deletado com sucesso.');
        return redirect()->back();
            }
            return redirect()->back()->with(['messagem-erro' => 'Ops! Erro ao deletar o anexo na tabela.']);
        }

        return redirect()->back()->with(['messagem-erro' => 'Ops! Arquivo não existe.']);
    }
}
