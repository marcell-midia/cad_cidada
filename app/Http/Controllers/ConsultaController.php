<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cidada;
use Carbon\Carbon;

class ConsultaController extends Controller
{
    public function index(){
        return view('consulta.index');
    }

    public function resultado(Request $request){
            $cadastro = Cidada::where('num_cpf_titular',$request->cpf)->first();
            if(!Empty($cadastro)){
                if(Carbon::createFromFormat('d/m/Y',$request->nascimento)->format('d/m/Y') == $cadastro->data_nascimento->format('d/m/Y')){
                    return view('consulta.consulta')->with(['cadastro' => $cadastro, 'status' => $cadastro->apto() ?: false]);
                }else{
                    return view('consulta.invalido')->with('request',$request);
                }
            }else{
                return view('consulta.invalido')->with('request',$request);
            }
    }
}
