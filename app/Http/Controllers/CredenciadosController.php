<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Credenciado;

class CredenciadosController extends Controller
{
    public function lista(Request $request){
        $records = Credenciado::where('cidade',$request->cidade)->get();
        $cid = Credenciado::all()->groupBy('cidade')->sortBy('cidade');
        $cidades = [];
        foreach($cid as $cidade){
            array_push($cidades,$cidade[0]->cidade);
        }
        return view('credenciados.lista')->with(['records' => $records, 'cidades' => $cidades]);
    }
}
