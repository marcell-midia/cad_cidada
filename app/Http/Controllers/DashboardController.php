<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cidada;
use App\Entrega21;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    public function dashboard(){
        if(! auth()->user()->email_verified_at){
            return redirect()->route('alterarsenha');
        }else{
        
        //total
        $cadastros = Cidada::all();
        $entregas = Entrega21::all();

        //municipio
        $municipio_todos = Cidada::select('id','municipio')->get()->groupBy('municipio');
        $por_municipio = [];

        foreach($municipio_todos as $municipio){
            $municipio->count();
            $entregues = Cidada::where('municipio',$municipio[0]->municipio)->whereHas('entregas21')->count();
            array_push($por_municipio,['municipio' => $municipio[0]->municipio, 'total' => $municipio->count(),'entregues' => $entregues]);
        }

        //data
        $datas = Cidada::whereNotNull('cronograma')->get()->sortBy('cronograma')->groupBy(function($item) {
            return $item->cronograma->format('Y-m-d');
        });
        $por_data = [];

        foreach($datas as $data){
            $entregues = Cidada::where('cronograma',$data[0]->cronograma)->whereHas('entregas')->count();
            array_push($por_data,['data' => $data[0]->cronograma, 'total' => $data->count(), 'entregues' => $entregues]);
        }


        return view('painel.dashboard')->with(['entregas' => $entregas, 'cadastros' => $cadastros, 'por_municipio' => $por_municipio,'por_data' => $por_data]);
        }

    }
}
