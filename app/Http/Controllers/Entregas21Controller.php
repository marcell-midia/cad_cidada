<?php
namespace App\Http\Controllers;

use App\Entrega21;
use App\Cidada;
//use App\Cartao;
use Illuminate\Http\Request;
use App\Http\Requests\EntregaStoreRequest;
use Carbon\Carbon;
use App\Local;

class Entregas21Controller extends Controller
{
    protected $titleController = 'Entregas: 2021';
    //número máximo de cartões por pessoa
    protected $max_entregas = 1;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $perPage = !empty(request()->get('per_page')) ? request()->get('per_page') : 10;
        $meta = ['title' => $this->titleController, 'action' => 'Registros'];
        $qr = Entrega21::select();
        if($request->search){
            $qr->whereHas('recipiente',function($query) use($request){
                $query->whereLike(['nome_benef_titular','num_cpf_titular','cod_familiar'], mb_strtoupper($request->search));
            })->orWhereHas('creator',function($query) use($request){
                $query->whereLike(['name','cpf'],$request->search);
            });
        }
        $records = $qr->orderBy('created_at','desc')->paginate($perPage);

        return view('painel.entregas21.index', compact('meta', 'records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = ['title' => $this->titleController, 'action' => 'Cadastrar'];
        // $recipientes = Cidada::where('apto_cartao',true)->Has('entregas', '<', $this->max_entregas)->get();
        // return view('painel.entregas21.create', compact('meta','recipientes'));
        return view('painel.entregas21.create', compact('meta'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entrega = new Entrega21();
        $entrega->fill($request->post());
        $entrega->created_by = auth()->user()->id;
        $entrega->local_id = auth()->user()->local_id;
        $entrega->cartao_id = $request->cartao_id;
        $entrega->duplicidade = Entrega21::where('cartao_id',$entrega->cartao_id)->exists();
        if($entrega->save()){
            return redirect()->route('anexo.get',$entrega->id)->with(['messagem' => 'Entrega cadastrada com sucesso']);
        }
        return redirect()->back()->with(['messagem-erro' => 'Falha ao cadastrar a entrega. Verifique o número do cartão inserido']);
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $meta = ['title' => $this->titleController, 'action' => 'Cadastrar'];
        $record = Entrega21::find($id);
        $recipientes = Cidada::where('apto_cartao',true)->where(function($query) use($record){
            $query->where(function($subquery) use($record){
                $subquery->whereDoesntHave('Entregas21');
            })->orWhere('id',$record->recipiente->id);
        })->get();
        return view('painel.entregas21.show', compact('meta','record','recipientes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meta = ['title' => $this->titleController, 'action' => 'Cadastrar'];
        $record = Entrega21::find($id);
        return view('painel.entregas21.edit', compact('meta','record'));
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entrega = Entrega21::find($id);
        $entrega->fill($request->post());
        $entrega->duplicidade = false;
        $entrega->cartao_id = $request->cartao_id;
        if(Entrega21::where('cartao_id',$entrega->cartao_id)->exists()){
            $entrega->duplicidade = true;
            if(Entrega21::where('cartao_id',$entrega->cartao_id)->first()->id == $entrega->id){
                $entrega->duplicidade = false;
            }
        }
        $entrega->updated_by = auth()->user()->id;
        if($entrega->save()){
            return redirect()->route('entregas21.index');
        }else{
        return redirect()->back()->with(['messagem-erro' => 'Ops! Erro ao atualizar as informações da entrega. Verifique o número do cartão inserido']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Entrega21::find($id);
        if ($record->delete()) {
            request()->session()->flash('messagem', 'Feito! dados deletado com sucesso.');
        } else request()->session()->flash('messagem-erro', 'Erro ao remover.');

        return redirect()->route('entregas21.index');
    }
}
