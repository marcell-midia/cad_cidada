<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserStoreRequest;
use App\Role;
use App\Local;

class UsuariosController extends Controller
{
    /**
     * @var TitleController
     */
    protected $titleController = 'Usuário';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = !empty(request()->get('per_page')) ? request()->get('per_page') : 10;
        $meta = ['title' => $this->titleController, 'action' => 'Registros'];
        if($request->search){
            $records = User::whereHas('role',function($query){
                $query->where('level','!=',20);
            })->whereLike(['name','cpf','email'],mb_strtoupper($request->search))->orderBy('name','asc')->paginate($perPage);
        }else{
            $records = User::whereHas('role',function($query){
                $query->where('level','!=',20);
            })->orderBy('name','asc')->paginate($perPage);
        }
        return view('painel.usuario.index', compact('meta', 'records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = ['title' => $this->titleController, 'action' => 'Cadastrar'];
        $roles = Role::where('name', '!=', 'master')->orderBy('name', 'asc')->pluck('name', 'id');
        $locais = Local::all();
        return view('painel.usuario.create', compact('meta', 'roles','locais'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $novo_usuario = new User();
        $novo_usuario->name = $request->name;
        $novo_usuario->cpf = $request->cpf;
        $novo_usuario->telefone_1 = $request->telefone_1;
        $novo_usuario->role_id = $request->role_id;
        $novo_usuario->local_id = $request->local_id;
        $novo_usuario->email = $request->email;
        $novo_usuario->password = bcrypt($request->password);
        $novo_usuario->ativo = true;


        if($novo_usuario->save()){


            return redirect()->route('usuarios.index')->with(['messagem' => 'Usuário cadastrado com sucesso']);
        }else{
            return redirect()->route('usuarios.index')->with(['messagem-erro' => 'Falha ao cadastrar o Usuário']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meta = ['title' => $this->titleController, 'action' => 'Cadastrar'];
        $record = User::find($id);
        if($record->has_role_level(20)){
            return redirect()->route('usuarios.index')->with(['messagem-erro' => 'Sem permissão para editar o usuário.']);
        }
        $roles = Role::where('name', '!=', 'master')->orderBy('name', 'asc')->pluck('name', 'id');
        $role = $record->role_id;
        $locais = Local::all();

        return view('painel.usuario.edit', compact('meta', 'record','roles','role','locais'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $alterar_usuario = User::find($id);
        $alterar_usuario->name = $request->name;
        $alterar_usuario->cpf = $request->cpf;
        $alterar_usuario->telefone_1 = $request->telefone_1;
        $alterar_usuario->role_id = $request->role_id;
        $alterar_usuario->local_id = $request->local_id;
        $alterar_usuario->email = $request->email;
        if($request->ativo != $alterar_usuario->ativo){
            $alterar_usuario->ativo = ! $alterar_usuario->ativo;
            #$alterar_usuario->sendStatusEmail();
        }
        if($alterar_usuario->save()){
            return redirect()->route('usuarios.index');
        }else{
        return redirect()->back()->with(['messagem-erro' => 'Ops! Erro ao atualizar as informações do usuário.']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = User::find($id);
        if ($record && !$record->has_role_level(20)) {
            if ($record->delete()) {
                request()->session()->flash('messagem', 'Feito! Usuário deletado com sucesso.');
                return redirect()->route('usuarios.index');
            }
        }else{
            return redirect()->back()->with(['messagem-erro' => 'Ops! Erro ao deletar o usuário.']);
        }
    }

    public function alterarSenha()
    {
        $usuario = User::find(auth()->user()->id);
        return view('painel.usuario.senha')->with('usuario',$usuario);

    }

    public function confirmarSenha(Request $request)
    {
        $usuario = User::find(auth()->user()->id);
        $request->validate([
            'senha1' => 'required',
            'senha2' => 'required|same:senha1',
        ]);
            $usuario->password = bcrypt($request->senha1);
            $usuario->email_verified_at = now();
            $usuario->save();
            return redirect()->route('dashboard')->with(['messagem' => 'Senha alterada com sucesso']);
    }

    public function resetarSenha($id)
    {
        $usuario = User::find($id);
        $usuario->password = bcrypt($usuario->cpf);
        $usuario->email_verified_at = null;
        $usuario->save();
        return redirect()->route('usuarios.index')->with(['messagem' => 'Senha resetada com sucesso']);

    }
}
