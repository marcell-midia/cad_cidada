<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CpfValido;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50|unique:users,name',
            'cpf' => ['required','numeric','unique:users,cpf',new CpfValido()],
            'email' => 'required|email|unique:users,email',
            'telefone_1' => 'required|unique:users,telefone_1',
            'password' => 'required|min:6|max:20|confirmed',
            'password_confirmation' => 'required|min:6|max:20',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nome',
            'cpf' => 'cpf',
            'email' => 'e-mail',
            'contato_1' => 'contato_1',
            'password' => 'senha',
            'password_confirmation' => 'confirmação da senha',
        ];
    }
}
