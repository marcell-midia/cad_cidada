<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CpfValido;

class CidadaStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->has_role_level(5);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'municipio'         => 'required',
            'nome_benef_titular'            => 'required',
            'data_nascimento'               => 'required',
            'num_cpf_titular'               => ['required', new CpfValido()]

        ];
    }

    public function attributes()
    {
        return [
            'municipio'                     => 'Município',
            'nome_benef_titular'            => 'Nome',
            'data_nascimento'               => 'Data de nascimento',
            'num_cpf_titular'               => 'Cpf'
        ];
    }
}
