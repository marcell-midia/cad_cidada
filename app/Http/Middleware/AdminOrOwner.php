<?php

namespace App\Http\Middleware;
use App\Entrega;

use Closure;

class AdminOrOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->has_role(['atendente','gestor'])){
            if($request->id){
                $entrega = Entrega::find($request->id);
                $status = ($entrega->created_by == auth()->user()->id);
            }else{
                    $status =  true;
            }
        }else{
                $status =  true;
        }

        if($status){
            return $next($request);
        }else{
            return abort(403,'Acesso Negado');
        }
    }
}
