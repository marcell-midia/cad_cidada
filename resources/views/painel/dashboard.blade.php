@extends('layouts.panel')

@section('title') Dashboard @endsection
<style>
.progress2 {
  height: 1.5em;
  width: 100%;
  background-color: #eeee;
  position: relative;
}
.progress2:before {
  content: attr(data-label);
  font-size: 0.8em;
  position: absolute;
  text-align: center;
  top: 5px;
  left: 0;
  right: 0;
}
.progress2 .value2 {
  background-color:#ffA500;
  display: inline-block;
  height: 100%;
}
</style>
@section('page-content')
	<div class="row">
			@if(session()->has('messagem-erro'))
                <div class="alert alert-danger">{{ session()->get('messagem-erro') }}</div>
            @endif

            @if(session()->has('messagem'))
                <div class="alert alert-success">{{ session()->get('messagem') }}</div>
            @endif
    </div>
        <h2 class="text-primary text-center">Total de Entregas Realizadas</h2>
        <hr>
	<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 offset-lg-2">
            <div class="card border border-primary bg-light">
                <div class="card-body background-white">
                    <div class="text-left progress2" data-label="{{intval(($entregas->count()/$cadastros->count())*100)}}% ({{$entregas->count()}} de {{$cadastros->count()}}) entregues">
  <span class="value2" style="width:{{intval(($entregas->count()/$cadastros->count())*100)}}%;"></span>
                </div>
                </div>
            </div>
            </div>
        </div>
        <h2 class="text-primary text-center">Entregas Por Município</h2>
        <hr>
	<div class="row">
            @foreach($por_municipio as $municipio)
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
            <div class="card border border-primary bg-light">
                <div class="card-header">
                <h5><a>{{$municipio['municipio']}}</a></h5>
                </div>
                <div class="card-body background-white">
                <div class="text-left progress2" data-label="{{intval(($municipio['entregues']/$municipio['total'])*100)}}% ({{$municipio['entregues']}} de {{$municipio['total']}}) entregues">
  <span class="value2" style="width:{{intval(($municipio['entregues']/$municipio['total'])*100)}}%;"></span>
                </div>
                </div>
            </div>
            </div>
            @endforeach
        </div>
        <h2 class="text-primary text-center">Entregas Por dia Agendado</h2>
        <hr>
	<div class="row">
        @foreach($por_data as $index=>$data)
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card border border-primary bg-light">
                <div class="card-header">
                    <h5><a>{{$index + 1}}º Dia ({{$data['data']->format('d/m/Y')}})</a></h5>
                </div>
                <div class="card-body background-white">
                    <div class="text-left progress2" data-label="{{intval(($data['entregues']/$data['total'])*100)}}% ({{$data['entregues']}} de {{$data['total']}}) entregues">
  <span class="value2" style="width:{{intval(($data['entregues']/$data['total'])*100)}}%;"></span>
                </div>
                </div>
            </div>
            </div>
        @endforeach
        </div>
    </div>
@endsection
