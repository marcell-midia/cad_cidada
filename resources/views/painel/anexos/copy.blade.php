@extends('adminlte::page')

@section('title', auth()->user()->empresa->nome(). ': Empresa')

@section('search-main') @include('includes.administrativo.search') @endsection

@section('page-titles')
    <div class="row page-titles">
        <div class="col-md-12 col-12 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.show', $empresa->id) }}">{{ ucwords(mb_strtolower($empresa->nome_razaosocial)) }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.getanexos', $empresa->id) }}">{{ $meta['title'] }}</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('empresas.getanexos', $empresa->id) }}">{{ $meta['action'] }}</a></li>
            </ol>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row mb-5">
                                <div class="col-sm-12">
                                    @if(session()->has('messagem-erro'))
                                        <div class="alert alert-danger">{{ session()->get('messagem-erro') }}</div>
                                    @endif

                                    @if(session()->has('messagem'))
                                        <div class="alert alert-success">{{ session()->get('messagem') }}</div>
                                    @endif

                                    @if(is_numeric(request()->segment(3)) && is_numeric(request()->segment(5)))
                                        {!! Form::model($record, ['route' => ['empresas.atualizaranexo', $empresa->id, $record->id], 'method' => 'put', 'enctype' => 'multipart/form-data', 'class' => 'form']) !!}
                                    @else
                                        {!! Form::open(['route' => ['empresas.salvaranexos', $empresa->id], 'method' => 'post', 'enctype' => 'multipart/form-data', 'class' => 'form']) !!}
                                    @endif
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                {!! Form::label('descricao', 'Descrição') !!}
                                                {!! Form::text('descricao', old('descricao', (is_numeric(request()->get(3)) && is_numeric(request()->get(5))) ? $record->descricao : null), ['class' => 'form-control', 'maxlength' => '100', 'required' => 'required']) !!}
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                {!! Form::label('descricao', 'Arquivo *') !!} <br>
                                                {!! Form::file('arquivo') !!}
                                            </div>
                                        </div>

                                        @include('layouts.parties.form_buttom')
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12">
                            @if( count($records) > 0 )
                                @php
                                    $pages = (($records->total() % $records->perPage()) > 0) ? (int)($records->total() / $records->perPage()) + 1 : ($records->total() / $records->perPage());
                                    $pag = max(min($pages, $records->currentPage()), 1);
                                    $start = ($pag - 1) * $records->perPage();
                                    $pluralTotal = ( $records->total() <= 1 ) ? '' : 's';
                                @endphp

                                <div class="alert alert-dark mb-5" role="alert">
                                Exibindo {{ min($records->total(), ($start + 1)) }} - {{ min($records->total(), ($start + $records->perPage())) }} de {{ $records->total() }} registro{{ $pluralTotal }} cadastrado{{ $pluralTotal }}
                                </div>

                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Data</th>
                                            <th>Descrição</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($records as $record)
                                        <tr>
                                            <td>{{ $record->created_at }}</td>
                                            <td>{{ $record->descricao }}</td>
                                            <td>
                                                <a class="btn btn-secondary btn-circle" href="{{ route('empresas.editaranexo', [$record->empresa_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-secondary btn-circle" href="{{ route('empresas.exibiranexo', [$record->empresa_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Baixar"><i class="fa fa-download"></i></a>
                                                <a class="btn btn-secondary btn-circle" href="{{ route('empresas.deletaranexo', [$record->empresa_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Deletar"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                @if(request()->get('search'))
                                    <div class="alert alert-info">
                                        Nenhum resultado encontrado com o termo <strong>{{ request()->get('search') }}</strong>.
                                    </div>

                                    <p>
                                        <a class="btn btn-block btn-info" href="{{ route('empresas.index') }}">
                                            <i class="fa fa-reply"></i> Voltar
                                        </a>
                                    </p>
                                @else
                                    <div class="alert alert-info">Não existem registros cadastrados.</div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
