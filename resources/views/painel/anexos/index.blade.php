@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection


@section('page-titles')
	<div class="row page-titles">
	    <div class="col-md-6 col-8 align-self-center">
	        <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
	        <ol class="breadcrumb">
	            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
	            <li class="breadcrumb-item"><a href="{{ route('entregas21.index') }}">Entregas:2021</a></li>
	            <li class="breadcrumb-item active"><a href="{{ route('entregas21.index') }}">{{ $meta['action'] }}</a></li>
	        </ol>
	    </div>
	</div>
@endsection

@section('page-content')
	<div class="row">
			@if(session()->has('messagem-erro'))
                <div class="alert alert-danger">{{ session()->get('messagem-erro') }}</div>
            @endif

            @if(session()->has('messagem'))
                <div class="alert alert-success">{{ session()->get('messagem') }}</div>
            @endif
</div>

	<div class="card bg-light">
	<div class="card-header">
		<h3 class="text-center">ANEXOS DA ENTREGA: {{$entrega21->recipiente->nome_benef_titular}}</h3>
</div>
		<div class="text-center">
			<form method="post" enctype='multipart/form-data' action="{{route('anexo.criar', $entrega21->id)}}">
			@csrf
				<div class="row">
					<div class="form-group col-xs-12 col-sm-5 offset-sm-1">
						<label for="descricao">Arquivo</label>
						<select name="descricao" required class="form-control">
							<option selected disabled>Selecione o arquivo</option>
							<option value="Comprovante de Residência">Comprovante de residência</option>
							<option value="Foto da entrega">Foto da Entrega</option>
							<option value="Cartão Entregue">Cartão Entregue</option>
							<option value="Documento de Identificação">Documento de identificação</option>
</select>
					</div>
					<div class="form-group col-xs-12 col-sm-5">
						<label for="arquivo">Arquivo</label>
						<input required  accept=".jpg,.jpeg,.png,.pdf" type="file" name="arquivo" class="form-control form-control-file">
			<h6 class="text-info">Formatos suportados: png, jpg e pdf</h6>
					</div>
				</div>
				<div class="col-md-4 offset-md-4">
				<input class="btn  btn-success text-center btn-block" type="submit" value="Cadastrar">
				<br>
				</div>
			</form>

</div>
</div>
	@if($records->count())
	<hr>
	<br>
	<div class="card bg-light">
	<div class="card-header">
		<h3 class="text-center">Documentos Cadastrados</h3>
	</div>
		<div class="text-center">
		<table class="table ">
			<thead>
				<tr>
					<th>Data</th>
					<th>Descrição</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

			@foreach($records as $record)
				<tr>
						<td>{{ $record->created_at->format('d/m/Y - H:i') }}</td>
					<td>{{ $record->descricao }}</td>
					<td>
						<a download class="btn btn-secondary btn-circle" href="data:{{$record->tipo}};base64,{{$record->arquivo}}" data-toggle="tooltip" data-placement="bottom" title="Baixar"><i class="fa fa-download"></i></a>
						<a class="btn btn-secondary btn-circle" href="{{ route('anexo.deletar', [$record->entrega21_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Deletar"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
				<div class="col-md-4 offset-md-4">
				<a class="btn  btn-primary text-center btn-block" href="/administrativo/entregas21">Finalizar Entrega</a>
				</div>
		@else

	<row class="text-center">
		<br>
        <hr>
		<h2>Nenhum Anexo Cadastrado</h2>
	</row>
	@endif
	<hr>
</div>
	</div>
	@if(count($records))
		@component('components.documentViewer',['documents' => $records, 'height' => '600'])
		@endcomponent
	@endif
	@endsection

@section('scripts')
@endsection
