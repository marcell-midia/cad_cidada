@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('search-main') @include('includes.administrativo.search') @endsection

@section('page-titles')
	<div class="row page-titles">
	    <div class="col-md-6 col-8 align-self-center">
	        <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
	        <ol class="breadcrumb">
	            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
	            <li class="breadcrumb-item"><a href="{{ route('entregas21.index') }}">{{ $meta['title'] }}</a></li>
	            <li class="breadcrumb-item active"><a href="{{ route('entregas21.index') }}">{{ $meta['action'] }}</a></li>
	        </ol>
	    </div>
        @if(auth()->user()->has_role(['atendente', 'corretor', 'admin','master']))
	    <div class="col-md-6 col-4 align-self-center">
	    	<a href="{{ route('entregas21.create') }}" class="btn pull-right hidden-sm-down btn-info"><i class="mdi mdi-plus-box"></i>Nova Entrega</a>
        @endif
	    </div>
	</div>
@endsection

@section('page-content')
	<div class="row">
			@if(session()->has('messagem-erro'))
                <div class="alert alert-danger">{{ session()->get('messagem-erro') }}</div>
            @endif

            @if(session()->has('messagem'))
                <div class="alert alert-success">{{ session()->get('messagem') }}</div>
            @endif

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="card bg-light">
				<div class="card-body">
					@if( count($records) > 0 )
						@php
						$pages = (($records->total() % $records->perPage()) > 0) ? (int)($records->total() / $records->perPage()) + 1 : ($records->total() / $records->perPage());
						$pag = max(min($pages, $records->currentPage()), 1);
						$start = ($pag - 1) * $records->perPage();
						$pluralTotal = ( $records->total() <= 1 ) ? '' : 's';
						@endphp

		                <div class="alert alert-dark mb-5" role="alert">
		                Exibindo {{ min($records->total(), ($start + 1)) }} - {{ min($records->total(), ($start + $records->perPage())) }} de {{ $records->total() }} registro{{ $pluralTotal }} cadastrado{{ $pluralTotal }}
		                </div>

	                	<table class="table table-bordered table-striped text-left">
							<thead>
								<tr>
									<th>Data da Entrega</th>
									<th>Atendente</th>
									<th>Beneficiário</th>
									<th>CPF</th>
									<th>Código Do Cartão</th>
									<th>Anexos</th>
									<th>Ver</th>
									<th>Editar</th>
								</tr>
							</thead>
							<tbody>
								@foreach($records as $record)
									<tr>
                                        <td>{{ $record->data_entrega->format('d/m/Y') }}</td>
										<td>
                                            @if($record->creator)
                                                <span class="text-info" title="CPF: {{$record->creator->cpf}}" data-toggle="tooltip"> {{$record->creator->name}}</span>
                                            @else
                                                Não especificado
                                            @endif

                                        </td>
										<td>{{$record->recipiente->nome_benef_titular }}</td>
                                        <td><span class="cpf">{{$record->recipiente->num_cpf_titular}}</span></td>
                                        <td>
											@if($record->duplicidade)
											<span data-toggle="tooltip" title="Existe outra entrega com o mesmo código de cartão" class="btn btn-sm btn-danger">{{$record->cartao_formatado()}}</span>
												@else
											<span data-toggle="tooltip" title="" class="btn btn-sm btn-primary">{{$record->cartao_formatado()}}</span>
												@endif
											</td>
										
										<td>
													<a class="btn btn-secondary btn-circle" href="{{ route('anexo.get', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Anexos"><i class="fa fa-paperclip"></i></a>
										</td>
										<td>
													<a class="btn btn-secondary btn-circle" href="{{ route('entregas21.show', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Detalhes"><i class="fa fa-eye"></i></a>
										</td>
										<td>
                                        @if(auth()->user()->is_admin_or_owner($record))
													<a class="btn btn-secondary btn-circle" href="{{ route('entregas21.edit', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-edit"></i></a>
                                                    @else
													<button disabled class="btn btn-secondary btn-circle" href="{{ route('entregas21.edit', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Apenas suas próprias entregas21 podem ser editadas"><i class="fa fa-edit"></i></button>
                                        @endif
										</td>

									</tr>
								@endforeach
							</tbody>
						</table>

						@if( !(request()->get('search')) )
	                  		{{-- <p><a class="btn btn-rounded btn-info" href="{{ route('empresas.index') }}"  style="color:#fff;"><i class="fa fa-reply"></i> Exibir --}}
		                      Exibindo todos os registros</a></p>
                        @else
                              Exibindo resultados para <strong>{{ request()->get('search') }}</strong>
                              <br>
                                    <a href="{{route('entregas21.index')}}"><i class="fa fa-reply"></i>Todos os cadastros</a>
		                @endif
					@else
						@if(request()->get('search'))
							<div class="alert alert-info">
								Nenhum resultado encontrado com o termo <strong>{{ request()->get('search') }}</strong>.
							</div>

							<p>
								{{-- <a class="btn btn-block btn-info" href="{{ route('empresas.index') }}"> --}}
                                    <a href="{{route('entregas21.index')}}"><i class="fa fa-reply"></i>Voltar</a>
								</a>
							</p>
						@else
							<div class="alert alert-info">Não existem registros cadastrados.</div>
							@endif
							@endif
				</div>
                {{$records->appends(request()->except('page'))->links()}}
			</div>		
		</div>
	</div>
	@endsection

@section('scripts')
@endsection
