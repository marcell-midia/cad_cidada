<style>
.preview{
	max-height:300px;
	min-height:300px;
	max-width:260px;
	max-width:260px;
	margin-left:27%;
	object-fit: cover;
}
</style>

<fieldset>
	<div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-lg-6">
      {!! Form::label('cadunico2020_id', 'Beneficiário (a) *') !!}

			@if(isset($record))
				<input required type="hidden" value="{{$record->recipiente->id}}" name="cadunico2020_id">
					<select required disabled class="selectrecipiente form-control" id="selectrecipiente" name="cadunico2020_id">
			@else
				<select required class="selectrecipiente form-control select2" name="cadunico2020_id">
			@endif

			@if(! isset($record))
				<option required disabled selected value="">Digite o CPF do recipiente</option>
			@endif

			@if(isset($record))
				<option required selected value="{{$record->recipiente->id}}">{{$record->recipiente->nome_benef_titular}} (CPF:{{$record->recipiente->num_cpf_titular}} NIS:{{$record->recipiente->num_nis_titular}})</option>
			@endif

			</select>
			<h6 class="text-center text-warning">Esta informação não poderá ser modificada depois</h5>
    </div>
		<div class="form-group col-xs-12 col-sm-12 col-lg-3">
			{!! Form::label('cartao_id', 'Código do Cartão *') !!}
				{{-- @if(! isset($record))
				@endif --}}
				@if(isset($record))
					<input class="form-control" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" type="text" value="{{$record->cartao_id}}" maxlength="10" minlength="5" name="cartao_id">
				@else
					<input class="form-control" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" type="text" maxlength="10" minlength="5" name="cartao_id">
				@endif
    </div>
		<div class="form-group col-xs-12 col-sm-12 col-lg-3">
			{!! Form::label('data_entrega', 'Data de entrega *') !!}
			@if(isset($record))
			{!! Form::date('data_entrega', $record->data_entrega, ['class' => 'form-control', 'maxlength' => '9']) !!}
			@else
			{!! Form::date('data_entrega', old('data_entrega', today()), ['class' => 'form-control', 'maxlength' => '9']) !!}
			@endif
    </div>
		</div>
    @if(isset($record))
		<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-6 offset-lg-3">
    	{!! Form::label('observacao', 'Observações sobre a correção feita') !!}
    	<textarea class="form-control" rows="3" name="observacao">{{$record->observacao ?: ''}}</textarea>
		</div>
  @endif

</fieldset>

<script>
function readURL(input, preview) {
    if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
    $('#' + preview).attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
    }
}
$("#foto").change(function() {
    readURL(this, 'foto_preview');
});
$(document).on('keypress', '.select2-search__field', function () {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});
</script>
