<style>
 .preview{
     max-height:300px;
     min-height:300px;
     max-width:260px;
     max-width:260px;
     margin-left:27%;
     object-fit: cover;
 }
</style>
<fieldset>

    <div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-6 offset-lg-3">
            {!! Form::label('foto', 'Foto do beneficiário(opcional)') !!}
						<input type="file" accept=".png, .jpg, .jpeg" name="foto" id="foto">
                    @if(isset($record))
                        @if($record->foto)
                            <img id="foto_preview" class="preview" src="{{Storage::url($record->foto)}}">
						@else
						<img id="foto_preview" class="preview" src="{{url('img/perfil.png')}}">
						@endif
                    @else
						<img id="foto_preview" class="preview" src="{{url('img/perfil.png')}}">
                    @endif


	</div>
    </div>
	<div class="row">
		<div class="form-group col-xs-12 col-sm-5 col-md-5 col-lg-6 offset-lg-1">
            {!! Form::label('nome_benef_titular', 'Nome *') !!}
            {!! Form::text('nome_benef_titular', old('codigo', null), ['class' => 'form-control', 'maxlength' => '100']) !!}
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4">
			{!! Form::label('telefone_contato', 'Contato *') !!}
            {!! Form::text('telefone_contato', old('telefone_contato', null), ['class' => 'form-control', 'maxlength' => '10']) !!}
		</div>
    </div>
	<div class="row">
		<div class="form-group col-xs-12 col-sm-5 col-md-5 col-lg-3">
			{!! Form::label('num_cpf_titular', 'CPF *') !!}
            @if(isset($record))
            {!! Form::text('num_cpf_titular', old('num_cpf_titular', null), ['class' => 'form-control', 'maxlength' => '14']) !!}
            @else
            {!! Form::text('num_cpf_titular', old('num_cpf_titular', null), ['class' => 'form-control', 'maxlength' => '14']) !!}
            @endif
        </div>
		<div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-3">
            {!! Form::label('cod_familiar', 'Código familiar *') !!}
            @if(isset($record))
            {!! Form::text('cod_familiar', old('cod_familiar', null), ['class' => 'form-control', 'maxlength' => '9']) !!}
            @else
            {!! Form::text('cod_familiar', old('cod_familiar', null), ['class' => 'form-control', 'maxlength' => '9']) !!}
            @endif
        </div>
		<div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-3">
            {!! Form::label('num_nis_titular', 'Número NIS *') !!}
            @if(isset($record))
            {!! Form::text('num_nis_titular', old('num_nis_titular', null), ['class' => 'form-control', 'maxlength' => '11']) !!}
            @else
            {!! Form::text('num_nis_titular', old('num_nis_titular', null), ['class' => 'form-control', 'maxlength' => '11']) !!}
            @endif
		</div>
		<div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2">
			{!! Form::label('data_nascimento', 'Nascimento *') !!}
            @if(isset($record))
                <input type="date" name="data_nascimento" class="form-control" value="{{\Carbon\Carbon::Parse($record->data_nascimento)->format('Y-m-d')}}">
            @else
            {!! Form::date('data_nascimento', old('data_nascimento', null), ['class' => 'form-control', 'maxlength' => '11']) !!}
            @endif
        </div>
	</div>

	<div class="row">
		<div class="form-group col-xs-12 col-sm-5 col-md-5 col-lg-6">
            {!! Form::label('municipio', 'Município *') !!}
            {!! Form::text('municipio', old('municipio', null), ['class' => 'form-control', 'maxlength' => '15']) !!}
        </div>
		<div class="form-group col-xs-12 col-sm-5 col-md-5 col-lg-6">
			{!! Form::label('logradouro_bairro', 'Bairro *') !!}
            {!! Form::text('logradouro_bairro', old('logradouro_bairro', null), ['class' => 'form-control', 'maxlength' => '80']) !!}
		</div>
	</div>

	<div class="row">
		<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
			{!! Form::label('tipo_logradouro', 'Tipo logradouro *') !!}
            {!! Form::text('tipo_logradouro', old('tipo_logradouro', null), ['class' => 'form-control', 'maxlength' => '80']) !!}
        </div>
		<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
			{!! Form::label('logradouro', 'Logradouro *') !!}
            {!! Form::text('logradouro', old('logradouro', null), ['class' => 'form-control', 'maxlength' => '90']) !!}
        </div>
		<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
			{!! Form::label('complemento', 'Complemento *') !!}
            {!! Form::text('complemento', old('complemento', null), ['class' => 'form-control', 'maxlength' => '70']) !!}
        </div>
	</div>

	<div class="row">
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
			{!! Form::label('cep', 'CEP *') !!}
            {!! Form::text('cep', old('cep', null), ['class' => 'form-control', 'maxlength' => '10']) !!}
		</div>

		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
			{!! Form::label('cod_ibge', 'Código IBGE *') !!}
            {!! Form::text('cod_ibge', old('cod_ibge', null), ['class' => 'form-control', 'maxlength' => '9']) !!}
		</div>


		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
			{!! Form::label('nome_conjuge', 'Nome conjuge') !!}
            {!! Form::text('nome_conjuge', old('nome_conjuge', null), ['class' => 'form-control', 'maxlength' => '100']) !!}
		</div>

		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
			{!! Form::label('nu_cpf_conjuge', ' CPF conjuge') !!}
            {!! Form::text('nu_cpf_conjuge', old('num_cpf_conjuge', null), ['class' => 'form-control', 'maxlength' => '100']) !!}
		</div>


		
		
</fieldset>
    <script>
				function readURL(input, preview) {
				if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
				$('#' + preview).attr('src', e.target.result);
				}
				
				reader.readAsDataURL(input.files[0]);
				}
				}
				$("#foto").change(function() {
				readURL(this, 'foto_preview');
				});
    </script>
