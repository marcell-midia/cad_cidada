@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('page-titles')
    <style>
.form-control:disabled{
    background-color:#eee;
}
.form-control{
    color:#222;
    border: 0;
}

    </style>
	<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('cidadas.index') }}">{{ $meta['title'] }}</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('cidadas.create') }}">{{ $meta['action'] }}</a></li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
        	<div class="btn-group pull-right" role="group" aria-label="Basic example">
                <a href="{{ route('cidadas.index') }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar cadastros</a>
            </div>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card bg-light">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @if( $errors->any() )
                                <div class="alert alert-danger">
                                    Preencha corretamente os campos abaixo: <br>
                                    @foreach( $errors->all() as $error )
                                        - {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                            
                            {!! Form::model($record, ['route' => ['cidadas.update', $record->id], 'method' => 'put', 'class' => 'form','enctype'=>'multipart/form-data', 'id'=>'viewform']) !!}
                                @include('painel.cidadas.form')

									
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
@endsection

@section('scripts')
$( document ).ready(function() {
    $("#viewform :input").prop("disabled", true);
});


$('.cnpj').mask('00.000.000/0000-00', {reverse: true});
$('.login').mask('000.000.000-00', {reverse: true});
$(".form").submit(function() {
  $(".login").unmask();
  $(".cnpj").unmask();
});
@endsection
