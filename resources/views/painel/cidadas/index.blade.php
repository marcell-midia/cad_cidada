@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('search-main') @include('includes.administrativo.search') @endsection

@section('page-titles')
	<div class="row page-titles">
	    <div class="col-md-6 col-8 align-self-center">
	        <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
	        <ol class="breadcrumb">
	            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
	            <li class="breadcrumb-item"><a href="{{ route('cidadas.index') }}">{{ $meta['title'] }}</a></li>
	            <li class="breadcrumb-item active"><a href="{{ route('cidadas.index') }}">{{ $meta['action'] }}</a></li>
	        </ol>
	    </div>
        @if(auth()->user()->has_role(['master','admin']))
	    <div class="col-md-6 col-4 align-self-center">
	    	<a href="{{ route('cidadas.create') }}" class="btn pull-right hidden-sm-down btn-info"><i class="mdi mdi-plus-box"></i>Novo Cadastro</a>
	    </div>
        @endif
	</div>
@endsection

@section('page-content')
	<div class="row">
			@if(session()->has('messagem-erro'))
                <div class="alert alert-danger">{{ session()->get('messagem-erro') }}</div>
            @endif

            @if(session()->has('messagem'))
                <div class="alert alert-success">{{ session()->get('messagem') }}</div>
            @endif

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="card bg-light">
				<div class="card-body">
						<div class="control text-center">
							<h4 class="text-secondary">Filtros</h4>
							<a href="{{route('cidadas.index')}} " class="btn text-dark btn-outline-primary">Sem Filtro</a>
							<a href="{{route('cidadas.index') . '?search=%3Dentregue'}} " class="btn text-dark btn-outline-success">Entregues</a>
							<a href="{{route('cidadas.index') . '?search=%3Dpendente'}} " class="btn text-dark btn-outline-warning">Pendentes</a>
							
						<hr>
						</div>
					@if( count($records) > 0 )
						@php
						$pages = (($records->total() % $records->perPage()) > 0) ? (int)($records->total() / $records->perPage()) + 1 : ($records->total() / $records->perPage());
						$pag = max(min($pages, $records->currentPage()), 1);
						$start = ($pag - 1) * $records->perPage();
						$pluralTotal = ( $records->total() <= 1 ) ? '' : 's';
						@endphp

		                <div class="alert alert-dark mb-5" role="alert">
		                Exibindo {{ min($records->total(), ($start + 1)) }} - {{ min($records->total(), ($start + $records->perPage())) }} de {{ $records->total() }} registro{{ $pluralTotal }} cadastrado{{ $pluralTotal }}
		                </div>

	                	<table class="table table-bordered table-striped text-left">
							<thead>
								<tr>
									<th>Nome</th>
									<th>CPF</th>
									<th>Nascimento</th>
									<th>Local p. Entrega</th>
									<th>Aptidão</th>
									<th>Cartões</th>
									<th>Ver</th>
									<th>Editar</th>
								</tr>
							</thead>
							<tbody>
								@foreach($records as $record)
									<tr>
										<td>{{ $record->nome_benef_titular }}</td>
                                        <td>
											@if($record->CPFValido())
											<span class="cpf">{{ $record->num_cpf_titular}}</span>
											@else
											<span data-toggle="tooltip" title="CPF Inválido" class="btn btn-sm btn-danger cpf">{{ $record->num_cpf_titular}}</span>
											@endif
									</td>
										<td>{{ $record->data_nascimento->format('d/m/Y') }}</td>
                                        <td><span class="text-primary" title="{{$record->end_escola}}" data-toggle="tooltip">{{ $record->escola }}</span></td>
										<td>
                                            @if($record->apto())
                                                <span data-toggle="tooltip" title="{{$record->estado()}}" class="btn btn-sm btn-success">
                                                    @if($record->entregas21->count() == 0)
                                                        Apto para entrega
                                                    @endif
                                                </span>
                                            @else
                                                @if($record->entregas21->count() == 1)
                                                <span data-toggle="tooltip" title="{{$record->estado()}}" class="btn btn-sm btn-info">Entrega Realizada</span>
                                                @else
                                                <span data-toggle="tooltip" title="{{$record->estado()}}" class="btn btn-sm btn-danger">Não Atende os Requisitos</span>
                                                @endif
                                            @endif
                                        </td>
                                            <td>
                                        @if(count($record->entregas21))
                                                <div class="button-group">
                                            @foreach($record->entregas21 as $entrega)
                                            @if(auth()->user()->has_role(['admin','master','atendente']))
                                            <a href="{{route('entregas21.edit',$entrega->id)}}" data-toggle="tooltip" title="Entregue em {{$entrega->data_entrega->format('d/m/y')}}" class="btn btn-sm btn-success text-white">{{$entrega->cartao_formatado()}}</a>
                                            @else
                                            <a href="{{route('entregas21.show',$entrega->id)}}" data-toggle="tooltip" title="Entregue em {{$entrega->data_entrega->format('d/m/y')}}" class="btn btn-sm btn-success text-white">{{$entrega->cartao_formatado()}}</a>
                                            @endif
                                        @endforeach
                                                </div>
                                        @else
                                            Nenhum
                                        @endif
                                        </td>
										
										<td>
													<a class="btn btn-secondary btn-circle" href="{{ route('cidadas.show', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Detalhes"><i class="fa fa-eye"></i></a>
                                        </td>
										<td>
                                            @if(auth()->user()->has_role(['master','admin','corretor']))
													<a class="btn btn-secondary btn-circle" href="{{ route('cidadas.edit', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-edit"></i></a>
                                            @else
													<button disabled class="btn btn-secondary btn-circle" href="{{ route('cidadas.edit', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-edit"></i></button>
                                            @endif
										</td>

									</tr>
								@endforeach
							</tbody>
						</table>
						@if( !(request()->get('search')) )
	                  		{{-- <p><a class="btn btn-rounded btn-info" href="{{ route('empresas.index') }}"  style="color:#fff;"><i class="fa fa-reply"></i> Exibir --}}
		                      Exibindo todos os registros</a></p>
                        @else
                              Exibindo resultados para <strong>{{ request()->get('search') }}</strong>
                              <br>
                                    <a href="{{route('cidadas.index')}}"><i class="fa fa-reply"></i>Todos os cadastros</a>
		                @endif
					@else
						@if(request()->get('search'))
							<div class="alert alert-info">
								Nenhum resultado encontrado com o termo <strong>{{ request()->get('search') }}</strong>.
							</div>

							<p>
								{{-- <a class="btn btn-block btn-info" href="{{ route('empresas.index') }}"> --}}
                                    <a href="{{route('cidadas.index')}}"><i class="fa fa-reply"></i>Voltar</a>
								</a>
							</p>
						@else
							<div class="alert alert-info">Não existem registros cadastrados.</div>
							@endif
							@endif
				</div>
                {{$records->appends(request()->except('page'))->links()}}
			</div>		
		</div>
	</div>
	@endsection

