@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('page-titles')
	<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('entregas.index') }}">{{ $meta['title'] }}</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('entregas.create') }}">{{ $meta['action'] }}</a></li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="btn-group pull-right" role="group" aria-label="Basic example">
                <a href="{{ route('entregas.index') }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar entregas</a>
            </div>
        </div>
    </div>
@endsection

@section('page-content')
	<div class="row">
		@if(session()->has('messagem-erro'))
			<div class="alert alert-danger">{{ session()->get('messagem-erro') }}</div>
		@endif

		@if(session()->has('messagem'))
			<div class="alert alert-success">{{ session()->get('messagem') }}</div>
		@endif

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card bg-light">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @if( $errors->any() )
                            <div class="alert alert-danger">
                                Preencha corretamente os campos abaixo: <br>
                                @foreach( $errors->all() as $error )
                                    - {{ $error }}<br>
                                @endforeach
                            </div>
                            @endif
                            {!! Form::open(['route' => 'entregas.store', 'method' => 'post', 'class' => 'form', 'enctype' =>'multipart/form-data']) !!}
                                @include('painel.entregas.form')
                                @include('layouts.parties.form_buttom')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
