<fieldset>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {!! Form::label('nome', 'Perfil *') !!}
            {!! Form::text('nome', old('nome', null), ['class' => 'form-control', 'maxlength' => '100']) !!}
        </div>
    </div>

    {!! Form::hidden('name', old('name', null), ['id' => 'name']) !!}

    @include('painel.perfil.map')
</fieldset>