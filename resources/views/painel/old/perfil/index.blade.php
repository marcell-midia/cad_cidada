@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('search-main') @include('includes.administrativo.search') @endsection

@section('page-titles')
	<div class="row page-titles">
	    <div class="col-md-6 col-8 align-self-center">
	        <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
	        <ol class="breadcrumb">
	            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
	            <li class="breadcrumb-item"><a href="{{ route('perfis.index') }}">{{ $meta['title'] }}</a></li>
	            <li class="breadcrumb-item active"><a href="{{ route('perfis.index') }}">{{ $meta['action'] }}</a></li>
	        </ol>
	    </div>
	    <div class="col-md-6 col-4 align-self-center">
	    	<a href="{{ route('perfis.create') }}" class="btn pull-right hidden-sm-down btn-info"><i class="mdi mdi-plus-circle"></i> Novo perfil</a>
	    </div>
	</div>
@endsection