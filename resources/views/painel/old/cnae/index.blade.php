@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('search-main') @include('includes.administrativo.search') @endsection

@section('page-titles')
	<div class="row page-titles">
	    <div class="col-md-6 col-8 align-self-center">
	        <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
	        <ol class="breadcrumb">
	            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="text-secondary">Dashboard</a></li>
	            <li class="breadcrumb-item"><a href="javascript:void(0);" class="text-secondary">Cadastro</a></li>
	            <li class="breadcrumb-item"><a href="{{ route('cnaes.index') }}" class="text-secondary">{{ $meta['title'] }}</a></li>
	            <li class="breadcrumb-item active"><a href="{{ route('cnaes.index') }}" class="text-secondary">{{ $meta['action'] }}</a></li>
	        </ol>
	    </div>
	    <div class="col-md-6 col-4 align-self-center">
	    	@include('painel.cnae.subnav')
	    </div>
	</div>
@endsection

@section('page-content')
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			@if(session()->has('messagem-erro'))
                <div class="alert alert-danger">{{ session()->get('messagem-erro') }}</div>
            @endif

            @if(session()->has('messagem'))
                <div class="alert alert-success">{{ session()->get('messagem') }}</div>
            @endif

			<div class="card">
				<div class="card-body">
					@if( count($records) > 0 )
						@php
						$pages = (($records->total() % $records->perPage()) > 0) ? (int)($records->total() / $records->perPage()) + 1 : ($records->total() / $records->perPage());
						$pag = max(min($pages, $records->currentPage()), 1);
						$start = ($pag - 1) * $records->perPage();
						$pluralTotal = ( $records->total() <= 1 ) ? '' : 's';
						@endphp

		                <div class="alert alert-dark mb-5" role="alert">
		                Exibindo {{ min($records->total(), ($start + 1)) }} - {{ min($records->total(), ($start + $records->perPage())) }} de {{ $records->total() }} registro{{ $pluralTotal }} cadastrado{{ $pluralTotal }}
		                </div>

	                	<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Códio</th>
									<th>Descrição</th>
									<th>Ativo</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($records as $record)
									<tr>
										<td>{{ $record->codigo }}</td>
										<td>{{ $record->nome }}</td>
										<td>@if($record->ativo) <span class="badge badge-success">ATIVO</span> @else <span class="badge badge-light">INATIVO</span> @endif</td>
										<td>
											@if($record->id != 1)
												@shield('cnaes.show')
													<a class="btn btn-secondary btn-circle" data-tooltip="tooltip" data-placement="bottom" data-toggle="modal" data-target="#show<?=$record->id?>" title="Visualizar"><i class="fa fa-eye"></i></a>
														<div class="modal fade" id="show<?=$record->id?>">
															<div class="modal-dialog" role="document">
																<div class="modal-content">
																	<div class="modal-header">
																		<h5 class="modal-title" id="TituloModalLongoExemplo">Visualizar</h5>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
																		<span aria-hidden="true">&times;</span>
																	</button>
																	</div>
																	<div class="modal-body">
																		<div class="row">
																			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
																				<sub>Código</sub>
																				<p>{{ $record->codigo }}</p>
																			</div>
																			<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
																				<sub>Código</sub>
																				<p>{{ $record->nome }}</p>
																			</div>
																		</div>
																	</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
																</div>
																</div>
															</div>
														</div>
												@endshield

												@shield('cnaes.edit')
													<a class="btn btn-secondary btn-circle" href="{{ route('cnaes.edit', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-edit"></i></a>
												@endshield
												
												@shield('cnaes.destroy')
													<a class="btn btn-danger btn-circle" href="{{ route('cnaes.destroy', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Deletar"><i class="fa fa-trash"></i></a>
												@endshield
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>

						<div class="row m-t-30">
                            <div class="col">
                                <div class="text-center">
                                    {!! $records->appends(['search' => request()->get('search'), 'per_page' => request()->get('per_page')])->links('includes.parciais.paginacao') !!}
                                </div>
                            </div>
                        </div>

						@if( !empty(request()->get('search')) )
	                  		<p><a class="btn btn-rounded btn-block btn-info" href="{{ route('cnaes.index') }}"  style="color:#fff;"><i class="fa fa-reply"></i> Exibir
		                      todos os registros</a></p>
		                @endif
					@else
						@if(request()->get('search'))
							<div class="alert alert-info">
								Nenhum resultado encontrado com o termo <strong>{{ request()->get('search') }}</strong>.
							</div>

							<p>
								<a class="btn btn-block btn-info" href="{{ route('cnaes.index') }}">
									<i class="fa fa-reply"></i> Voltar
								</a>
							</p>
						@else
							<div class="alert alert-info">Não existem registros cadastrados.</div>
						@endif
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection