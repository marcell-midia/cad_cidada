<fieldset>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            {!! Form::label('codigo', 'Código *') !!}
            {!! Form::text('codigo', old('codigo', null), ['class' => 'form-control cnae', 'maxlength' => 15]) !!}
        </div>
        <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
            {!! Form::label('nome', 'Descrição *') !!}
            {!! Form::text('nome', old('nome', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
        </div>
    </div>
</fieldset>