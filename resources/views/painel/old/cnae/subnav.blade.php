<div class="btn-group pull-right" role="group" aria-label="Basic example">
	@if( app('request')->route()->action['as'] != 'cnaes.index' )
        <a href="{{ route('cnaes.index') }}" class="btn btn-secondary"><i class="fa fa-reply"></i> Listar de cnae</a>
    @endif
    <a href="{{ route('cnaes.create') }}" class="btn btn-info"><i class="mdi mdi-plus-circle"></i> Novo cnae</a>
</div>