@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('search-main') @include('includes.administrativo.search') @endsection

@section('page-titles')
	<div class="row page-titles">
	    <div class="col-md-6 col-8 align-self-center">
	        <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
	        <ol class="breadcrumb">
	            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="text-secondary">Dashboard</a></li>
	            <li class="breadcrumb-item"><a href="javascript:void(0);" class="text-secondary">Cadastro</a></li>
	            <li class="breadcrumb-item"><a href="{{ route('cnaes.index') }}" class="text-secondary">{{ $meta['title'] }}</a></li>
	            <li class="breadcrumb-item active"><a href="{{ route('cnaes.create') }}" class="text-secondary">{{ $meta['action'] }}</a></li>
	        </ol>
	    </div>
	    <div class="col-md-6 col-4 align-self-center">
	    	@include('painel.cnae.subnav')
	    </div>
	</div>
@endsection

@section('page-content')
	<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @if( $errors->any() )
                                <div class="alert alert-danger">
                                    Preencha corretamente os campos abaixo: <br>
                                    @foreach( $errors->all() as $error )
                                        - {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif

                            {!! Form::model($record, ['route' => ['cnaes.update', $record->id], 'method' => 'put', 'class' => 'form']) !!}
                                @include('painel.cnae.form')
                                @include('layouts.parties.form_buttom')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	<script>
		$(function () {
			$('.somente-numero').bind("keyup blur focus", function(e) {
	            e.preventDefault();
	            var expre = /[^\d]/g;
	            $(this).val($(this).val().replace(expre,''));
	       });
		});
	</script>
@endsection