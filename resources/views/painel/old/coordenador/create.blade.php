@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('page-titles')
	<div class="row page-titles">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.index') }}">Empresa</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.edit', $empresa->id) }}">{{ ucwords(mb_strtolower($empresa->nome_razaosocial)) }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.exibirparecercoordenador', [$empresa->id, $area->id, $parecerTecnico->id]) }}">{{ $meta['title'] }}</a></li>
                {{-- <li class="breadcrumb-item"><a href="{{ route('empresas.cadastrarparecercoordenador', [$empresa->id, $area->id, $parecerTecnico->id]) }}">{{ $meta['action'] }}</a></li> --}}
            </ol>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 align-self-center">
            <div class="btn-group pull-right" role="group" aria-label="Basic example">
                <a href="{{ route('empresas.index') }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar de empresas</a>
                <a href="{{ route('empresas.exibirareas', $empresa->id) }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar de áreas</a>
                <a href="{{ route('empresas.cadastrarparecercoordenador', [$empresa->id, $area->id, $parecerTecnico->id]) }}" class="btn btn-info"><i class="mdi mdi-plus-circle"></i> Novo registro</a>
            </div>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <span class="badge badge-secondary">Modelo</span> <br>
                            @if($empresa->tipo_modelo == 1)
                                Completo
                            @else
                                Simplicado
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <span class="badge badge-secondary">Tipo de pessoa</span> <br>
                            @if($empresa->tipo_modelo == 1)
                                P. Jurídica
                            @else
                                P. Física
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <span class="badge badge-secondary">
                                @if($empresa->tipo_cadastro == 1)
                                    Razão social
                                @else
                                    Nome titular
                                @endif
                            </span> <br>
                            {{ $empresa->nome_razaosocial }}
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <span class="badge badge-secondary">
                                @if($empresa->tipo_cadastro == 1)
                                    CNPJ
                                @else
                                    CPF
                                @endif
                            </span> <br>
                            {{ $empresa->cpf_cnpj }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @if( $errors->any() )
                                <div class="alert alert-danger">
                                    Preencha corretamente os campos abaixo: <br>
                                    @foreach( $errors->all() as $error )
                                        - {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif

                            {!! Form::open(['route' => ['empresas.salvarparecercoordenador', $empresa->id, $area->id, $parecerTecnico->id], 'method' => 'post', 'class' => 'form']) !!}
                                @include('painel.coordenador.form')
                                @include('layouts.parties.form_buttom')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('input[name=tipo]').change(function() {
                if ($(this).val() == 4) {
                    $('textarea[name=observacoes]').attr('disabled', false);
                } else {
                    $('textarea[name=observacoes]').attr('disabled', true);
                }
            });
        });
    </script>
@endsection