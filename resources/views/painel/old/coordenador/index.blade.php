@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('search-main') @include('includes.administrativo.search') @endsection

@section('page-titles')
    <div class="row page-titles">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.index') }}">Empresa</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.edit', $empresa->id) }}">{{ ucwords(mb_strtolower($empresa->nome_razaosocial)) }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.exibirparecercoordenador', [$empresa->id, $area->id, $parecerTecnico->id]) }}">{{ $meta['title'] }}</a></li>
            </ol>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 align-self-center">
            <div class="btn-group pull-right" role="group" aria-label="Basic example">
                <a href="{{ route('empresas.index') }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar de empresas</a>
                <a href="{{ route('empresas.exibirareas', $empresa->id) }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar de área</a>
                <a href="{{ route('empresas.cadastrarparecercoordenador', [$empresa->id, $area->id, $parecerTecnico->id]) }}" class="btn btn-info"><i class="mdi mdi-plus-circle"></i> Novo registro</a>
            </div>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <span class="badge badge-secondary">Modelo</span> <br>
                            @if($empresa->tipo_modelo == 1)
                                Completo
                            @else
                                Simplicado
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <span class="badge badge-secondary">Tipo de pessoa</span> <br>
                            @if($empresa->tipo_modelo == 1)
                                P. Jurídica
                            @else
                                P. Física
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <span class="badge badge-secondary">
                                @if($empresa->tipo_cadastro == 1)
                                    Razão social
                                @else
                                    Nome titular
                                @endif
                            </span> <br>
                            {{ $empresa->nome_razaosocial }}
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <span class="badge badge-secondary">
                                @if($empresa->tipo_cadastro == 1)
                                    CNPJ
                                @else
                                    CPF
                                @endif
                            </span> <br>
                            {{ $empresa->cpf_cnpj }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @if( count($records) > 0 )
                                @php
                                    $pages = (($records->total() % $records->perPage()) > 0) ? (int)($records->total() / $records->perPage()) + 1 : ($records->total() / $records->perPage());
                                    $pag = max(min($pages, $records->currentPage()), 1);
                                    $start = ($pag - 1) * $records->perPage();
                                    $pluralTotal = ( $records->total() <= 1 ) ? '' : 's';
                                @endphp

                                <div class="alert alert-dark mb-5" role="alert">
                                    Exibindo {{ min($records->total(), ($start + 1)) }} - {{ min($records->total(), ($start + $records->perPage())) }} de {{ $records->total() }} registro{{ $pluralTotal }} cadastrado{{ $pluralTotal }}
                                </div>

                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Data</th>
                                            <th>Parecer Coordenador</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($records as $record)
                                            <tr>
                                                <td>{{ $record->present()->getDateRegister }}</td>
                                                <td>{{ $record->present()->getParecerTecnico }}</td>
                                                <td>
                                                    <a class="btn btn-secondary btn-circle" href="{{ route('empresas.editarparecercoordenador', [$record->empresa_id, $record->area_id, $record->parecer_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-edit"></i></a>
                                                    <a class="btn btn-secondary btn-circle" href="{{ route('empresas.exibirparecerdiretor', [$record->empresa_id, $record->area_id, $record->parecer_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Parecer Diretor"><i class="fa fa-gavel"></i></a>
                                                    <a class="btn btn-danger btn-circle" href="{{ route('empresas.deletarparecercoordenador', [$record->empresa_id, $record->area_id, $record->parecer_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Deletar"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                @if(request()->get('search'))
                                    <div class="alert alert-info">
                                        Nenhum resultado encontrado com o termo <strong>{{ request()->get('search') }}</strong>.
                                    </div>

                                    <p>
                                        <a class="btn btn-block btn-info" href="{{ route('empresas.index') }}">
                                            <i class="fa fa-reply"></i> Voltar
                                        </a>
                                    </p>
                                @else
                                    <div class="alert alert-info">Não existem registros cadastrados.</div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection