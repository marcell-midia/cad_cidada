<fieldset>
	<div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
			{!! Form::radio('tipo', 1, false) !!}
			{!! Form::label('tipo', 'De acordo com o Parecer Técnico e pela Continuidade do processo para emissão do Certificado de Registro no CERM.') !!}
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
			{!! Form::radio('tipo', 2, false) !!}
			{!! Form::label('tipo', 'De acordo com o Parecer Técnico e Desfavorável a emissão do Certificado de Registro no CERM.') !!}
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
			{!! Form::radio('tipo', 3, false) !!}
			{!! Form::label('tipo', 'De acordo com o Parecer Técnico e pela Notificação do Empreendedor para atendimento de exigência.') !!}
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
			{!! Form::radio('tipo', 4, false) !!}
			{!! Form::label('tipo', 'Para revisão do Parecer Técnico em virtude das seguintes observações:') !!}
		</div>
	</div>
	<div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
			{{ Form::textarea('observacoes', null, ['class' => 'form-control', 'disabled' => true]) }}
		</div>
	</div>
</fieldset>