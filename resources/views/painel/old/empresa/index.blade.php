@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('search-main') @include('includes.administrativo.search') @endsection

@section('page-titles')
	<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ $meta['title'] }}</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('empresas.index') }}">{{ $meta['action'] }}</a></li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
        	<a href="{{ route('empresas.create') }}" class="btn pull-right hidden-sm-down btn-info"><i class="mdi mdi-plus-circle"></i> Nova empresa</a>
        </div>
    </div>
@endsection

@section('page-content')
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="card">
				<div class="card-body">
					@if( count($records) > 0 )
						@php
						$pages = (($records->total() % $records->perPage()) > 0) ? (int)($records->total() / $records->perPage()) + 1 : ($records->total() / $records->perPage());
						$pag = max(min($pages, $records->currentPage()), 1);
						$start = ($pag - 1) * $records->perPage();
						$pluralTotal = ( $records->total() <= 1 ) ? '' : 's';
						@endphp

		                <div class="alert alert-dark mb-5" role="alert">
		                Exibindo {{ min($records->total(), ($start + 1)) }} - {{ min($records->total(), ($start + $records->perPage())) }} de {{ $records->total() }} registro{{ $pluralTotal }} cadastrado{{ $pluralTotal }}
		                </div>

	                	<table class="table table-bordered table-striped text-center">
							<thead>
								<tr>
									<th>Modelo</th>
									<th>Tipo</th>
									<th>Empresa</th>
									<th>CNPJ/CPF</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($records as $record)
									<tr>
										<td>@if($record->tipo_modelo == 1) COMPLETO @else SIMPLICADO @endif</td>
										<td>@if($record->tipo_cadastro == 1) PESSOA JURÍDICA @else PESSOA FISICA @endif</td>
										<td>{{ $record->nome_razaosocial }}</td>
										@if(strlen($record->cpf_cnpj) > 12)
										<td class="cnpj">{{ $record->cpf_cnpj }}</td>
										@else
										<td class="cpf">{{ $record->cpf_cnpj }}</td>
										@endif
										<td>
											<a class="btn btn-secondary btn-circle" href="{{ route('empresas.show', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Visualizar"><i class="fa fa-eye"></i></a>
											<a class="btn btn-secondary btn-circle" href="{{ route('empresas.edit', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-edit"></i></a>
											<a class="btn btn-secondary btn-circle" href="{{ route('empresas.exibirsocios', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Sócios"><i class="fa fa-users"></i></a>
											<a class="btn btn-warning btn-circle" href="{{ route('empresas.exibirareas', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Áreas"><i class="fa fa-list-ul"></i></a>
											<a class="btn btn-success btn-circle" href="{{ route('empresas.getanexos', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Documentação"><i class="fa fa-paperclip"></i></a>
											<a class="btn btn-success btn-circle" href="{{ route('empresas.exibirareas', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Certificados"><i class="fa fa-certificate"></i></a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>

						@if( !empty(request()->get('search')) )
	                  		<p><a class="btn btn-rounded btn-info" href="{{ route('empresas.index') }}"  style="color:#fff;"><i class="fa fa-reply"></i> Exibir
		                      todos os registros</a></p>
		                @endif
					@else
						@if(request()->get('search'))
							<div class="alert alert-info">
								Nenhum resultado encontrado com o termo <strong>{{ request()->get('search') }}</strong>.
							</div>

							<p>
								<a class="btn btn-block btn-info" href="{{ route('empresas.index') }}">
									<i class="fa fa-reply"></i> Voltar
								</a>
							</p>
						@else
							<div class="alert alert-info">Não existem registros cadastrados.</div>
						@endif
					@endif
				</div>
			</div>		
		</div>
	</div>
@endsection
