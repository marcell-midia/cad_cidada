@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection
@section('page-titles')
	<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item">{{ $meta['title'] }}</li>
                <li class="breadcrumb-item active">{{ $meta['action'] }}</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
        	<a href="{{ route('empresas.create') }}" class="btn pull-right hidden-sm-down btn-info"><i class="mdi mdi-plus-circle"></i> Novo registro</a>
        </div>
    </div>
@endsection

@section('page-content')
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
                        <div class="col-12">
                            @if( $errors->any() )
                                <div class="alert alert-danger">
                                    Preencha corretamente os campos abaixo: <br>
                                    @foreach( $errors->all() as $error )
                                        - {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                            
                            {!! Form::open(['route' => 'empresas.store', 'method' => 'post', 'class' => 'form']) !!}
                                @include('painel.empresa.form')
                                @include('layouts.parties.form_buttom')
                            {!! Form::close() !!}
                        </div>
                    </div>
				</div>
			</div>		
		</div>
	</div>
@endsection

@section('scripts')
	<script>
        $(function () {
            $('#juridico_natureza').change(function() {
                if ($(this).val() == 4) {
                    $('input[name=juridico_natureza_outra]').attr('disabled', false);
                } else {
                    $('input[name=juridico_natureza_outra]').attr('disabled', true);
                }
            });

            $('#juridico_tipo_entidade').change(function() {
                if ($(this).val() == 6) {
                    $('input[name=juridico_tipo_entidade_outra]').attr('disabled', false);
                } else {
                    $('input[name=juridico_tipo_entidade_outra]').attr('disabled', true);
                }
            });

            $('input[name=tipo_modelo]').change(function() {

                if ($(this).val() == 1) {
                    $('#nomeFantasia').attr('disabled', false);
                    $('#pessoaFisica').attr('disabled', true);
                    $('#fisicaNome').hide();
                    $('#fisicaCpf').hide();
                    $('#fisicaRg').hide();
                    $('#fisicaRgUf').hide();
                    $('#juridicoRazaoSocial').show();
                    $('#juridicoNomeFantasia').show();
                    $('#juridicoCnpj').show();
                    $('#juridicoInscEstadualUF').show();
                    $('#juridicoInscEstadual').show();
                    $('#juridicoInscMunicipal').show();
                    $('#juridicoControleAcionario').show();
                    $('#juridicoTipoUnidade').show();
                    $('#juridicoPorteEmpresa').show();
                    $('#juridicoCapitalAutorizado').show();
                    $('#juridicoNatureza').show();
                    $('#juridico_natureza').empty();
                    $('#juridico_natureza').append(
                        '<option></option>' +
                        '<option value="1">S/A</option>' +
                        '<option value="2">LTDA</option>' +
                        '<option value="3">COOPERATIVA</option>' +
                        '<option value="4">OUTRA</option>'
                    );
                    $('#juridicoCapitalSocial').show();
                    $('#juridicoCapitalSocial').removeClass('col-6');
                    $('#juridicoCapitalSocial').addClass('col-3');
                    $('#juridicoTipoCapital').show();
                    $('#juridicoEsfera').hide();
                    $('#juridicoEsferaOutra').hide();
                    $('#juridicoTipoEntidade').hide();
                    $('#juridicoTipoEntidadeOutra').hide();
                    $('#juridicoSite').show();
                    $('#juridicoEmail').show();
                    $('#juridicoCapitalSubscrito').show();
                    $('#juridicoCapitalIntegral').show();
                } else {
                    $('input[name=tipo_pessoa]').change(function () {
                        if ($(this).val() == '2') {
                            $('#nomeFantasia').attr('disabled', true);
                        } else {
                            $('#nomeFantasia').attr('disabled', false);
                        }
                    });
                    $('#pessoaFisica').attr('disabled', false);
                    $('#fisicaNome').hide();
                    $('#fisicaCpf').hide();
                    $('#fisicaRg').hide();
                    $('#fisicaRgUf').hide();
                    $('#juridicoRazaoSocial').show();
                    $('#juridicoNomeFantasia').show();
                    $('#juridicoCnpj').show();
                    $('#juridicoInscEstadual').show();
                    $('#juridicoInscMunicipal').show();
                    $('#juridicoControleAcionario').hide();
                    $('#juridicoTipoUnidade').hide();
                    $('#juridicoPorteEmpresa').hide();
                    $('#juridicoCapitalAutorizado').hide();
                    $('#juridicoNatureza').show();
                    $('#juridico_natureza').empty();
                    $('#juridico_natureza').append(
                        '<option></option>' +
                        '<option value="1">S/A</option>' +
                        '<option value="2">LTDA</option>' +
                        '<option value="3">COOPERATIVA</option>' +
                        '<option value="5">ME</option>' +
                        '<option value="6">COOPERATIVA</option>' +
                        '<option value="7">EMPRESA OPTANTE PELO SIMPLES NACIONAL</option>' +
                        '<option value="8">PESSOA FÍSICA</option>' +
                        '<option value="4">OUTRA</option>'
                    );
                    $('#juridicoNaturezaOutra').show();
                    $('#juridicoCapitalSocial').show();
                    $('#juridicoTipoCapital').hide();
                    $('#juridicoEsfera').show();
                    $('#juridicoEsferaOutra').show();
                    $('#juridicoTipoEntidade').show();
                    $('#juridicoTipoEntidadeOutra').show();
                    $('#juridicoSite').show();
                    $('#juridicoEmail').show();
                    $('#juridicoCapitalSubscrito').hide();
                    $('#juridicoCapitalIntegral').hide();
                }
            });

            $('input[name=tipo_pessoa]').change(function() {
                if ($(this).val() == 1) {
                    $('#fisicaNome').hide();
                    $('#fisicaCpf').hide();
                    $('#fisicaRg').hide();
                    $('#fisicaRgUf').hide();
                    $('#juridicoRazaoSocial').show();
                    $('#juridicoNomeFantasia').show();
                    $('#juridicoCnpj').show();
                    $('#juridicoInscEstadualUF').show();
                    $('#juridicoInscEstadual').show();
                    $('#juridicoInscMunicipal').show();
                    $('#juridicoControleAcionario').hide();
                    $('#juridicoTipoUnidade').hide();
                    $('#juridicoPorteEmpresa').hide();
                    $('#juridicoCapitalAutorizado').hide();
                    $('#juridicoNatureza').show();
                    $('#juridicoNaturezaOutra').show();
                    $('#juridicoCapitalSocial').removeClass('col-sm-2 col-md-2 col-lg-2');
                    $('#juridicoCapitalSocial').addClass('col-sm-3 col-md-3 col-lg-3');
                    $('#juridicoCapitalSocial').show();
                    $('#juridicoTipoCapital').hide();
                    $('#juridicoEsfera').show();
                    $('#juridicoEsferaOutra').show();
                    $('#juridicoTipoEntidade').show();
                    $('#juridicoTipoEntidadeOutra').show();
                    $('#juridicoSite').removeClass('col-sm-5 col-md-5 col-lg-5');
                    $('#juridicoSite').addClass('col-sm-6 col-md-6 col-lg-6');
                    $('#juridicoSite').show();
                    $('#juridicoEmail').removeClass('col-sm-5 col-md-5 col-lg-5');
                    $('#juridicoEmail').addClass('col-sm-6 col-md-6 col-lg-6');
                    $('#juridicoCapitalSubscrito').hide();
                    $('#juridicoCapitalIntegral').hide();
                } else {
                    $('#fisicaNome').show();
                    $('#fisicaCpf').show();
                    $('#fisicaRg').show();
                    $('#fisicaRgUf').show();
                    $('#juridicoRazaoSocial').hide();
                    $('#juridicoNomeFantasia').hide();
                    $('#juridicoCnpj').hide();
                    $('#juridicoInscEstadualUF').hide();
                    $('#juridicoInscEstadual').hide();
                    $('#juridicoInscMunicipal').hide();
                    $('#juridicoControleAcionario').hide();
                    $('#juridicoTipoUnidade').hide();
                    $('#juridicoTipoUnidade').hide();
                    $('#juridicoPorteEmpresa').hide();
                    $('#juridicoCapitalAutorizado').hide();
                    $('#juridicoNatureza').show();
                    $('#juridicoNaturezaOutra').show();
                    $('#juridicoCapitalSocial').removeClass('col-sm-3 col-md-3 col-lg-3');
                    $('#juridicoCapitalSocial').addClass('col-sm-2 col-md-2 col-lg-2');
                    $('#juridicoCapitalSocial').show();
                    $('#juridicoTipoCapital').hide();
                    $('#juridicoEsfera').hide();
                    $('#juridicoEsferaOutra').hide();
                    $('#juridicoTipoEntidade').hide();
                    $('#juridicoTipoEntidadeOutra').hide();
                    $('#juridicoSite').removeClass('col-sm-6 col-md-6 col-lg-6');
                    $('#juridicoSite').addClass('col-sm-5 col-md-5 col-lg-5');
                    $('#juridicoSite').show();
                    $('#juridicoEmail').removeClass('col-sm-6 col-md-6 col-lg-6');
                    $('#juridicoEmail').addClass('col-sm-5 col-md-5 col-lg-5');
                    $('#juridicoCapitalSubscrito').hide();
                    $('#juridicoCapitalIntegral').hide();
                } 
            });

            //Quando o campo cep perde o foco.
            $('input[name=cep]').blur(function() {
                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');
                //Verifica se campo cep possui valor informado.
                if (cep != "") {
                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;
                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {
                        //Preenche os campos com "..." enquanto consulta webservice.
                        $('input[name=endereco]').val("...");
                        $('input[name=numero]').val("...");
                        $('input[name=bairro]').val("...");
                        $('input[name=cidade]').val("...");
                        $('input[name=complemento]').val("...");
                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $('input[name=endereco]').val(dados.logradouro.toUpperCase());
                                $('input[name=bairro]').val(dados.bairro.toUpperCase());
                                $('input[name=cidade]').val(dados.localidade.toUpperCase());
                                $('input[name=numero]').val('');
                                $('input[name=numero]').focus();
                                $('input[name=complemento]').val('');
                                $('select[name=uf]').children('option[value="'+dados.uf+'"]').attr("selected", true);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                $('input[name=endereco]').val('');
                                $('input[name=bairro]').val('');
                                $('input[name=cidade]').val('');
                                $('input[name=complemento]').val('');
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        $('input[name=endereco]').val('');
                        $('input[name=bairro]').val('');
                        $('input[name=cidade]').val('');
                        $('input[name=complemento]').val('');
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    $('input[name=endereco]').val('');
                    $('input[name=bairro]').val('');
                    $('input[name=cidade]').val('');
                }
            });

            //Quando o campo cep perde o foco.
            $('input[name=responsavel_cep]').blur(function() {
                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');
                //Verifica se campo cep possui valor informado.
                if (cep != "") {
                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;
                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {
                        //Preenche os campos com "..." enquanto consulta webservice.
                        $('input[name=responsavel_logradouro]').val("...");
                        $('input[name=responsavel_numero]').val("...");
                        $('input[name=responsavel_complemento]').val("...");
                        $('input[name=responsavel_bairro]').val("...");
                        $('input[name=responsavel_cidade]').val("...");
                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $('input[name=responsavel_logradouro]').val(dados.logradouro.toUpperCase());
                                $('input[name=responsavel_bairro]').val(dados.bairro.toUpperCase());
                                $('input[name=responsavel_cidade]').val(dados.localidade.toUpperCase());
                                $('input[name=responsavel_numero]').val('');
                                $('input[name=responsavel_numero]').focus();
                                $('input[name=responsavel_complemento]').val('');
                                $('select[name=responsavel_estado]').children('option[value="'+dados.uf+'"]').attr("selected", true);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                $('input[name=responsavel_logradouro]').val('');
                                $('input[name=responsavel_numero]').val('');
                                $('input[name=responsavel_complemento]').val('');
                                $('input[name=responsavel_bairro]').val('');
                                $('input[name=responsavel_cidade]').val('');
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        $('input[name=responsavel_logradouro]').val('');
                        $('input[name=responsavel_numero]').val('');
                        $('input[name=responsavel_complemento]').val('');
                        $('input[name=responsavel_bairro]').val('');
                        $('input[name=responsavel_cidade]').val('');
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    $('input[name=responsavel_logradouro]').val('');
                    $('input[name=responsavel_numero]').val('');
                    $('input[name=responsavel_complemento]').val('');
                    $('input[name=responsavel_bairro]').val('');
                    $('input[name=responsavel_cidade]').val('');
                }
            });

            //Quando o campo cep perde o foco.
            $('input[name=atividade_cep]').blur(function() {
                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');
                //Verifica se campo cep possui valor informado.
                if (cep != "") {
                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;
                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {
                        //Preenche os campos com "..." enquanto consulta webservice.
                        $('input[name=atividade_logradouro]').val("...");
                        $('input[name=atividade_numero]').val("...");
                        $('input[name=atividade_complemento]').val("...");
                        $('input[name=atividade_bairro]').val("...");
                        $('input[name=atividade_municipio]').val("...");
                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $('input[name=atividade_logradouro]').val(dados.logradouro.toUpperCase());
                                $('input[name=atividade_bairro]').val(dados.bairro.toUpperCase());
                                $('input[name=atividade_municipio]').val(dados.localidade.toUpperCase());
                                $('input[name=atividade_numero]').val('');
                                $('input[name=atividade_numero]').focus();
                                $('input[name=atividade_complemento]').val('');
                                $('select[name=atividade_estado]').children('option[value="'+dados.uf+'"]').attr("selected", true);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                $('input[name=atividade_logradouro]').val('');
                                $('input[name=atividade_numero]').val('');
                                $('input[name=atividade_complemento]').val('');
                                $('input[name=atividade_bairro]').val('');
                                $('input[name=atividade_municipio]').val('');
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        $('input[name=atividade_logradouro]').val('');
                        $('input[name=atividade_numero]').val('');
                        $('input[name=atividade_complemento]').val('');
                        $('input[name=atividade_bairro]').val('');
                        $('input[name=atividade_municipio]').val('');
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    $('input[name=atividade_logradouro]').val('');
                    $('input[name=atividade_numero]').val('');
                    $('input[name=atividade_complemento]').val('');
                    $('input[name=atividade_bairro]').val('');
                    $('input[name=atividade_municipio]').val('');
                }
            });

            $("#formCadastroBasico").validate({
                highlight: function(element) {
                    $(element).closest('.form-control').addClass('is-invalid').removeClass('is-valid');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-control').addClass('is-valid').removeClass('is-invalid');
                },
                errorElement: 'span',
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    tipo_pessoa: "required",
                    juridico_raz_social: {
                        required: {
                            depends: function(element) {
                                    return $('input[name="tipo_pessoa"]').is(':checked');
                                }
                            },
                        minlength: 2,
                        maxlength: 100
                    },
                    juridico_nome_fantasia: {
                        required: {
                            depends: function(element) {
                                return $('input[name="tipo_pessoa"]').is(':checked');
                            }
                        },
                        minlength: 2,
                        maxlength: 100
                    },
                    juridico_cnpj: {
                        required: {
                            depends: function(element) {
                                return $('input[name="tipo_pessoa"]').is(':checked');
                            }
                        },
                        maxlength: 18,
                        cnpj: true
                    },
                    juridico_insc_estadual: {
                        required: {
                            depends: function(element) {
                                return $('input[name="tipo_pessoa"]').is(':checked');
                            }
                        },
                        InscricaoEstadualBR: true
                    },
                    juridico_insc_municipal: {
                        minlength: 2,
                        maxlength: 20
                    },
                    juridico_natureza: {
                        required: true
                    },
                    cep: {
                        postalcodeBR: true
                    },
                    endereco: {
                        required: true,
                        minlength: 2,
                        maxlength: 100
                    },
                    numero: {
                        minlength: 2,
                        maxlength: 10
                    },
                    complemento: {
                        minlength: 2,
                        maxlength: 50
                    },
                    bairro: {
                        required: true,
                        minlength: 2,
                        maxlength: 50
                    },
                    cidade: {
                        required: true,
                        minlength: 2,
                        maxlength: 50
                    },
                    uf: {
                        required: true
                    },
                    cnae: {
                        required: true
                    },
                    juridico_controle_acionario: {
                        required: true
                    },
                    juridico_tipo_unidade: {
                        required: true
                    },
                    juridico_capital_social: {
                        required: true
                    },
                    juridico_tipo_capital: {
                        required: true
                    }
                },
                messages: {
                    tipo_pessoa: "Tipo"
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
            $('.mask_money').mask('000.000.000.000.000,00', {reverse: true});
			$('.mask_cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.mask_cpf').mask('000.000.000-00', {reverse: true});
			$('.mask_cep').mask('00000-000', {reverse: true});
            $(".somente_numero").bind("keyup blur focus", function(e) {
                e.preventDefault();
                var expre = /[^\d]/g;
                $(this).val($(this).val().replace(expre,''));
           });
        });
    </script>
@endsection