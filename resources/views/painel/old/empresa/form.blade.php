<fieldset>
    <h3 class="box-title">Empresa</h3>
    <hr class="m-b-40">
    <div class="row">
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label>Tipo do modelo *</label><br>
            <div class="input-group">
                <div class="input-group-prepend ">
                    <div class="input-group-text pb-0">
                        <label class="form-group-label">
                            @php
                            if (request()->old('tipo_modelo')) {
                                if (request()->old('tipo_modelo') == 1) {
                                    $check_modelo1 = 'checked=""';
                                    $check_modelo2 = '';
                                } else {
                                    $check_modelo1 = '';
                                    $check_modelo2 = 'checked=""';
                                }
                            } else {
                                if (app('request')->route()->action['as'] == 'empresas.edit') {
                                    if ($record->tipo_modelo == 1) {
                                        $check_modelo1 = 'checked=""';
                                        $check_modelo2 = '';
                                    } else {
                                        $check_modelo1 = '';
                                        $check_modelo2 = 'checked=""';
                                    }
                                } else {
                                    $check_modelo1 = 'checked=""';
                                    $check_modelo2 = '';
                                }
                            }
                            @endphp
                            <input checked type="radio" name="tipo_modelo" value="1" {{ $check_modelo1 }}> Completo
                        </label>
                    </div>
                    <div class="input-group-text pb-0">
                        <label>
                            <input type="radio" name="tipo_modelo" value="2" {{ $check_modelo2 }}> Simplicado
                        </label>
                    </div>
                </div>
            </div>
        </div>

       <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <label>Tipo de pessoa  *</label><br>
            <div class="input-group">
                <div class="input-group-prepend ">
                    @php
                    if (request()->old('tipo_pessoa')) {
                        if (request()->old('tipo_pessoa') == 1) {
                            $check_pessoa1 = 'checked=""';
                            $check_pessoa2 = '';
                            if (request()->old('tipo_modelo') == 1) {
                                $pessoaChecked = 'disabled';
                            } else {
                                $pessoaChecked = '';
                            }
                        } else {
                            $check_pessoa1 = '';
                            $check_pessoa2 = 'checked=""';
                            if (request()->old('tipo_modelo') == 2) {
                                $pessoaChecked = '';
                            } else {
                                $pessoaChecked = '';
                            }
                        }
                    } else {
                        if (app('request')->route()->action['as'] == 'empresas.edit') {
                            if ($record->tipo_cadastro == 1) {
                                $check_pessoa1 = 'checked=""';
                                $check_pessoa2 = '';
                                if ($record->tipo_modelo == 1) {
                                    $pessoaChecked = 'disabled';
                                } else {
                                    $pessoaChecked = '';
                                }
                            } else {
                                $check_pessoa1 = '';
                                $check_pessoa2 = 'checked=""';
                                if ($record->tipo_modelo == 2) {
                                    $pessoaChecked = '';
                                } else {
                                    $pessoaChecked = '';
                                }
                            }
                        } else {
                            $check_pessoa1 = 'checked=""';
                            $check_pessoa2 = '';
                            $pessoaChecked = 'disabled';
                        }
                    }
                    @endphp
                    <div class="input-group-text pb-0">
                        <label class="form-group-label">
                            <input type="radio" name="tipo_pessoa" id="pessoaJuridica" value="1" {{ $check_pessoa1 }}> P. Jurídica
                        </label>
                    </div>
                    <div class="input-group-text pb-0">
                        <label>
                            <input type="radio" name="tipo_pessoa" id="pessoaFisica" value="2" {{ $check_pessoa2 }} <?=$pessoaChecked?>> P. Física
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoRazaoSocial = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoRazaoSocial = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoRazaoSocial = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoRazaoSocial = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoRazaoSocial = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoRazaoSocial = 'style="display: none;"';
                }
            } else {
                $juridicoRazaoSocial = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6" id="juridicoRazaoSocial" <?=$juridicoRazaoSocial?>>
            {!! Form::label('juridico_raz_social', 'Razão social  *') !!}
            {!! Form::text('juridico_raz_social', old('juridico_raz_social', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->nome_razaosocial : null) : null), ['class' => 'form-control', 'maxlength' => '100']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $fisicaNome = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $fisicaNome = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $fisicaNome = 'style="display: block;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $fisicaNome = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $fisicaNome = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $fisicaNome = 'style="display: block;"';
                }
            } else {
                $fisicaNome = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12" id="fisicaNome" <?=$fisicaNome?>>
            {!! Form::label('fisica_nome', 'Nome titular *') !!}
            {!! Form::text('fisica_nome', old('juridico_raz_social', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) ? $record->nome_razaosocial : null) : null), ['class' => 'form-control', 'maxlength' => '100']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoNomeFantasia = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoNomeFantasia = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoNomeFantasia = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoNomeFantasia = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoNomeFantasia = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoNomeFantasia = 'style="display: none;"';
                }
            } else {
                $juridicoNomeFantasia = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6" id="juridicoNomeFantasia" <?=$juridicoNomeFantasia?>>
            {!! Form::label('juridico_nome_fantasia', 'Nome fantasia') !!}
            {!! Form::text('juridico_nome_fantasia', old('juridico_nome_fantasia', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->nome_fantasia : null) : null), ['class' => 'form-control', 'maxlength' => '100']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoCnpj = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoCnpj = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoCnpj = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoCnpj = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoCnpj = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoCnpj = 'style="display: none;"';
                }
            } else {
                $juridicoCnpj = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoCnpj" <?=$juridicoCnpj?>>
            {!! Form::label('juridico_cnpj', 'CNPJ  *') !!}
            {!! Form::text('juridico_cnpj', old('juridico_cnpj', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->cpf_cnpj : null) : null), ['class' => 'form-control mask_cnpj']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoInscEstadualUF = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoInscEstadualUF = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoInscEstadualUF = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoInscEstadualUF = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoInscEstadualUF = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoInscEstadualUF = 'style="display: none;"';
                }
            } else {
                $juridicoInscEstadualUF = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2" id="juridicoInscEstadualUF" <?=$juridicoInscEstadualUF?>>
            {!! Form::label('juridico_insc_estadual_uf', 'IE/UF') !!}
            {!! Form::select('juridico_insc_estadual_uf', ['' => ''] + Utilities::estados(), old('juridico_insc_estadual_uf', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->insc_estadual_rg_uf : null) : null), ['id' => 'juridico_insc_estadual_uf', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoInscEstadual = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoInscEstadual = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoInscEstadual = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoInscEstadual = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoInscEstadual = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoInscEstadual = 'style="display: none;"';
                }
            } else {
                $juridicoInscEstadual = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoInscEstadual" <?=$juridicoInscEstadual?>>
            {!! Form::label('juridico_insc_estadual', 'Inscrição Estadual (IE)', ['class' => 'd-sm-none d-md-none d-lg-block d-xl-block']) !!}
            {!! Form::label('juridico_insc_estadual', 'IE', ['class' => 'd-none d-md-none d-lg-none d-xl-none d-sm-block d-md-block']) !!}
            {!! Form::text('juridico_insc_estadual', old('juridico_insc_estadual', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->insc_estadual_rg : null) : null), ['class' => 'form-control somente_numero']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoInscMunicipal = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoInscMunicipal = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoInscMunicipal = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoInscMunicipal = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoInscMunicipal = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoInscMunicipal = 'style="display: none;"';
                }
            } else {
                $juridicoInscMunicipal = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4" id="juridicoInscMunicipal" <?=$juridicoInscMunicipal?>>
            {!! Form::label('juridico_insc_municipal', 'Inscrição Municipal') !!}
            {!! Form::text('juridico_insc_municipal', old('juridico_insc_municipal', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->insc_municipal : null) : null), ['class' => 'form-control somente_numero', 'maxlength' => 20]) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoControleAcionario = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoControleAcionario = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoControleAcionario = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoControleAcionario = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoControleAcionario = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoControleAcionario = 'style="display: none;"';
                }
            } else {
                $juridicoControleAcionario = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoControleAcionario" <?=$juridicoControleAcionario?>>
            {!! Form::label('juridico_controle_acionario', 'Controle acionário') !!}
            {!! Form::select('juridico_controle_acionario', ['' => '', 1 => 'NACIONAL', 2 => 'ESTRANGEIRO'], old('juridico_controle_acionario', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->natureza_controle : null) : null), ['class'=>'form-control']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoTipoUnidade = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoTipoUnidade = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoTipoUnidade = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoTipoUnidade = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoTipoUnidade = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoTipoUnidade = 'style="display: none;"';
                }
            } else {
                $juridicoTipoUnidade = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoTipoUnidade" <?=$juridicoTipoUnidade?>>
            {!! Form::label('juridico_tipo_unidade', 'Tipo e unidade', ['class' => 'd-sm-none d-md-none d-lg-block d-xl-block']) !!}
            {!! Form::label('juridico_tipo_unidade', 'Cont. acionário', ['class' => 'd-none d-md-none d-lg-none d-xl-none d-sm-block d-md-block']) !!}
            {!! Form::select('juridico_tipo_unidade', ['' => '', 1 => 'FILIAL', 2 => 'MATRIZ'], old('juridico_tipo_unidade', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) ? $record->natureza_unidade : null) : null), ['class'=>'form-control']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoPorteEmpresa = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoPorteEmpresa = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoPorteEmpresa = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoPorteEmpresa = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoPorteEmpresa = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoPorteEmpresa = 'style="display: none;"';
                }
            } else {
                $juridicoPorteEmpresa = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoPorteEmpresa" <?=$juridicoPorteEmpresa?>>
            {!! Form::label('juridico_porte_empresa', 'Porte da empresa', ['class' => 'd-sm-none d-md-none d-lg-block d-xl-block']) !!}
            {!! Form::label('juridico_porte_empresa', 'P. da empresa', ['class' => 'd-none d-md-none d-lg-none d-xl-none d-sm-block d-md-block']) !!}
            {!! Form::select('juridico_porte_empresa', ['' => '', 1 => 'MICRO', 2 => 'PEQUENA', 3 => 'MÉDIA', 4 => 'GRANDE'], old('juridico_porte_empresa', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->natureza_porte : null) : null), ['class'=>'form-control']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoCapitalAutorizado = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoCapitalAutorizado = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoCapitalAutorizado = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoCapitalAutorizado = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoCapitalAutorizado = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoCapitalAutorizado = 'style="display: none;"';
                }
            } else {
                $juridicoCapitalAutorizado = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoCapitalAutorizado" <?=$juridicoCapitalAutorizado?>>
            {!! Form::label('juridico_capital_autorizado', 'Capital autorizado', ['class' => 'd-sm-none d-md-none d-lg-block d-xl-block']) !!}
            {!! Form::label('juridico_capital_autorizado', 'Cap. autorizado', ['class' => 'd-none d-md-none d-lg-none d-xl-none d-sm-block d-md-block']) !!}
            {!! Form::text('juridico_capital_autorizado', old('juridico_capital_autorizado', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->natureza_capital_autorizado : null) : null), ['class' => 'form-control mask_money']) !!}
        </div>
    </div>

    <div class="row">
        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $fisicaCpf = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $fisicaCpf = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $fisicaCpf = 'style="display: block;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $fisicaCpf = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $fisicaCpf = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $fisicaCpf = 'style="display: block;"';
                }
            } else {
                $fisicaCpf = 'style="display: none;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6" id="fisicaCpf" <?=$fisicaCpf?>>
            {!! Form::label('fisica_cpf', 'CPF  *') !!}
            {!! Form::text('fisica_cpf', old('fisica_cpf', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) ? $record->cpf_cnpj : null) : null), ['class' => 'form-control mask_cpf']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $fisicaRg = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $fisicaRg = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $fisicaRg = 'style="display: block;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $fisicaRg = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $fisicaRg = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $fisicaRg = 'style="display: block;"';
                }
            } else {
                $fisicaRg = 'style="display: none;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4" id="fisicaRg" <?=$fisicaRg?>>
            {!! Form::label('fisica_rg', 'RG') !!}
            {!! Form::text('fisica_rg', old('fisica_rg', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) ? $record->insc_estadual_rg : null) : null), ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2" id="fisicaRgUf" <?=$fisicaRg?>>
            {!! Form::label('fisica_rg_uf', 'RG/UF') !!}
            {!! Form::select('fisica_rg_uf', ['' => ''] + Utilities::estados(), old('fisica_rg_uf', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) ? $record->insc_estadual_rg_uf : null) : null), ['id' => 'fisica_rg_uf', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
            
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoNatureza = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoNatureza = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoNatureza = 'style="display: block;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoNatureza = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoNatureza = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoNatureza = 'style="display: none;"';
                }
            } else {
                $juridicoNatureza = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6" id="juridicoNatureza" <?=$juridicoNatureza?>>
            @php
            if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
                if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                    $listaNaturezaJuridica = \App\Helpers\Utilities::juridico_natureza(1, 1);
                } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                    $listaNaturezaJuridica = \App\Helpers\Utilities::juridico_natureza(2, 1);
                } else {
                    $listaNaturezaJuridica = \App\Helpers\Utilities::juridico_natureza(2, 2);
                }
            } else {
                if (is_numeric(request()->segment(3))) {
                    if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                        $listaNaturezaJuridica = \App\Helpers\Utilities::juridico_natureza(1, 1);
                    } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                        $listaNaturezaJuridica = \App\Helpers\Utilities::juridico_natureza(2, 1);
                    } else {
                        $listaNaturezaJuridica = \App\Helpers\Utilities::juridico_natureza(2, 2);
                    }
                } else {
                    $listaNaturezaJuridica = \App\Helpers\Utilities::juridico_natureza(1, 1);
                }
            }
            @endphp
            {!! Form::label('juridico_natureza', 'Natureza jurídica') !!}
            {!! Form::select('juridico_natureza', ['' => ''] + $listaNaturezaJuridica, old('juridico_natureza', (is_numeric(request()->segment(3))) ? $record->natureza_tipo : null), ['class'=>'form-control']) !!}
        </div>
        @php
        if (request()->old('juridico_natureza')) {
            if (request()->old('juridico_natureza') == 4) {
                $juridicoNaturezaCheck = false;
            } else {
                $juridicoNaturezaCheck = true;
            }
        } else {
            if (is_numeric(request()->segment(3))) {
                if ($record->natureza_tipo == 4) {
                    $juridicoNaturezaCheck = false;
                } else {
                    $juridicoNaturezaCheck = true;
                }
            } else {
                $juridicoNaturezaCheck = true;
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6" id="juridicoNaturezaOutra" <?=$juridicoNatureza?>>
            {!! Form::label('juridico_natureza_outra', 'Outra') !!}
            {!! Form::text('juridico_natureza_outra', old('juridico_natureza_outra', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->natureza_tipo_outra : null) : null), ['class' => 'form-control', 'maxlength' => '100', 'disabled' => $juridicoNaturezaCheck]) !!}
        </div>
        <div class="form-group col-12">
            {!! Form::label('cnae_id', 'CNAE') !!}
            {!! Form::select('cnae_id', ['' => ''] + $cnaes->toArray(), old('cnae_id', null), ['class'=>'form-control', 'id' => 'nomeFantasia']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoCapitalSocial = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoCapitalSocial = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoCapitalSocial = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $nomeFantasia = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoCapitalSocial = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoCapitalSocial = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoCapitalSocial = 'style="display: none;"';
                } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                      $nomeFantasia = 'style="display: block;"';
                }
            } else {
                $juridicoCapitalSocial = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoCapitalSocial" <?=$juridicoCapitalSocial?>>
            {!! Form::label('juridico_capital_social', 'Capital social') !!}
            {!! Form::text('juridico_capital_social', old('juridico_capital_social', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2) ? $record->natureza_capital_social : null) : null), ['class' => 'form-control mask_money', 'maxlength' => '100']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoTipoCapital = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoTipoCapital = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoTipoCapital = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoTipoCapital = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoTipoCapital = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoTipoCapital = 'style="display: none;"';
                }
            } else {
                $juridicoTipoCapital = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoTipoCapital" <?=$juridicoTipoCapital?>>
            {!! Form::label('juridico_tipo_capital', 'Tipo de capital') !!}
            {!! Form::select('juridico_tipo_capital', ['' => '', 1 => 'ABERTO', 2 => 'FECHADO'], old('juridico_tipo_capital', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) ? $record->natureza_capital : null) : null), ['class'=>'form-control']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoCapitalSubscrito = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoCapitalSubscrito = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoCapitalSubscrito = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoCapitalSubscrito = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoCapitalSubscrito = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoCapitalSubscrito = 'style="display: none;"';
                }
            } else {
                $juridicoCapitalSubscrito = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoCapitalSubscrito" <?=$juridicoCapitalSubscrito?>>
            {!! Form::label('juridico_capital_subscrito', 'Capital subscrito') !!}
            {!! Form::text('juridico_capital_subscrito', old('juridico_capital_subscrito', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) ? $record->natureza_capital_subscrito : null) : null), ['class' => 'form-control mask_money', 'maxlength' => '100']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoCapitalIntegral = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoCapitalIntegral = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoCapitalIntegral = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoCapitalIntegral = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoCapitalIntegral = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoCapitalIntegral = 'style="display: none;"';
                }
            } else {
                $juridicoCapitalIntegral = 'style="display: block;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoCapitalIntegral" <?=$juridicoCapitalIntegral?>>
            {!! Form::label('juridico_capital_integral', 'Capital integral') !!}
            {!! Form::text('juridico_capital_integral', old('juridico_capital_integral', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) ? $record->natureza_capital_integral : null) : null), ['class' => 'form-control mask_money', 'maxlength' => '100']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoEsfera = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoEsfera = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoEsfera = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoEsfera = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoEsfera = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoEsfera = 'style="display: none;"';
                }
            } else {
                $juridicoEsfera = 'style="display: none;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoEsfera" <?=$juridicoEsfera?>>
            {!! Form::label('juridico_esfera_entidade', 'Esfera da entidade') !!}
            {!! Form::select('juridico_esfera_entidade', ['' => '', 1 => 'MUNICIPAL', 2 => 'ESTADUAL', 3 => 'NACIONAL', 4 => 'INTERNACIONAL'], old('juridico_esfera_entidade', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->associacao_esfera : null) : null), ['class'=>'form-control']) !!}
        </div>

        @php
        if (request()->old('tipo_modelo') && request()->old('tipo_pessoa')) {
            if (request()->old('tipo_modelo') == 1 && request()->old('tipo_pessoa') == 1) {
                $juridicoTipoEntidade = 'style="display: none;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 1) {
                $juridicoTipoEntidade = 'style="display: block;"';
            } elseif (request()->old('tipo_modelo') == 2 && request()->old('tipo_pessoa') == 2) {
                $juridicoTipoEntidade = 'style="display: none;"';
            }
        } else {
            if (app('request')->route()->action['as'] == 'empresas.edit') {
                if ($record->tipo_modelo == 1 && $record->tipo_cadastro == 1) {
                    $juridicoTipoEntidade = 'style="display: none;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) {
                    $juridicoTipoEntidade = 'style="display: block;"';
                } elseif ($record->tipo_modelo == 2 && $record->tipo_cadastro == 2) {
                    $juridicoTipoEntidade = 'style="display: none;"';
                }
            } else {
                $juridicoTipoEntidade = 'style="display: none;"';
            }
        }
        @endphp
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoTipoEntidade" <?=$juridicoTipoEntidade?>>
            {!! Form::label('juridico_tipo_entidade', 'Tipo de entidade') !!}
            {!! Form::select('juridico_tipo_entidade', ['' => '', 1 => 'ASSOCIAÇÃO INDUSTRIAL', 2 => 'ASSOCIAÇÃO COMERCIAL', 3 => 'ASSOCIAÇÃO EMPRESARIAL', 4 => 'SINDICATO', 5 => 'COOPERATIVA', 6 => 'OUTRA'], old('juridico_capital_integral', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->associacao_tipo : null) : null), ['id' => 'juridico_tipo_entidade', 'class'=>'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoTipoEntidadeOutra" <?=$juridicoTipoEntidade?>>
            {!! Form::label('juridico_tipo_entidade_outra', 'Outra') !!}
            {!! Form::text('juridico_tipo_entidade_outra',old('juridico_tipo_entidade_outra', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->associacao_tipo_outra : null) : null), ['class' => 'form-control', 'maxlength' => '100', 'disabled' => true]) !!}
        </div>

        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6" id="juridicoSite">
            {!! Form::label('juridico_site', 'Site do empreendimento') !!}
            {!! Form::text('juridico_site', old('juridico_site', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->site : null) : null), ['class' => 'form-control', 'maxlength' => '100']) !!}
        </div>
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6" id="juridicoEmail">
            {!! Form::label('email', 'E-mail') !!}
            {!! Form::email('email', old('email', (is_numeric(request()->segment(3))) ? (($record->tipo_modelo == 1 || $record->tipo_modelo == 2 && $record->tipo_cadastro == 1) ? $record->email : null) : null), ['class' => 'form-control', 'maxlength' => '100']) !!}
        </div>

        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2" id="juridicoCep">
            {!! Form::label('cep', 'CEP') !!}
            {!! Form::text('cep', old('cep', (is_numeric(request()->segment(3))) ? $record->cep : null), ['class' => 'form-control mask_cep', 'maxlength' => 9]) !!}
        </div>
        <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
            {!! Form::label('endereco', 'Endereço') !!}
            {!! Form::text('endereco', old('endereco', (is_numeric(request()->segment(3))) ? $record->logradouro : null), ['class' => 'form-control', 'maxlength' => 100]) !!}
        </div>
        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2">
            {!! Form::label('numero', 'Número') !!}
            {!! Form::text('numero', old('numero', (is_numeric(request()->segment(3))) ? $record->numero : null), ['class' => 'form-control', 'maxlength' => 10]) !!}
        </div>
        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4" id="juridicoComplemento">
            {!! Form::label('complemento', 'Complemento') !!}
            {!! Form::text('complemento', old('complemento', (is_numeric(request()->segment(3))) ? $record->complemento : null), ['class' => 'form-control', 'maxlength' => 50]) !!}
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoBairro">
            {!! Form::label('bairro', 'Bairro') !!}
            {!! Form::text('bairro', old('bairro', (is_numeric(request()->segment(3))) ? $record->bairro : null), ['class' => 'form-control', 'maxlength' => 50]) !!}
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3" id="juridicoCidade">
            {!! Form::label('cidade', 'Cidade') !!}
            {!! Form::text('cidade', old('cidade', (is_numeric(request()->segment(3))) ? $record->municipio : null), ['class' => 'form-control', 'maxlength' => 50]) !!}
        </div>
        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2" id="juridicoEstado">
            {!! Form::label('uf', 'UF') !!}
            {!! Form::select('uf', ['' => ''] + Utilities::estados(), old('uf', (is_numeric(request()->segment(3))) ? $record->estado : null), ['id' => 'uf', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
        </div>
    </div>

    <h3 class="box-title m-t-40">Responsável</h3>
    <hr class="m-b-40">

    <div class="row">
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('responsavel_nome', 'Nome') !!}
            {!! Form::text('responsavel_nome', old('responsavel_nome', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
        </div>

        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('responsavel_cpf', 'CPF') !!}
            {!! Form::text('responsavel_cpf', old('responsavel_cpf', null), ['class' => 'form-control mask_cpf', 'maxlength' => 15]) !!}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('responsavel_cargo', 'Cargo') !!}
            {!! Form::text('responsavel_cargo', old('responsavel_cargo', null), ['class' => 'form-control', 'maxlength' => 50]) !!}
        </div>

        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('responsavel_conceituacao', 'Conceituação') !!}
            {!! Form::text('responsavel_conceituacao', old('responsavel_conceituacao', null), ['class' => 'form-control', 'maxlength' => 50]) !!}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('responsavel_contato', 'Contato') !!}
            {!! Form::text('responsavel_contato', old('responsavel_contato', null), ['class' => 'form-control', 'maxlength' => 15]) !!}
        </div>

        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('responsavel_email', 'E-mail') !!}
            {!! Form::text('responsavel_email', old('responsavel_email', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2">
            {!! Form::label('responsavel_cep', 'CEP') !!}
            {!! Form::text('responsavel_cep', old('responsavel_cep', null), ['class' => 'form-control mask_cep', 'maxlength' => 9]) !!}
        </div>
        
        <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
            {!! Form::label('responsavel_logradouro', 'Endereço') !!}
            {!! Form::text('responsavel_logradouro', old('responsavel_logradouro', null), ['class' => 'form-control', 'maxlength' => 80]) !!}
        </div>

        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2">
            {!! Form::label('responsavel_numero', 'Número') !!}
            {!! Form::text('responsavel_numero', old('responsavel_numero', null), ['class' => 'form-control', 'maxlength' => 10]) !!}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
            {!! Form::label('responsavel_complemento', 'Complemento') !!}
            {!! Form::text('responsavel_complemento', old('responsavel_complemento', null), ['class' => 'form-control', 'maxlength' => 50]) !!}
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::label('responsavel_bairro', 'Bairro') !!}
            {!! Form::text('responsavel_bairro', old('responsavel_bairro', null), ['class' => 'form-control', 'maxlength' => 50]) !!}
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::label('responsavel_cidade', 'Cidade') !!}
            {!! Form::text('responsavel_cidade', old('responsavel_cidade', null), ['class' => 'form-control', 'maxlength' => 50]) !!}
        </div>

        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2" id="juridicoEstado">
            {!! Form::label('responsavel_estado', 'UF') !!}
            {!! Form::select('responsavel_estado', ['' => ''] + Utilities::estados(), old('responsavel_estado', null), ['id' => 'responsavel_estado', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
        </div>
    </div>
</fieldset>