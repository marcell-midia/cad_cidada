@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection
@section('page-titles')
<div class="row page-titles">
  <div class="col-md-6 col-8 align-self-center">
    <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="{{ route('empresas.index') }}">{{ $meta['title'] }}</a></li>
      <li class="breadcrumb-item active"><a href="{{ route('empresas.edit', $record->id) }}">{{ $meta['action'] }}</a></li>
    </ol>
</div>
<div class="col-md-6 col-4 align-self-center">
  <div class="btn-group pull-right" role="group" aria-label="Basic example">
    <a href="{{ route('empresas.index') }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar de empresas</a>
    <a href="{{ route('empresas.create') }}" class="btn btn-info"><i class="mdi mdi-plus-circle"></i> Nova empresa</a>
  </div>
</div>
</div>

<div class="row">
  <div class="card">
    <div class="card-body">
      <div class="col-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              @if($record->tipo_cadastro == 1)
              
              @if($record->tipo_modelo == 1)
              <div class="box-header">
                <h3 class="box-title">Modelo Completo P. Jurídica</h3>
              </div>
              {{--  {{ dd($record) }}  --}}
              {{--  Modelo completo  --}}
            
              <tr>
                <td colspan="2"> CNPJ: {{ $record->cpf_cnpj }} </td>  
                <td colspan="1"> Razão social: {{ $record->nome_razaosocial }} </td>
                <td colspan="4"> Nome fantasia: {{ $record->nome_fantasia }} </td>
                <td colspan="4"> Inscrição Estadual: {{ $record->insc_estadual_rg }} </td>
              </tr>
              
              <tr>
                <td colspan="1"> IE/UF: {{ $record->insc_estadual_rg_uf }} </td>
                <td colspan="2"> Inscrição Municipal: {{ $record->insc_municipal }} </td>
                <td colsapan="4"> Controle acionário: {{ $record->present()->getNaturezaControle }} </td>
                <td colsapan="5"> Tipo e unidade: {{ $record->present()->getNaturezaUnidade }} </td>
              </tr>

              <tr>
                <td colsapan="5"> Porte da empresa: {{ $record->present()->getNaturezaPorte }} </td>
                <td colsapan="5"> Capital autorizado: {{ $record->natureza_capital_autorizado }} </td>
                <td colsapan="5"> Natureza jurídica: {{ $record->present()->getNaturezaTipo }} </td>
                <td colspan="6">CNAE: {{ $record->cnae->nome }}</td>
              </tr>

              <tr>
                <td colsapan="5"> Capital social: {{ $record->natureza_capital_social }} </td>
                <td colsapan="5"> Tipo de capital: {{ $record->present()->getNaturezaCapital }} </td>
                <td colsapan="5"> Capital subscrito: {{ $record->natureza_capital_subscrito }} </td>
                <td colsapan="5"> Capital integral: {{ $record->natureza_capital_integral }} </td>
                <td colsapan="6"> Site empreendimento: {{ $record->site }} </td>
              </tr>
              
              <tr>
                <td colsapan="5"> Email: {{ $record->email }} </td>
                <td colsapan="3"> CEP: {{ $record->cep }} </td>
                <td colsapan="5"> Endereço: {{ $record->logradouro }} </td>
                <td colsapan="2"> Número: {{ $record->numero }} </td>
                <td colsapan="5"> Bairro: {{ $record->bairro }} </td>
                <td colsapan="2"> Cidade: {{ $record->municipio }} </td>
                <td colsapan="2"> UF: {{ $record->estado }} </td>
              </tr>
              <tr>
              </tr>
              @else
              
              {{--  Modelo Simplificado  --}}
              <div class="box-header">
                <h3 class="box-title">Modelo Simplificado P. Jurídica</h3>
              </div>
              <tr>
                <td colsapan="5"> Razão social: {{ $record->nome_razaosocial }} </td>
                <td colsapan="5"> Nome Fantasia: {{ $record->nome_fantasia }}</td>
                <td colsapan="5"> CNPJ: {{ $record->cpf_cnpj }}</td>
                <td colsapan="5"> IE/UF: {{ $record->insc_estadual_rg_uf }}</td>
                <td colsapan="5"> Inscrição Estadual (IE): {{ $record->insc_estadual_rg }}</td>
              </tr>
              
              <tr>  
                <td colsapan="5"> Inscrição Municipal (IE): {{ $record->insc_municipal }}</td>
                <td colsapan="5"> Natureza Jurídica: {{ \App\Helpers\Utilities::juridico_natureza($record->tipo_modelo, $record->tipo_cadastro, $record->natureza_tipo) }}</td>
                <td colsapan="5"> CNAE: {{ $record->cnae->nome }} </td>
                <td colsapan="5"> Capital Social: {{ $record->natureza_capital_social }} </td>
                <td colsapan="5"> Esfera da Entidade: {{ $record->associacao_esfera }} </td>
              </tr>
              
              <tr>
                <td colsapan="5"> Tipo de Entidade: {{ $record->associacao_tipo }} </td>
                <td colsapan="5"> Site: {{ $record->site }} </td>
                <td colsapan="5"> Email: {{ $record->email }} </td>
                <td colsapan="5"> CEP: {{ $record->cep }}</td>
                <td colsapan="5"> Endereço: {{ $record->logradouro }} </td>
              </tr>
                      
              <tr>
                <td colsapan="5"> Número: {{ $record->numero }} </td>
                <td colsapan="5"> Bairro: {{ $record->bairro }} </td>
                <td colsapan="5"> Cidade: {{ $record->municipio }} </td>
                <td colsapan="5"> UF: {{ $record->estado }} </td>
              </tr>      

              @endif
              @else
              <div class="box-header">
                <h3 class="box-title">Modelo Simplificado Pessoa Física</h3>
              </div>
              <tr>
                <td colsapan="5"> Nome titular: {{ $record->nome_razaosocial }} </td>
                <td colsapan="5"> CPF: {{ $record->responsavel_cpf }} </td>
                <td colsapan="5"> RG: {{ $record->insc_estadual_rg }} </td>
                <td colsapan="5"> RG/UF: {{ $record->insc_estadual_rg_uf }} </td>
                {{-- <td colsapan="5"> CNAE: {{ $record->cnae->nome }} </td> --}}
              </tr>

              <tr>
                <td colsapan="5"> Site: {{ $record->site }} </td>
                <td colsapan="5"> Email: {{ $record->email }} </td> 
                <td colsapan="5"> CEP: {{ $record->cep }} </td> 
                <td colsapan="5"> Endereço: {{ $record->logradouro }} </td> 
                <td colsapan="5"> Número: {{ $record->numero }} </td> 
              </tr>

              <tr>
                  <td colsapan="5"> Complemento: {{ $record->complemento }} </td>
                  <td colsapan="5"> Bairro: {{ $record->bairro }} </td> 
                  <td colsapan="5"> Cidade: {{ $record->municipio }} </td> 
                  <td colsapan="5"> UF: {{ $record->estado }} </td> 
                  
                </tr>
             @endif
             <!-- /.box-body -->
            </tbody>
          </table>
         
          {{--  Responsável  --}}

          <div class="box-header">
              <h3 class="box-title">Responsável</h3>
          </div>
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tr>
                <td colsapan="5"> Nome: {{ $record->responsavel_nome }} </td>
                <td colsapan="5"> CPF: {{ $record->cpf_cnpj }} </td>
                <td colsapan="5"> Cargo: {{ $record->responsavel_cargo }} </td>
                <td colsapan="5"> Conceituação: {{ $record->responsavel_conceituacao }} </td>
                <td colsapan="5"> Contato: {{ $record->responsavel_contato }} </td>
              </tr>
              
              <tr> 
                <td colsapan="5"> E-mail: {{ $record->responsavel_email }} </td>
              </tr>
              
              <tr>
                <td colsapan="5"> CEP: {{ $record->responsavel_cep }} </td>
                <td colsapan="5"> Endereço: {{ $record->responsavel_logradouro }} </td>
                <td colsapan="5"> Número: {{ $record->responsavel_numero }} </td>
                <td colsapan="5"> Bairro: {{ $record->responsavel_bairro }} </td>
                <td colsapan="5"> Cidade: {{ $record->responsavel_cidade }} </td>
              </tr>
            
              <tr>
                <td colsapan="5"> UF: {{ $record->responsavel_estado }} </td>
              </tr>

            </tbody>
            </table>
        </div>
      </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>		
@endsection
