<fieldset>
    <div class="row">
        <div class="col-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Endereço da atividade</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {!! Form::label('codigo_ageamapa', 'Protocolo da Agência Amapá') !!}
                            {!! Form::text('codigo_ageamapa', old('codigo_ageamapa', null), ['class' => 'form-control mask_protocolo', 'maxlength' => 9]) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            {!! Form::label('cep', 'CEP') !!}
                            {!! Form::text('cep', old('cep', null), ['class' => 'form-control mask_cep', 'maxlength' => 9]) !!}
                        </div>
                        
                        <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            {!! Form::label('logradouro', 'Endereço') !!}
                            {!! Form::text('logradouro', old('logradouro', null), ['class' => 'form-control', 'maxlength' => 80]) !!}
                        </div>

                        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            {!! Form::label('numero', 'Número') !!}
                            {!! Form::text('numero', old('numero', null), ['class' => 'form-control', 'maxlength' => 10]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('complemento', 'Complemento') !!}
                            {!! Form::text('complemento', old('complemento', null), ['class' => 'form-control', 'maxlength' => 50]) !!}
                        </div>

                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('bairro', 'Bairro') !!}
                            {!! Form::text('bairro', old('bairro', null), ['class' => 'form-control', 'maxlength' => 50]) !!}
                        </div>

                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('municipio', 'Município *') !!}
                            {!! Form::text('municipio', old('municipio', null), ['class' => 'form-control', 'maxlength' => 50]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('contato', 'Contato') !!}
                            {!! Form::text('contato', old('contato', null), ['class' => 'form-control mask_phone']) !!}
                        </div>

                        <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            {!! Form::label('email', 'E-mail') !!}
                            {!! Form::text('email', old('email', null), ['class' => 'form-control', 'maxlength' => 200]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('latitude', 'Latitude') !!}
                            {!! Form::text('latitude', old('latitude', null), ['class' => 'form-control mask_geo_l', 'maxlength' => 80]) !!}
                        </div>

                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('longitude', 'Longitude') !!}
                            {!! Form::text('longitude', old('longitude', null), ['class' => 'form-control mask_geo_w', 'maxlength' => 80]) !!}
                        </div>


                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('zona', 'Zona') !!}
                            {!! Form::text('zona', old('zona', null), ['class' => 'form-control', 'maxlength' => 50]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="row">
        <div class="col-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Documento ambiental vigente</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('documento_ambiental', 'Documento ambiental vigente') !!}
                            {!! Form::select('documento_ambiental', ['' => ''] + $documentoAmbientalVigente, old('documento_ambiental', null), ['id' => 'documento_ambiental', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                        </div>

                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('documento_ambiental_outra', 'Outra') !!}
                            {!! Form::text('documento_ambiental_outra', old('documento_ambiental_outra', null), ['class' => 'form-control', 'maxlength' => '100']) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('documento_ambiental_numero', 'Nº do documento') !!}
                            {!! Form::text('documento_ambiental_numero', old('documento_ambiental_numero', null), ['class' => 'form-control', 'maxlength' => 20]) !!}
                        </div>

                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('documento_ambiental_data', 'Data de expedição') !!}
                            {!! Form::date('documento_ambiental_data', old('documento_ambiental_data', null), ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('documento_ambiental_validade', 'Validade do documento') !!}
                            {!! Form::date('documento_ambiental_validade', old('documento_ambiental_validade', null), ['class' => 'form-control']) !!}
                        </div>

                       {{-- add --}}


                        <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-12">
                            <div class="card card-outline-inverse" style="border: 1px solid rgba(44,43,46,.75);">
                                <div class="card-header">
                                    <h4 class="m-b-0 text-white">Ações em Execução</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                            <div class="row">
                                                <div class="form-group col-12">
                                                    {!! Form::label('acao_execucao', 'Ações em execução') !!}
                                                    {!! Form::select('acao_execucao[]', $acaoExecucao->toArray(), old('acao_execucao', (count($areaAcaoExecucoes)) ? $areaAcaoExecucoes->toArray() : []), ['id' => 'acao_execucao', 'class' => 'form-control substancia-mineral-options', 'multiple' => true]) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                            <div class="row">
                                                <div class="form-group col-12">
                                                    {!! Form::label('acao_execucao_outra', 'Outra') !!}
                                                    {!! Form::text('acao_execucao_outra', old('acao_execucao_outra', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

{{-- add barra --}}

    <div class="row">
        <div class="col-12">
            <div class="card card-outline-info">
                <div class="card-header">
            <h4 class="m-b-0 text-white">Informações sobre o processo junto ao ANM</h4>
                </div>
                <hr class="m-b-20">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        {!! Form::label('dnpm_requerente', 'Titular ou requerente (Nome ou razão social)') !!}
                        {!! Form::text('dnpm_requerente', old('dnpm_requerente', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                    </div>

                    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        {!! Form::label('dnpm_numero', 'Número do processo') !!}
                        {!! Form::text('dnpm_numero', old('dnpm_numero', null), ['class' => 'form-control', 'maxlength' => 30]) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        {!! Form::label('dnpm_ano', 'Ano do processo') !!}
                        {!! Form::text('dnpm_ano', old('dnpm_ano', null), ['class' => 'form-control', 'maxlength' => 4]) !!}
                    </div>

                    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        {!! Form::label('dnpm_numero_documento', 'Nº do documento') !!}
                        {!! Form::text('dnpm_numero_documento', old('dnpm_numero_documento', null), ['class' => 'form-control', 'maxlength' => 30]) !!}
                    </div>

                    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        {!! Form::label('dnpm_numero_validade', 'Validade do documento') !!}
                        {!! Form::date('dnpm_numero_validade', old('dnpm_numero_validade', null), ['class' => 'form-control', 'maxlength' => 30]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="row">
        <div class="col-12">
            <div class="card card-outline-inverse" style="border: 1px solid rgba(44,43,46,.75);">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Substância(s) Mineral(s)</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <div class="row">
                                <div class="form-group col-12">
                                    {!! Form::label('dnpm_substancia', 'Substância(s) mineral(s)') !!}
                                    {!! Form::select('dnpm_substancia[]', $substanciaMineral->toArray(), old('dnpm_substancia', (count($areaSubstancias)) ? $areaSubstancias->toArray() : []), ['id' => 'dnpm_substancia', 'class' => 'form-control substancia-mineral-options', 'multiple' => true]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                            <div class="row">
                                <div class="form-group col-12">
                                    {!! Form::label('dnpm_substancia_outra', 'Outra') !!}
                                    {!! Form::text('dnpm_substancia_outra', old('dnpm_substancia_outra', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-12">
                                    {!! Form::label('dnpm_area', 'Área(HA)') !!}
                                    {!! Form::text('dnpm_area', old('dnpm_area', null), ['class' => 'form-control', 'maxlength' => 50]) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    {!! Form::label('anm_operacional', 'Situação operacional') !!}
                    {!! Form::select('anm_operacional', ['' => ''] + $anmOperacionalTipo, old('anm_operacional', null), ['id' => 'anm_operacional', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                </div>
        
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    {!! Form::label('dnpm_fase', 'Fase do processo mineral') !!}
                    {!! Form::select('dnpm_fase', ['' => ''] + $faseProcessoMineral, old('dnpm_fase', null), ['id' => 'dnpm_fase', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                </div>
            </div>
        </div>
	</div>

    </div>


    <div class="row">
        <div class="col-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Exploração mineral (Por substância)</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('exploracao_pesquisa', 'Substância explorada') !!}
                            {!! Form::text('exploracao_pesquisa', old('exploracao_pesquisa', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>

                        {{-- Add_multi_select 1 --}}

                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_lavra_aberto', 'Lavra a céu aberto') !!}
                            {!! Form::select('exploracao_lavra_aberto[]', $lavraCeuAberto, old('exploracao_lavra_aberto', (app('request')->route()->action['as'] == 'empresas.editararea') ? $exploracaoLavraAbertos : null), ['id' => 'exploracao_lavra_aberto', 'class' => 'form-control select2', 'style' => 'width: 100%;', 'multiple' => true, 'data-placeholder' => 'Selecione uma opção']) !!}

                        </div>

                         {{-- {Add_multi_select} 2 --}}

                         <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                             {!! Form::label('exploracao_lavra_subterranea', 'Lavra subterrânea') !!}
                             {!! Form::select('exploracao_lavra_subterranea[]', $lavraSubterranea, old('exploracao_lavra_subterranea', (app('request')->route()->action['as'] == 'empresas.editararea') ? $exploracaoLavraSubterraneas : null), ['id' => 'exploracao_lavra_subterranea', 'class' => 'form-control select2', 'style' => 'width: 100%;', 'multiple' => true, 'data-placeholder' => 'Selecione uma opção']) !!}

                         </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('exploracao_desmonte', 'Tipo de desmonte') !!}
                            {!! Form::select('exploracao_desmonte[]', $tipoDesmonte, old('exploracao_desmonte', (app('request')->route()->action['as'] == 'empresas.editararea') ? $exploracaoDesmontes : null), ['id' => 'exploracao_desmonte', 'class' => 'form-control select2', 'style' => 'width: 100%;', 'multiple' => true, 'data-placeholder' => 'Selecione uma opção']) !!}

                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('exploracao_rejeito', 'Destinação dos rejeitos') !!}
                            {!! Form::select('exploracao_rejeito[]', $destinacaoRejeito, old('exploracao_rejeito', (app('request')->route()->action['as'] == 'empresas.editararea') ? $exploracaoRejeitos : null), ['id' => 'exploracao_rejeito', 'class' => 'form-control select2', 'style' => 'width: 100%;', 'multiple' => true, 'data-placeholder' => 'Selecione uma opção']) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('exploracao_rejeito_outra', 'Outra') !!}
                            {!! Form::text('exploracao_rejeito_outra', old('exploracao_rejeito_outra', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                    </div>

                    {{-- ADD Add_multi_select 3--}}

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_esteril', 'Destinação do estéril') !!}
                            {!! Form::select('exploracao_esteril[]', $tipoEsteril, old('exploracao_esteril', (app('request')->route()->action['as'] == 'empresas.editararea') ? $exploracaoEsterios : null), ['id' => 'exploracao_esteril', 'class' => 'form-control select2', 'style' => 'width: 100%;', 'multiple' => true, 'data-placeholder' => 'Selecione uma opção']) !!}

                        </div>

                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_esteril_outra', 'Outra') !!}
                            {!! Form::text('exploracao_esteril_outra', old('exploracao_esteril_outra', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_transporte', 'Tipo de transporte') !!}
                            {!! Form::select('exploracao_transporte[]', $tipoTransporte, old('exploracao_transporte', (app('request')->route()->action['as'] == 'empresas.editararea') ? $exploracaoTransportes : null), ['id' => 'exploracao_transporte', 'class' => 'form-control select2', 'style' => 'width: 100%;', 'multiple' => true, 'data-placeholder' => 'Selecione uma opção']) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_numero_viagem', 'Nº de viagens/mês') !!}
                            {!! Form::text('exploracao_numero_viagem', old('exploracao_numero_viagem', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('exploracao_producao_periodo', 'Período de produção') !!}
                            {!! Form::text('exploracao_producao_periodo', old('exploracao_producao_periodo', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            {!! Form::label('exploracao_producao_tempo', 'Tempo') !!}
                            {!! Form::number('exploracao_producao_tempo', old('exploracao_producao_tempo', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            {!! Form::label('exploracao_producao_tempo_periodo', 'Período') !!}
                            {!! Form::text('exploracao_producao_tempo_periodo', old('exploracao_producao_tempo_periodo', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>

                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('exploracao_producao_unidade', 'Unidade de produção') !!}
                            {!! Form::select('exploracao_producao_unidade', ['' => ''] + $unidadeProducao, old('exploracao_producao_unidade', null), ['id' => 'exploracao_producao_unidade', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_qtde_produzida', 'Quantidade produzida') !!}
                            {!! Form::text('exploracao_qtde_produzida', old('exploracao_qtde_produzida', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_teor', 'Teor(concentração)') !!}
                            {!! Form::text('exploracao_teor', old('exploracao_teor', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_qtde_comercializada', 'Quantidade Comercializada') !!}
                            {!! Form::text('exploracao_qtde_comercializada', old('exploracao_qtde_comercializada', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_preco_unidade', 'Preço por unidade') !!}
                            {!! Form::text('exploracao_preco_unidade', old('exploracao_preco_unidade', null), ['class' => 'form-control mask_money', 'maxlength' => 100]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_cfem_recolhido', 'Valor rec. de CFEM no período') !!}
                            {!! Form::text('exploracao_cfem_recolhido', old('exploracao_cfem_recolhido', null), ['class' => 'form-control mask_money', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_cfem_data', 'data do recolhimento (CFEM)') !!}
                            {!! Form::date('exploracao_cfem_data', old('exploracao_cfem_recolhido', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_icms_recolhido', 'Valor rec. de ICMS no período') !!}
                            {!! Form::text('exploracao_icms_recolhido', old('exploracao_icms_recolhido', null), ['class' => 'form-control mask_money', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            {!! Form::label('exploracao_icms_data', 'data do recolhimento (ICMS)') !!}
                            {!! Form::date('exploracao_cfem_data', old('exploracao_cfem_recolhido', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('exploracao_producao_destino', 'Principal destino da produção') !!}
                            {!! Form::select('exploracao_producao_destino', ['' => ''] + $destinoProducao, old('exploracao_producao_destino', null), ['id' => 'exploracao_producao_destino', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            {!! Form::label('exploracao_principal_comprador', 'Principal comprador') !!}
                            {!! Form::text('exploracao_principal_comprador', old('exploracao_principal_comprador', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                        {{-- ADD AQUI --}}
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('capacidade_instalada', 'Capacidade total instalada (ROM)') !!}
                            {!! Form::text('capacidade_instalada', old('capacidade_instalada', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('vida_util_mina', 'Vida últil restante da jazida/mina') !!}
                            {!! Form::text('vida_util_mina', old('vida_util_mina', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            {!! Form::label('relacao_minerio', 'Relação Estério/minério realizada') !!}
                            {!! Form::text('relacao_minerio', old('relacao_minerio', null), ['class' => 'form-control', 'maxlength' => 100]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-12">
            <div class="card card-outline-inverse" style="border: 1px solid rgba(44,43,46,.75);">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Reserva de minério/substância</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('reserva_medida', 'Medida') !!}
                            {!! Form::number('reserva_medida', old('reserva_medida', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('unidade_medida', 'Unidade') !!}
                            {!! Form::select('unidade_medida', ['' => ''] + $unidadeMedida, old('unidade_medida', null), ['id' => 'unidade_medida', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('reserva_indicada', 'Indicada') !!}
                            {!! Form::number('reserva_indicada', old('reserva_indicada', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('unidade_indicada', 'Unidade') !!}
                            {!! Form::select('unidade_indicada', ['' => ''] + $unidadeIndicada, old('unidade_indicada', null), ['id' => 'unidade_indicada', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('reserva_inferida', 'Inferida') !!}
                            {!! Form::number('reserva_inferida', old('reserva_inferida', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('unidade_inferida', 'Unidade') !!}
                            {!! Form::select('unidade_inferida', ['' => ''] + $unidadeInferida, old('unidade_inferida', null), ['id' => 'unidade_inferida', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('reserva_lavravel', 'Lavrável') !!}
                            {!! Form::number('reserva_lavravel', old('reserva_lavravel', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                        </div>
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            {!! Form::label('unidade_lavravel', 'Unidade') !!}
                            {!! Form::select('unidade_lavravel', ['' => ''] + $unidadeLavravel, old('unidade_lavravel', null), ['id' => 'unidade_lavravel', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-outline-info" style="border: 1px solid #009efb;">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Mão de obra envolvida</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @if($empresa->tipo_modelo == 2)
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                {!! Form::label('mao_obra_numero_cooperado', 'Nº de cooperados/Associados') !!}
                                {!! Form::number('mao_obra_numero_cooperado', old('mao_obra_numero_cooperado', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                            </div>
                        @endif
                        <div class="form-group col-xs-12 @if($empresa->tipo_modelo == 1) col-sm-6 col-md-6 col-lg-6 @else col-sm-4 col-md-4 col-lg-4 @endif">
                            {!! Form::label('mao_obra_empregado_direto', 'Mão de obra total empregada direita') !!}
                            {!! Form::number('mao_obra_empregado_direto', old('mao_obra_empregado_direto', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                        </div>
                        <div class="form-group col-xs-12 @if($empresa->tipo_modelo == 1) col-sm-6 col-md-6 col-lg-6 @else col-sm-4 col-md-4 col-lg-4 @endif">
                            {!! Form::label('mao_obra_empregado_terceirizado', 'Mão de obra total empregada terceirizada') !!}
                            {!! Form::number('mao_obra_empregado_terceirizado', old('mao_obra_empregado_terceirizado', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                        </div>
                    </div>

                    @if($empresa->tipo_modelo == 1)
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                {!! Form::label('mao_obra_empregado_pesquisa', 'Nº de trabalhadores empregados na atividade de pesquisa') !!}
                                {!! Form::number('mao_obra_empregado_pesquisa', old('mao_obra_empregado_pesquisa', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                {!! Form::label('mao_obra_empregado_lavra', 'Nº de trabalhadores empregados na atividade lavra') !!}
                                {!! Form::number('mao_obra_empregado_lavra', old('mao_obra_empregado_lavra', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                {!! Form::label('mao_obra_empregado_beneficiamento', 'Nº de trabalhadores empregados no beneficiamento') !!}
                                {!! Form::number('mao_obra_empregado_beneficiamento', old('mao_obra_empregado_beneficiamento', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                {!! Form::label('mao_obra_empregado_meio_ambiente', 'Nº de trabalhadores empregados no meio ambiente') !!}
                                {!! Form::number('mao_obra_empregado_meio_ambiente', old('mao_obra_empregado_meio_ambiente', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                {!! Form::label('mao_obra_empregado_seguranca', 'Nº de trabalhadores empregados na saúde e segurança ocupacional') !!}
                                {!! Form::number('mao_obra_empregado_seguranca', old('mao_obra_empregado_seguranca', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                {!! Form::label('mao_obra_empregado_suporte', 'Nº de trabalhadores empregados na administração e suporte') !!}
                                {!! Form::number('mao_obra_empregado_suporte', old('mao_obra_empregado_suporte', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <div class="card card-outline-inverse" style="border: 1px solid rgba(44,43,46,.75);">
                                <div class="card-header">
                                    <h4 class="m-b-0 text-white">Nº de trabalhadores por idade</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            {!! Form::label('mao_obra_funcionario_15', 'De 15 a 17 anos') !!}
                                            {!! Form::number('mao_obra_funcionario_15', old('mao_obra_funcionario_15', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            {!! Form::label('mao_obra_funcionario_18', 'De 18 a 24 anos') !!}
                                            {!! Form::number('mao_obra_funcionario_18', old('mao_obra_funcionario_18', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            {!! Form::label('mao_obra_funcionario_25', 'De 25 a 49 anos') !!}
                                            {!! Form::number('mao_obra_funcionario_25', old('mao_obra_funcionario_25', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            {!! Form::label('mao_obra_funcionario_45', 'Mais de 50 anos') !!}
                                            {!! Form::number('mao_obra_funcionario_45', old('mao_obra_funcionario_45', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="card card-outline-inverse" style="border: 1px solid rgba(44,43,46,.75);">
                                <div class="card-header">
                                    <h4 class="m-b-0 text-white">Nº de trabalhadores por sexo</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            {!! Form::label('mao_obra_funcionario_masculino', 'Masculino') !!}
                                            {!! Form::number('mao_obra_funcionario_masculino', old('mao_obra_funcionario_masculino', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            {!! Form::label('mao_obra_funcionario_feminino', 'Feminino') !!}
                                            {!! Form::number('mao_obra_funcionario_feminino', old('mao_obra_funcionario_feminino', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($empresa->tipo_modelo == 1)
                        <div class="row">
                            <div class="col-12">
                                <div class="card card-outline-inverse" style="border: 1px solid rgba(44,43,46,.75);">
                                    <div class="card-header">
                                        <h4 class="m-b-0 text-white">Número de Funcionários por grau de instrução e Salário Médio</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                {!! Form::label('mao_obra_funcionario_sem_instrucao', 'Nº funcionários sem instruções') !!}
                                                {!! Form::number('mao_obra_funcionario_sem_instrucao', old('mao_obra_funcionario_sem_instrucao', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                {!! Form::label('mao_obra_funcionario_sem_instrucao_salario', 'Valor média (R$)') !!}
                                                {!! Form::text('mao_obra_funcionario_sem_instrucao_salario', old('mao_obra_funcionario_sem_instrucao_salario', null), ['class' => 'form-control mask_money']) !!}
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                {!! Form::label('mao_obra_funcionario_fundamental_incompleto', 'Nº funcionários com ensino fundamental incompleto') !!}
                                                {!! Form::number('mao_obra_funcionario_fundamental_incompleto', old('mao_obra_funcionario_fundamental_incompleto', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                {!! Form::label('mao_obra_funcionario_fundamental_incompleto_salario', 'Valor média (R$)') !!}
                                                {!! Form::text('mao_obra_funcionario_fundamental_incompleto_salario', old('mao_obra_funcionario_fundamental_incompleto_salario', null), ['class' => 'form-control mask_money']) !!}
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                {!! Form::label('mao_obra_funcionario_fundamental_completo', 'Nº funcionários com ensino fundamental completo') !!}
                                                {!! Form::number('mao_obra_funcionario_fundamental_completo', old('mao_obra_funcionario_fundamental_completo', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                {!! Form::label('mao_obra_funcionario_fundamental_completo_salario', 'Valor média (R$)') !!}
                                                {!! Form::text('mao_obra_funcionario_fundamental_completo_salario', old('mao_obra_funcionario_fundamental_completo_salario', null), ['class' => 'form-control mask_money']) !!}
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                {!! Form::label('mao_obra_funcionario_medio_incompleto', 'Nº funcionários com ensino médio incompleto') !!}
                                                {!! Form::number('mao_obra_funcionario_medio_incompleto', old('mao_obra_funcionario_medio_incompleto', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                {!! Form::label('mao_obra_funcionario_medio_incompleto_salario', 'Valor média (R$)') !!}
                                                {!! Form::text('mao_obra_funcionario_medio_incompleto_salario', old('mao_obra_funcionario_medio_incompleto_salario', null), ['class' => 'form-control mask_money']) !!}
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                {!! Form::label('mao_obra_funcionario_medio_completo', 'Nº funcionários com ensino médio completo') !!}
                                                {!! Form::number('mao_obra_funcionario_medio_completo', old('mao_obra_funcionario_medio_completo', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                {!! Form::label('mao_obra_funcionario_medio_completo_salario', 'Valor média (R$)') !!}
                                                {!! Form::text('mao_obra_funcionario_medio_completo_salario', old('mao_obra_funcionario_medio_completo_salario', null), ['class' => 'form-control mask_money']) !!}
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                {!! Form::label('mao_obra_funcionario_superior_incompleto', 'Nº funcionários com ensino superior incompleto') !!}
                                                {!! Form::number('mao_obra_funcionario_superior_incompleto', old('mao_obra_funcionario_superior_incompleto', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                {!! Form::label('mao_obra_funcionario_superior_incompleto_salario', 'Valor média (R$)') !!}
                                                {!! Form::text('mao_obra_funcionario_superior_incompleto_salario', old('mao_obra_funcionario_superior_incompleto_salario', null), ['class' => 'form-control mask_money']) !!}
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                                {!! Form::label('mao_obra_funcionario_superior_completo', 'Nº funcionários com ensino superior completo') !!}
                                                {!! Form::number('mao_obra_funcionario_superior_completo', old('mao_obra_funcionario_superior_completo', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                {!! Form::label('mao_obra_funcionario_superior_completo_salario', 'Valor média (R$)') !!}
                                                {!! Form::text('mao_obra_funcionario_superior_completo_salario', old('mao_obra_funcionario_superior_completo_salario', null), ['class' => 'form-control mask_money']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                {!! Form::label('mao_obra_qualificacao_profissional', 'Oferta qualificação profissional anual') !!}
                                {!! Form::select('mao_obra_qualificacao_profissional', [0 => 'NÃO', 1 => 'SIM'], old('mao_obra_qualificacao_profissional', null), ['id' => 'mao_obra_qualificacao_profissional', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                {!! Form::label('mao_obra_qualificacao_profissional_numero', 'Nº de profissionais qualificados anaualmente') !!}
                                {!! Form::number('mao_obra_qualificacao_profissional_numero', old('mao_obra_qualificacao_profissional_numero', null), ['class' => 'form-control', 'min' => 0, 'max' => 999999]) !!}
                            </div>
                        </div>
                    @endif


                    @if($empresa->tipo_modelo == 2)
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                {!! Form::label('mao_obra_contratacao', 'Forma de contratação dos trabalhadores') !!}
                                {!! Form::select('mao_obra_contratacao', ['' => ''] + $formaContratacao, old('mao_obra_contratacao', null), ['id' => 'mao_obra_contratacao', 'class' => 'form-control select2', 'style' => 'width: 100%;']) !!}
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                {!! Form::label('mao_obra_contratacao_outra', 'Outra') !!}
                                {!! Form::text('mao_obra_contratacao_outra', old('mao_obra_contratacao_outra', null), ['class' => 'form-control', 'maxlength' => 100, 'disabled' => true]) !!}
                            </div>
                            <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                {!! Form::label('mao_obra_medio_pago', 'Valor médio pago aos trabalhadores por mês(R$)') !!}
                                {!! Form::text('mao_obra_medio_pago', old('mao_obra_medio_pago', null), ['class' => 'form-control mask_money', 'maxlength' => 100]) !!}

                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</fieldset>