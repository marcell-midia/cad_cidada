@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('search-main') @include('includes.administrativo.search') @endsection

@section('page-titles')
    <div class="row page-titles">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.index') }}">Empresa</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.edit', $empresa->id) }}">{{ ucwords(mb_strtolower($empresa->nome_razaosocial)) }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.exibirareas', $empresa->id) }}">{{ $meta['title'] }}</a></li>
            </ol>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 align-self-center">
            <div class="btn-group pull-right" role="group" aria-label="Basic example">
                <a href="{{ route('empresas.index') }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar de empresas</a>
                <a href="{{ route('empresas.cadastrararea', $empresa->id) }}" class="btn btn-info"><i class="mdi mdi-plus-circle"></i> Novo área</a>
            </div>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <span class="badge badge-secondary">Modelo</span> <br>
                            @if($empresa->tipo_modelo == 1)
                                Completo
                            @else
                                Simplificado
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <span class="badge badge-secondary">Tipo de pessoa</span> <br>
                            @if($empresa->tipo_modelo == 1)
                                P. Jurídica
                            @else
                                P. Física
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <span class="badge badge-secondary">
                                @if($empresa->tipo_cadastro == 1)
                                    Razão social
                                @else
                                    Nome titular
                                @endif
                            </span> <br>
                            {{ $empresa->nome_razaosocial }}
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <span class="badge badge-secondary">
                                @if($empresa->tipo_cadastro == 1)
                                    CNPJ
                                @else
                                    CPF
                                @endif
                            </span> <br>
                            {{ $empresa->cpf_cnpj }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            @if($errors->any())
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Erro:</strong> {{$errors->first()}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
            @endif
            @if(session()->has('mensagem'))
                <div class="alert alert-success">{{ session()->get('messagem') }}</div>
            @endif
                                    
            <div class="card">
                <div class="card-body">
                    @if( count($records) > 0 )
                        @php
                        $pages = (($records->total() % $records->perPage()) > 0) ? (int)($records->total() / $records->perPage()) + 1 : ($records->total() / $records->perPage());
                        $pag = max(min($pages, $records->currentPage()), 1);
                        $start = ($pag - 1) * $records->perPage();
                        $pluralTotal = ( $records->total() <= 1 ) ? '' : 's';
                        @endphp

                        <div class="alert alert-dark mb-5" role="alert">
                        Exibindo {{ min($records->total(), ($start + 1)) }} - {{ min($records->total(), ($start + $records->perPage())) }} de {{ $records->total() }} registro{{ $pluralTotal }} cadastrado{{ $pluralTotal }}
                        </div>

                        <table class="table text-center table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Certificação</th>
                                    <th>Código</th>
                                    <th>Nº Protocolo</th>
                                    <th>ANM</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($records as $record)
                                    <tr>
                                @php
                                $presidente = \App\ParecerPresidente::where([['empresa_id', $record->empresa_id], ['area_id', $record->id]])->first();
                                //$certificate = (count($certificate) == 1) ? $certificate->first() : null;
                                @endphp
                                            @isset($presidente->tipo)
                                                @if($presidente->tipo == 1)
                                                    <td><a class="btn btn-success btn-sm" href="{{ route('empresas.emitirCertificado', [$record->empresa_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom">Emitir certificado</a></td>
                                                @else
                                                    <td class="text-white"><button class="btn btn-sm btn-danger btn-small" data-toggle="tooltip" data-placement="top" title="Não aprovado">Não elegível</button></td>
                                                @endif
                                            @endisset
                                            @empty($presidente->tipo)
                                                    <td class="text-white"><button class="btn btn-sm btn-danger btn-small" data-toggle="tooltip" data-placement="top" title="Parecer do presidente Pendente">Não elegível</button></td>
                                            @endempty
                                        <td>{{ mb_strtoupper(\Hashids::encode($record->id)) }}</td>
                                        <td>{{ $record->codigo_ageamapa }}</td>
                                        <td>{{ $record->dnpm_numero }}</td>
                                        <td>
                                            <a class="btn btn-secondary btn-circle" href="{{ route('empresas.editararea', [$record->empresa_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            {{-- @php
                                                dd($record);
                                            @endphp --}}
                                            {{--  <a class="btn btn-secondary btn-circle" href="{{ route('empresas.exibirpareceres', [$record->empresa_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Parecer técnino">  --}}
                                                {{--  <i class="fa fa-check-square-o"></i>  --}}
                                            </a>
                                            <a class="btn btn-danger btn-circle" href="{{ route('empresas.deletararea', [$record->empresa_id, $record->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Deletar"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if( !empty(request()->get('search')) )
                            <p><a class="btn btn-rounded btn-info" href="{{ route('empresas.exibirareas', $empresa->id) }}"  style="color:#fff;"><i class="fa fa-reply"></i> Exibir
                              todos os registros</a></p>
                        @endif
                    @else
                        @if(request()->get('search'))
                            <div class="alert alert-info">
                                Nenhum resultado encontrado com o termo <strong>{{ request()->get('search') }}</strong>.
                            </div>

                            <p>
                                <a class="btn btn-block btn-info" href="{{ route('empresas.exibirareas', $empresa->id) }}">
                                    <i class="fa fa-reply"></i> Voltar
                                </a>
                            </p>
                        @else
                            <div class="alert alert-info">Não existem registros cadastrados.</div>
                        @endif
                    @endif
                </div>
            </div>      
        </div>
    </div>
@endsection