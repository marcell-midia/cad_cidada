@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection
@section('page-titles')
	<div class="row page-titles">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.index') }}">Empresa</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.edit', $empresa->id) }}">{{ ucwords(mb_strtolower($empresa->nome_razaosocial)) }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.exibirareas', $empresa->id) }}">{{ $meta['title'] }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('empresas.cadastrararea', $empresa->id) }}">{{ $meta['action'] }}</a></li>
            </ol>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 align-self-center">
            <div class="btn-group pull-right" role="group" aria-label="Basic example">
                <a href="{{ route('empresas.index') }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar de empresas</a>
                <a href="{{ route('empresas.exibirareas', $empresa->id) }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar de áreas</a>
                <a href="{{ route('empresas.cadastrararea', $empresa->id) }}" class="btn btn-info"><i class="mdi mdi-plus-circle"></i> Nova área</a>
            </div>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <span class="badge badge-secondary">Modelo</span> <br>
                            @if($empresa->tipo_modelo == 1)
                                Completo
                            @else
                                Simplicado
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <span class="badge badge-secondary">Tipo de pessoa</span> <br>
                            @if($empresa->tipo_modelo == 1)
                                P. Jurídica
                            @else
                                P. Física
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <span class="badge badge-secondary">
                                @if($empresa->tipo_cadastro == 1)
                                    Razão social
                                @else
                                    Nome titular
                                @endif
                            </span> <br>
                            {{ $empresa->nome_razaosocial }}
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <span class="badge badge-secondary">
                                @if($empresa->tipo_cadastro == 1)
                                    CNPJ
                                @else
                                    CPF
                                @endif
                            </span> <br>
                            {{ $empresa->cpf_cnpj }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @if( $errors->any() )
                                <div class="alert alert-danger">
                                    Preencha corretamente os campos abaixo: <br>
                                    @foreach( $errors->all() as $error )
                                        - {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                            
                            {!! Form::open(['route' => ['empresas.salvararea', $empresa->id], 'method' => 'post', 'class' => 'form']) !!}
                                @include('painel.area.form')
                                @include('layouts.parties.form_buttom')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('.mask_protocolo').mask('00.000.00000/0000', {reverse: true});
            $('.mask_phone').mask('(00) 0000-0000', {reverse: true});
            $('.mask_geo_l').mask('000°00\'00\'\'Z', {
                translation: {
                    'Z': {
                        pattern: /[ENSW]/
                    }
                }
            });
            $('.mask_geo_w').mask('000°00\'00\'\'Z', {
                translation: {
                    'Z': {
                        pattern: /[ENSW]/
                    }
                }
            });
            $('.mask_cep').mask('00000-000', {reverse: true});
            $('.mask_date').mask('00/00/0000', {reverse: true});
            $('.mask_money').mask('000.000.000.000.000,00', {reverse: true});
            $('.mask_mixed').mask('AAA 000-S0S');
            $('.substancia-mineral-options').multiSelect();
            $('.acao-execucao-options').multiSelect();
            $('.select2').select2();
            var masks = ['(00) 00000-0000', '(00) 0000-00009'], maskBehavior = function(val, e, field, options) {
                return val.length > 14 ? masks[0] : masks[1];
            }
            $('.mask_phone').mask(maskBehavior, {
                onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior(val, e, field, options), options);
                }
            });

            //Quando o campo cep perde o foco.
            $('input[name=cep]').blur(function() {
                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');
                //Verifica se campo cep possui valor informado.
                if (cep != "") {
                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;
                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {
                        //Preenche os campos com "..." enquanto consulta webservice.
                        $('input[name=logradouro]').val("...");
                        $('input[name=numero]').val("...");
                        $('input[name=complemento]').val("...");
                        $('input[name=bairro]').val("...");
                        $('input[name=municipio]').val("...");
                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $('input[name=logradouro]').val(dados.logradouro.toUpperCase());
                                $('input[name=bairro]').val(dados.bairro.toUpperCase());
                                $('input[name=municipio]').val(dados.localidade.toUpperCase());
                                $('input[name=numero]').val('');
                                $('input[name=numero]').focus();
                                $('input[name=complemento]').val('');
                                $('select[name=estado]').children('option[value="'+dados.uf+'"]').attr("selected", true);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                $('input[name=logradouro]').val('');
                                $('input[name=numero]').val('');
                                $('input[name=complemento]').val('');
                                $('input[name=bairro]').val('');
                                $('input[name=municipio]').val('');
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        $('input[name=logradouro]').val('');
                        $('input[name=numero]').val('');
                        $('input[name=complemento]').val('');
                        $('input[name=bairro]').val('');
                        $('input[name=municipio]').val('');
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    $('input[name=logradouro]').val('');
                    $('input[name=numero]').val('');
                    $('input[name=complemento]').val('');
                    $('input[name=bairro]').val('');
                    $('input[name=municipio]').val('');
                }
            });
        });
    </script>
@endsection