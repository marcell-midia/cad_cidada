@extends('layouts.panel')

@section('title') {{ $meta['title'] }} @endsection

@section('page-titles')
	<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ $meta['title'] }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('usuarios.index') }}">{{ $meta['title'] }}</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('usuarios.create') }}">{{ $meta['action'] }}</a></li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
        	<div class="btn-group pull-right" role="group" aria-label="Basic example">
                <a href="{{ route('usuarios.index') }}" class="btn btn-info"><i class="fa fa-mail-reply-all"></i> Listar de usuários</a>
                <a href="{{ route('usuarios.create') }}" class="btn btn-info"><i class="mdi mdi-plus-circle"></i> Novo usuário</a>
            </div>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card bg-light">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @if( $errors->any() )
                                <div class="alert alert-danger">
                                    Preencha corretamente os campos abaixo: <br>
                                    @foreach( $errors->all() as $error )
                                        - {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                            
                            {!! Form::model($record, ['route' => ['usuarios.update', $record->id], 'method' => 'put', 'class' => 'form']) !!}
                                @include('painel.usuario.form')
						<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<label id="active_label">Ativo?</label>
							<input type="hidden" value="0" checked name="ativo">
							@if($record->ativo)
							<input type="checkbox" value="1" checked name="ativo">
							@else
							<input type="checkbox" value="1" name="ativo">
							@endif
							@if(($record->updated_at == $record->created_at) && !$record->ativo)
							<h6 class="text-muted">Esse usuário é novo. Ativá-lo enviará um email de confirmação</h6>
							@endif
						</div>
                        @if(!$record->email_verified_at)
                            <p>Este usuario ainda utiliza a senha padrão</p>
                        @else
    <p class="text-center">Clique <a class="text-primary" href="{{ route('usuarios.resetarsenha', $record->id) }}" data-toggle="tooltip" data-placement="bottom" title="Resetar Senha">aqui</a> para resetar a senha deste usuário</p>
                        @endif
                                @include('layouts.parties.form_buttom')

									
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
@endsection

@section('scripts')
$('.cnpj').mask('00.000.000/0000-00', {reverse: true});
$('.login').mask('000.000.000-00', {reverse: true});
$(".form").submit(function() {
  $(".login").unmask();
  $(".cnpj").unmask();
});
@endsection
