<fieldset>
	<div class="row">
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('name', 'Nome *') !!}
            {!! Form::text('name', old('name', null), ['class' => 'form-control', 'maxlength' => '50']) !!}
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('email', 'E-mail *') !!}
            {!! Form::text('email', old('email', null), ['class' => 'form-control', 'maxlength' => '100']) !!}
        </div>
	</div>
	<div class="row">
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('telefone_1', 'Telefone 1 *') !!}
            {!! Form::text('telefone_1', old('telefone_1', null), ['class' => ['form-control','telefone'], 'maxlength' => '11']) !!}
        </div>
		<div class="form-group col-xs-12 col-sm-12 col-lg-6">
            {!! Form::label('local_id', 'Local *') !!}
            <select required class="select2 form-control" name="local_id">
                    @if(! isset($record))
                    <option disabled selected value="">Ponto de entrega</option>
                    @endif
                @foreach($locais as $local)
                    @if(isset($record))
                        @if($record->local_id == $local->id)
                            <option selected value="{{$local->id}}">{{$local->local}}</option>
                        @else
                            <option value="{{$local->id}}">{{$local->local}}</option>
                        @endif
                    @else
                            <option value="{{$local->id}}">{{$local->local}}</option>
                @endif
                @endforeach
            </select>
	</div>
    </div>

	<div class="row">
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('cpf', 'CPF *') !!}
            {!! Form::text('cpf', old('cpf', null), ['class' => 'form-control cpf', 'maxlength' => '50']) !!}
        </div>
		<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
            {!! Form::label('role_id', 'Perfil *') !!}
            {!! Form::select('role_id', ['' => 'Selecione'] + $roles->toArray(), old('role_id', (app('request')->route()->action['as'] == 'usuarios.edit') ? $role : null), ['id' => 'role_id', 'class' => 'form-control', 'style' => 'width: 100%;', 'onChange' => 'toggleConcedentes()' ]) !!}
        </div>
    </div>


    <div id="concedentes-form" class="row">
        <div class="col-12">
            <div class="card bg-light card-outline-inverse" style="border:;">
                <div class="card-header">
                    <h4 class="m-b-0 text-white text-center">Concedentes Atribuídos</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <div class="row">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






    @if(app('request')->route()->action['as'] != 'usuarios.edit')
        <div class="row">
            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                {!! Form::label('password', 'Senha *') !!}
                {!! Form::password('password', ['class' => 'form-control', 'minlength' => 6, 'maxlength' => 20]) !!}
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                {!! Form::label('password_confirmation', 'Confirmação da senha *') !!}
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'minlength' => 6, 'maxlength' => 20]) !!}
            </div>
        </div>
    @endif

</fieldset>


    <script>
        function toggleConcedentes(){
            var perfil = $('#role_id option:selected').text();
                if(perfil === 'articulador'){
                    $('#concedentes-form').show();
				$("#entidade").select2({
					width: '100%' // need to override the changed default
				});
                }else{
                    $('#concedentes-form').hide();
                }
        }

    $( document ).ready(function() {
            toggleConcedentes();
    });
    </script>
