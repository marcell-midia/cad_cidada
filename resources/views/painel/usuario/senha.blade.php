@extends('layouts.panel') 
@section('title', 'Editar Credenciais')
@section('page-content')
    <div class="container">
	<div class="row">
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
				@endif
					</div>
                    @if(! auth()->user()->email_verified_at)
					<div class="alert alert-danger">
                        Por razões de segurança ao realizar o primeiro login é recomendado mudar a senha padrão
					</div>
                    @endif
<form action="{{route('confirmarsenha')}}" method="post">
                {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$usuario->id}}">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="card bg-light">
				<div class="card-body">
<form action="{{route('confirmarsenha')}}" method="post">
                {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$usuario->id}}">
    <div class="col-md-6 offset-md-3 text-center">
            <h3> Editar usuário</h3>
    <div class="form-group">
        <label for="nome">Nome</label>
        <input class="form-control" disabled autocomplete="off" type="text" name="nome" value="{{$usuario->name}}">
    </div>
    <div class="form-group">
        <label for="senha1">Nova Senha(mínimo 8 caracteres)</label>
		<input class="form-control" pattern=".{8,}" autocomplete="off" type="password" placeholder="Nova senha" id="senha1" name="senha1" value="">
	</div>
		<div class="form-group">
			<label for="senha2">Confirmar Senha</label>
			<input class="form-control" pattern=".{8,}" autocomplete="off" type="password" placeholder="Confirmação da senha" id="senha2" name="senha2" value="">
			<div id="divCheckPasswordMatch">
			</div>

		</div>
		<br>
		<input class="btn btn-sucess form-control" type="submit" id="enviar" name="enviar" value="Salvar Alterações">
</form>
				</div>
			</div>		
		</div>
@endsection

@section('scripts')
	function checkPasswordMatch() {
    var password = $("#senha1").val();
    var confirmPassword = $("#senha2").val();

    if (password != confirmPassword){

	$("#divCheckPasswordMatch").html("As senhas são diferentes");
    $('#enviar').prop("disabled",true);
    }else{
	$("#divCheckPasswordMatch").html("As senhas são iguais");
    $('#enviar').prop("disabled",false);
	}

	}

	$("#senha1").keyup(function (){
		checkPasswordMatch();
	
	});
	$("#senha2").keyup(function (){
		checkPasswordMatch();
	
	});

	$(document).ready(function () {
    $('#enviar').prop("disabled",true);
	});

@endsection
