@extends('layouts.authentication')

@section('title') Login @endsection

@section('content')
<style>
	.form-control:focus{
		color: #fff;
	}
	.form-control{
		color: #ddd;
	}
	#content{
		margin-top:0rem;
	}
	@media (min-width: 800px) 
	{
		#content{
			margin-top:3rem;
		}
	}
	</style>
	<div class="login-box card">
		<div class="card-body">
			<form class="form-horizontal form-material" id="loginform" action="{{ route('login') }}" method="post">
				<a href="javascript:void(0)" class="text-center db">
					<img src="{{ asset('images/logo-text.png') }}" alt="Home" />
				    <h2><b><font color="white">RENDA CIDADÃ EMERGENCIAL</font></b></h2></a>
				</a>

				@if( $errors->any() )
		            <div class="row">
		                <div class="col mt-3 mb-3">
		                    <div class="alert alert-danger">
		                        Preencha corretamente os campos abaixo: <br>
		                        @foreach( $errors->all() as $error )
		                            - {{ $error }}<br>
		                        @endforeach
		                    </div>
		                </div>
		            </div>
		        @endif

		        {{ csrf_field() }}

		        <div class="form-group">
		            <div class="col-xs-12">
		                <input id="cpf" type="text" class="form-control only-numbers" name="cpf" placeholder="CPF" value="{{ old('cpf') }}" required="" autofocus>
		            </div>
		        </div>

		        <div class="form-group">
		            <div class="col-xs-12">
		                <input id="password" type="password" class="form-control" name="password" placeholder="Senha" required="">
		            </div>
		        </div>

				<div class="form-group">
					<div class="col-md-12">
						<div class="checkbox checkbox-primary pull-left p-t-0">
								{{--  <input id="checkbox-signup" type="checkbox">
								<label for="checkbox-signup">Remember me</label>  --}}
						</div>
					</div>
				</div>
				<div class="form-group text-center m-t-20">
					<div class="col-xs-12">
						<button class="btn btn-block btn-outline-success" type="submit">Entrar</button>
			</form>
					</div>
				</div>
				<div class="form-group text-center m-t-20">
					<div class="col-xs-12">
						<form method="get" action="{{route('login')}}">
						</form>
					</div>
				</div>
                <div class="form-group text-center m-t-20">
					<div class="col-xs-12">
						<form method="get" action="{{route('login')}}">
							 <a href="{{route('consulta.index')}}" class="btn btn-block btn-outline-warning">Consulta Pública</a>
						</form>
					</div>
				</div>


				{{-- <div class="form-group m-b-0">
					<div class="col-sm-12 text-center">
						<p>Esqueceu a senha? <a href="#" class="text-primary m-l-5"><b>Clique aqui</b></a></p>
					</div>
				</div> --}}

		</div>
	</div>
@endsection
@section('script')

$('#cpf').mask('000.000.000-00', {reverse: true});
$("#loginform").submit(function() {
  $("#cpf").unmask();

});
@endsection
