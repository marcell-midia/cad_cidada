@extends('layouts.authentication')
@section('title') Cadastro @endsection

@section('content')
<br>
<br>
<br>
<div class="container">
    <div class="row text-center justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pré-Cadastro Realizado com Sucesso</div>

                <div class="card-body text-center">
				<h4 class="text-primary">Seu cadastro será validado por um administrador do sistema</h4>
				<p> Suas credenciais permanecem desativadas.<br> Depois da validação um e-mail será enviado para: <a href="mailto:{{$email}}"> {{$email}}</a> </p> 
			</div>
                <div class=" card-footer">
				<h6 class="text-secondary">Para mais informações, entre em contato com a administração do sistema em: <a href="mailto:emaildosistema">emaildosistema</a></h6>
					
				</div>
		</div>
	</div>
	</div>
</div>

@endsection
