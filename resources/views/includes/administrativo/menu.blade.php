<li>
    <a href="{{ route('dashboard') }}" aria-expanded="false">
        <i class="mdi mdi-gauge"></i><span class="hide-menu">Resumo </span>
    </a>
</li>
    @if(auth()->user()->has_role(['master','admin']))
<li>
    <a href="{{route('usuarios.index')}}" aria-expanded="false"><i class="mdi mdi-account-multiple-plus">
        </i><span class="hide-menu">Usuários</span>
    </a>
</li>
    @endif
<li>
    <a href="{{route('cidadas.index')}}" aria-expanded="false">
        <i class="mdi mdi-account"></i><span class="hide-menu">Cadastros</span>
    </a>
</li>
<li>
    <a class="text-primary" href="{{route('entregas21.index')}}" aria-expanded="false">
        <i class="mdi mdi-cart"></i><span class="hide-menu">Entregas 2021</span>
    </a>
</li>
<li>
    <a href="{{route('entregas.index')}}" aria-expanded="false">
        <i class="mdi mdi-calendar"></i><span class="disabled hide-menu">Entregas 2020</span>
    </a>
</li>
