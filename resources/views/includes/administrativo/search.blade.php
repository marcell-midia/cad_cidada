<li class="nav-item hidden-sm-down">
    {!! Form::open(['method' => 'get', 'role' => 'search', 'class' => 'app-search']) !!}
        {!! Form::text('search', request()->get('search'), ['class' => 'form-control', 'placeholder' => 'Buscar....','id' => 'search']) !!}

        @if( !empty(request()->get('per_page')) )
            {!! Form::hidden('per_page', request()->get('per_page')) !!}
        @endif
    {!! Form::close() !!}
</li>
