@extends('layouts.authentication')

@section('title') Resultado da Consulta @endsection

@section('content')
<style>
.form-control:focus{
    color: #fff;
}
.res{
    color: #fff;
    font-family:"Roboto";
}
#content{
    margin-top:0rem;
}
@media (min-width: 800px) 
{
    #content{
        margin-top:3rem;
    }
}
</style>
<div id="content" class="row">
    <div class="col-md-8 res col-sm-12 col-xs-12 col-lg-6 col-xl-8 offset-md-2 offset-lg-4 offset-xl-2">
	<div class="card bg-light" style="padding-top:2rem;">
            <h1 class="text-center ">Estado do Cadastro</h1>
            <hr>
            <h4 class=" text-center ">{{$cadastro->nome_benef_titular}} - <span class="cpf">{{$cadastro->num_cpf_titular}}</span></h4>
		<div class="card-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">

            @if($status == true)
            <h4 class="">
            <b>Você foi selecionado(a) pelo Governo do Estado do Amapá de acordo com as diretrizes da Lei nº 2540/2021 para receber um cartão do Programa Renda Cidadã Emergencial 2021

</b>
            </h4>

            <h5 class="">
            Compareça no dia <b>{{$cadastro->cronograma->format('d/m/Y')}}</b>, turno da <b>{{$cadastro->turno}}</b>, na <a target="_blank" data-toggle="tooltip" title="Google Maps" href="{{$cadastro->mapsEscola()}}"><b>{{$cadastro->escola}}</b></a>, {{$cadastro->end_escola}}, {{$cadastro->municipio}}-AP,  portando documento com foto (RG,Comprovante de Residência, Carteira de Trabalho Previdência Social ou Carteira de Motorista), para retirar seu cartão.

                <hr>
            </h5>

            @else
            <h4 class="">
            Informamos que este <b>cadastro não preenche os requisitos necessários</b> neste momento, para receber o RENDA CIDADÃ EMERGENCIAL.
</h4>
@endif
<a href="{{url('pdf/aviso_renda.pdf')}}"class=""><h5 class="btn btn-block btn-outline-info ">Instruções Gerais</h5></a>
                            <br>
                            <h5 class="text-center"><a class="btn btn-block btn-outline-success text-center" href="{{route('consulta.index')}}">Nova Consulta</a></h5>
	</div>
    </div>

</div>
</div>
@endsection
