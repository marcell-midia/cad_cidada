@extends('layouts.authentication')

@section('title') Consulta Pública @endsection

@section('content')
<style>
.form-control:focus{
    color: #fff;
}
.form-control{
    color: #ddd;
}
#content{
    margin-top:0rem;
}
</style>
<div id="content" class="row">
    <div class="col-md-8 col-sm-12 col-xs-12 col-lg-6 col-xl-8 offset-md-2 offset-lg-4 offset-xl-2">
	<div class="card">
		<div class="card-body">
			<form class="form-horizontal form-material" id="consultaform" action="{{ route('consulta.consulta') }}" method="post">
				<a href="{{route('login')}}" class="text-center db">
					<img src="{{ asset('images/logo-text.png') }}" alt="RENDA CIDADÃ EMERGENCIAL" />
				</p> <h2 class="text-center"><b><font color="white">Consulta Pública</font></b></h2></a>
				</a>

				@if( $errors->any() )
		            <div class="row">
		                <div class="col mt-3 mb-3">
		                    <div class="alert alert-danger">
		                        Preencha corretamente os campos abaixo: <br>
		                        @foreach( $errors->all() as $error )
		                            - {{ $error }}<br>
		                        @endforeach
		                    </div>
		                </div>
		            </div>
		        @endif

		        {{ csrf_field() }}

		        <div class="form-group">
		            <div class="col-xs-12">
                        <label class="text-center text-white" for="cpf">CPF</label>
		                <input id="cpf" type="text" class="form-control only-numbers" name="cpf" placeholder="CPF(11 dígitos, apenas números)" required="">
		            </div>
		        </div>
		        <div class="form-group">
		            <div class="col-xs-12">
                        <label class="text-center text-white" for="nascimento">Data de nascimento</label>
		                <input id="nascimento" type="text" placeholder="dd/mm/aaaa(apenas números)" class="form-control" name="nascimento" required="">
		            </div>
		        </div>

				<div class="form-group text-center m-t-20">
					<div class="col-12">
						<button class="btn btn-block btn-outline-success" type="submit">Consultar</button>
                        <a href="{{url('pdf/aviso_renda.pdf')}}"class="btn btn-block btn-outline-info">Instruções Gerais</a>
                        <a href="https://www.alelo.com.br/onde-aceita" class="btn btn-block btn-outline-info">Estabelecimentos Credenciados</a>
							<a href="{{route('login')}}"class="btn btn-block btn-outline-warning">Login: Atendentes</a>
			</form>
					</div>
				</div>

		</div>
	</div>
    </div>
</div>
@endsection
@section('script')

$('#cpf').mask('000.000.000-00', {reverse: true});
$('#nascimento').mask('00/00/0000');
$("#consultaform").submit(function() {
  $("#cpf").unmask();

});
@endsection
