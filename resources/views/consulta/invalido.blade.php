@extends('layouts.authentication')

@section('title') Resultado da Consulta @endsection

@section('content')
<style>
.form-control:focus{
    color: #fff;
}
.res{
    color: #ddd;
}
#content{
    margin-top:0rem;
}
@media (min-width: 800px) 
{
    #content{
        margin-top:3rem;
    }
}
</style>
<div id="content" class="row">
    <div class="col-md-8 res col-sm-12 col-xs-12 col-lg-6 col-xl-8 offset-md-2 offset-lg-4 offset-xl-2">
	<div class="card bg-light">
		<div class="card-body text-center">
            <h1 class="text-center ">Cadastro Não Encontrado</h1>
            <h4 class="text-center">Após cruzamento do CadÚnico com as bases de dados estaduais, com apoio do Ministério Público Estadual – MP/AP, informamos que este cadastro <b>não preenche os requisitos necessários previstos na Lei nº 2540/2021</b>, neste momento, para receber o RENDA CIDADÃ EMERGENCIAL. Evite aglomerações, busque informações sobre sua situação cadastral junto ao Centro de Referência de Assistência Social (CRAS), no seu município.</h4>
        <br>
        <h5 class="">CPF informado: <span class="cpf">{{$request->cpf}}</span> <br> Data de nascimento informada: {{$request->nascimento}}</h5>
        <hr>
        <a class="btn btn-block btn-outline-primary " href="javascript: history.go(-1)">Voltar</a>
        </div>
	</div>
    </div>
</div>
@endsection
