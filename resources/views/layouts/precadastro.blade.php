<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>@yield('title')</title>
	<link rel='stylesheet' href='{{ asset('css/bootstrap.css') }}'>
	<link rel='stylesheet' href='{{ asset('css/font-awesome/all.css') }}'>
	<link rel='stylesheet' href="{{ asset('css/precadastro.css') }}"></link>
</head>
<body>
	@yield('content')

	<script src="{{ asset('js/jquery-3.3.1.js') }}"></script>
    <script src="{{ asset('js/popper.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
	<script src="{{ asset('js/additional-methods.js') }}"></script>
	<script src="{{ asset('js/jquery.mask.js') }}"></script>
	<script>
		$(function () {
            $('.mask_cnpj').mask('00.000.000/0000-00', {reverse: true});
        });
	</script>
	@yield('js')
</body>
</html>
