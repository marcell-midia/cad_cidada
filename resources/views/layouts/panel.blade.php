<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>RENDA - CIDADÃ - @yield('title')</title>
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/chartist-js/dist/chartist-init.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/css-chart/css-chart.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/multiselect/css/multi-select.css') }}" rel="stylesheet">
    <link href="{{ asset('css/panel/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/panel/colors/blue.css') }}" id="theme" rel="stylesheet">
    <link href="{{ asset('css/panel/custom.css') }}" id="theme" rel="stylesheet">
    <link href="{{ asset('css/panel/wcoords.css') }}" id="theme" rel="stylesheet">
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/panel/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('js/panel/waves.js') }}"></script>
    <script src="{{ asset('js/panel/sidebarmenu.js') }}"></script>
    <script src="{{ asset('plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <script src="{{ asset('js/panel/custom.min.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/additional-methods.js') }}"></script>
    <script src="{{ asset('js/jquery.mask.js') }}"></script>
    <script src="{{ asset('plugins/chartist-js/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/dist/js/pt-BR.js') }}"></script>
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/switchery/dist/switchery.min.js') }}"></script>
    {{-- <script src="{{ asset('plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/panel/dashboard2.js') }}"></script> --}}
    <script src="{{ asset('plugins/styleswitcher/jQuery.style.switcher.js') }}"></script>
    <script src="{{ asset('plugins/multiselect/js/jquery.multi-select.js') }}"></script>

    <script src="{{ asset('js/panel/coords.js') }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border mini-sidebar">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ route('dashboard') }}">
                        <b>
                            {{-- <img src="{{ asset('images/logo-icon.png') }}" alt="homepage"  class="dark-logo" /> --}}
                            {{-- <img src="{{ asset('images/logo-light-icon.png') }}" alt="homepage" class="light-logo" /> --}}
                        </b>
                        <span>
                            <img src="{{ asset('images/logo-bar.png') }}" alt="homepage" class="dark-logo" /> <a> <h6><b>RENDA</b> | CIDADÃ</a></h6>

                        </span>
                    </a>
                </div>

                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <li class="nav-item">
                            <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-meunweb"></i></a>
                        </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        @yield('search-main')
                        <li class="nav-item dropdown">
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-text">
                                                <h4>{{ Auth::user()->name }}</h4>
                                                <p class="text-muted">{{ Auth::user()->login }}</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                            <a href="{{route('alterarsenha')}}" class="dropdown-item"><i class="ti-wallet"></i> Alterar Senha</a>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="javascript:;" onclick="event.preventDefault(); document.getElementById('form-logout-top').submit();"><i class="fa fa-sign-out"></i> Sair</a>
                                        <form id="form-logout-top" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            <input type="hiden" id="_token" name="_token" value="{{ csrf_token() }}">
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <aside class="left-sidebar">
            <div class="scroll-sidebar">
                <div class="user-profile">
                    <div class="profile-text"> <a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <div class="dropdown-menu animated flipInY">
                            <a href="{{route('alterarsenha')}}" class="dropdown-item"><i class="ti-wallet"></i> Alterar Senha</a>
                            <div class="dropdown-divider"></div>
							<form class="form-inline" method="post" action="{{route('logout')}}">
								@csrf
                            <a onclick="this.closest('form').submit();return false;" class="dropdown-item"><i class="fa fa-power-off"></i> Sair</a>
							</form>
                        </div>
                    </div>
                </div>
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        @include('includes.administrativo.menu')
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <!-- End Bottom points-->
        </aside>

        <div class="page-wrapper">
            <div class="container-fluid">
                @yield('page-titles')

                @yield('page-content')
            </div>

            <footer class="footer">
                © {{ date('Y') }} - {{env('APP_NAME')}}
            </footer>
        </div>
    </div>

    <script>
	 var SPMaskBehavior = function (val) {
		 return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	 },
		 spOptions = {
			 onKeyPress: function(val, e, field, options) {
				 field.mask(SPMaskBehavior.apply({}, arguments), options);
			 }
		 };
	 $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
	 $('.cpf').mask('000.000.000-00', {reverse: true});
     $('.cnae').mask('0000-0/00', {reverse: true});
     $('.telefone').mask(SPMaskBehavior,spOptions);
     $(".form").submit(function() {
		 $(".cnpj").unmask();
		 $(".cpf").unmask();
		 $(".telefone").unmask();
     });
     $(function () {
         $('[data-toggle="tooltip"]').tooltip();
     });
	 $(document).ready(function() {
		 $('.select2').select2({
             language: "pt-BR"
         });
		 $('.selectcard').select2({
             language: "pt-BR",
            minimumInputLength: 5
         });
		 $('.selectrecipiente').select2({
             language: "pt-BR",
            minimumInputLength: 11,
         @if(! isset($record))
              ajax: {
                url: '{{route('pessoaJson')}}',
                type: "GET",
                dataType: 'json',
                data: function (params) {
                  var query = {
                    cpf: params.term,
                  }
                  return query;
                },
              }
        @endif
         });
	 });
    @yield('scripts')
    </script>
</body>

</html>
