<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
	<title>RENDA - CIDADÃ - @yield('title')</title>
	<link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/panel/style.css') }}" rel="stylesheet">
	<link href="{{ asset('css/panel/colors/blue.css') }}" id="theme" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
	</div>
		
	<section id="wrapper" class="login-register login-sidebar"  style="background-image:url('{{ asset('images/background/login-register.svg') }}');">
		@yield('content')
	</section>

	<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap/js/popper.min.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/panel/jquery.slimscroll.js') }}"></script>
	<script src="{{ asset('js/panel/waves.js') }}"></script>
	<script src="{{ asset('js/jquery.mask.js') }}"></script>
	<script src="{{ asset('js/panel/sidebarmenu.js') }}"></script>
	<script src="{{ asset('plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
	<script src="{{ asset('js/panel/custom.min.js') }}"></script>
	<script src="{{ asset('plugins/styleswitcher/jQuery.style.switcher.js') }}"></script>
</body>
</html>

<script>
 @yield('script')
	 $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
	 $('.cpf').mask('000.000.000-00', {reverse: true});
     $('.cnae').mask('0000-0/00', {reverse: true});
     $('.telefone').mask(SPMaskBehavior,spOptions);
</script>
