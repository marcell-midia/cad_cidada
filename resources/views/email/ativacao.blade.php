@extends('layouts.precadastro')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 offset-md-2">
	<div class="text-center">
		<div class="card">
			<div class="card-header">
				<h2 class="text-primary">SISMINERA</h2>
			</div>
			<div class="card-body">
		<h4>Ativação de Cadastro</h4>
		<p>Seu cadastro foi homologado por um administrador do sistema. Seu login é o CPF cadastrado: <b class="text-primary"> {{$cpf}}</b></p>
		<hr>
			</div>

			<div class="card-footer">
		<a class="btn btn-success" href="{{route('login')}}">Entrar no Sistema</a>
			</div>
		</div>
	</div>

	</div>
</div>
@endsection
