@extends('layouts.precadastro')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 offset-md-2">
	<div class="text-center">
		<div class="card">
			<div class="card-header">
				<h2 class="text-primary">SISMINERA</h2>
			</div>
			<div class="card-body">
		<h4 class="text-danger">Inativação de Cadastro</h4>
		<p>Seu cadastro foi bloqueado por um administrador do sistema.<br> Para mais informações, entre em contato com a administração do sistema em: <a href="mailto:sisminera@ageamapa.ap.gov.br">sisminera@ageamapa.ap.gov.br</a>
		</p>
		<hr>
			</div>

		</div>
	</div>

	</div>
</div>
@endsection
