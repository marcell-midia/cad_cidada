		<h3>Novo Cadastro</h3>
		<p>Um novo cadastro foi criado no sistema</p>
		<h4>Informações</h4>
		<ol>
			<li>Nome: {{$user->name}}</li>
			<li>Cpf: {{$user->login}}</li>
			<li>CNPJ: {{$user->cnpj}}</li>
			<li>Email: {{$user->email}}</li>
		</ol>
		<hr>
		<a class="btn btn-success" href="{{route('usuarios.edit',['id' => $user->id])}}">Ativar esse cadastro</a>
