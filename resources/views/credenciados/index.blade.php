@extends('layouts.authentication')

@section('title') Consulta de Estabelecimentos @endsection

@section('content')
<style>
.form-control:focus{
    color: #fff;
}
.form-control{
    color: #ddd;
}
#content{
    margin-top:0rem;
}
</style>
<div id="content" class="row">
    <div class="col-md-8 col-sm-12 col-xs-12 col-lg-6 col-xl-8 offset-md-2 offset-lg-4 offset-xl-2">
	<div class="card">
		<div class="card-body">
			<form class="form-horizontal form-material" id="consultaform" action="{{ route('credenciados.lista') }}" method="get">
				<a href="{{route('login')}}" class="text-center db">
					<img src="{{ asset('images/logo-text.png') }}" alt="Home" />
				</p> <h2><b><font color="white">RENDA CIDADÃ | EMERGENCIAL </font></b></h2></a>
				</p> <h3 class="text-center"><b><font color="white">Estabelecimentos credenciados</font></b></h3></a>
				</a>

				@if( $errors->any() )
		            <div class="row">
		                <div class="col mt-3 mb-3">
		                    <div class="alert alert-danger">
		                        Preencha corretamente os campos abaixo: <br>
		                        @foreach( $errors->all() as $error )
		                            - {{ $error }}<br>
		                        @endforeach
		                    </div>
		                </div>
		            </div>
		        @endif

		        {{ csrf_field() }}

		        <div class="form-group">
		            <div class="col-xs-12">
                        <label class="text-center text-white" for="cidade">Cidade</label>
                        <select name="cidade" class="form-control">
                            @foreach($cidades as $cidade)
                                <option value={{$cidade}}>{{$cidade}}</option>
                            @endforeach
                        </select>
		            </div>
		        </div>
		        <div class="form-group">
		            <div class="col-xs-12">
                        <label class="text-center text-white" for="bairro">Termo de Pesquisa</label>
		                <input id="termo" type="text" placeholder="Opcional. Bairro ou nome do estabelecimento (sem acentos)" class="form-control" name="termo">
		            </div>
		        </div>

				<div class="form-group">
					<div class="col-md-12">
						<div class="checkbox checkbox-primary pull-left p-t-0">
								{{--  <input id="checkbox-signup" type="checkbox">
								<label for="checkbox-signup">Remember me</label>  --}}
						</div>
					</div>
				</div>
				<div class="form-group text-center m-t-20">
					<div class="col-12">
						<button class="btn btn-block btn-outline-success" type="submit">Pesquisar</button>
                        <a href="{{url('pdf/aviso_renda.pdf')}}"class="btn btn-block btn-outline-info">INSTRUÇÕES GERAIS</a>
							<a href="{{route('consulta.index')}}"class="btn btn-block btn-outline-info">Voltar para Consulta Pública</a>
			</form>
					</div>
				</div>
				{{-- <div class="form-group m-b-0">
					<div class="col-sm-12 text-center">
						<p>Esqueceu a senha? <a href="#" class="text-primary m-l-5"><b>Clique aqui</b></a></p>
					</div>
				</div> --}}

		</div>
	</div>
    </div>
</div>
@endsection
@section('script')

$('#cpf').mask('000.000.000-00', {reverse: true});
$('#nascimento').mask('00/00/0000');
$("#consultaform").submit(function() {
  $("#cpf").unmask();

});
@endsection
