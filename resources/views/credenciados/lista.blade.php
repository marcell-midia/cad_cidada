@extends('layouts.front')

@section('title') Consulta de Estabelecimentos @endsection

@section('content')
<style>
#lista_info{
    color:#fff !important;
}
#lista_filter{
    color:#fff !important;
}
</style>
<div id="content" class="row">
    <div class="col-md-12 col-lg-10 offset-lg-1">
        <hr>
        <h2 class="text-center text-light">LISTA DE ESTABELECIMENTOS EM:
</h2>
			<form id="consultaform" action="{{ route('credenciados.lista') }}" method="get">
                        <select onchange="this.form.submit()" name="cidade" class="form-control text-center">
                            @foreach($cidades as $cidade)
                                @if($cidade == request()->cidade)
                                <option selected value="{{$cidade}}">{{$cidade}}</option>
                                @else
                                <option value="{{$cidade}}">{{$cidade}}</option>
                                @endif
                            @endforeach
                        </select>
</form>

	<div class="card background-light">
		<div class="card-body text-light">
                    <table id="lista">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Bairro</th>
                                <th>Descrição</th>
                            </tr>
                        </thead>
                        <tbody class="text-dark">
                            @foreach($records as $record)
                                <tr>
                                    <td>{{ $record->nome_fantasia}}</td>
                                    <td>{{$record->bairro }}</td>
                                    <td>{{$record->descricao_mcc}}</td>
                                    
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
        <a class="btn text-center btn-inline btn-outline-primary " href="{{route('consulta.index')}}">Voltar Para Tela Inicial</a>
		</div>
	</div>
    </div>
</div>
@endsection
@section('script')

$(document).ready( function () {
    $('#lista').DataTable( {
     paging: false,
     scrollY: 400,
    "language": {
     "sEmptyTable": "Nenhum registro encontrado",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "_MENU_ resultados por página",
    "sLoadingRecords": "Carregando...",
    "sProcessing": "Processando...",
    "sZeroRecords": "Nenhum registro encontrado",
    "sSearch": "Pesquisar",
    "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
    },
    "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
    },
    "select": {
        "rows": {
            "_": "Selecionado %d linhas",
            "0": "Nenhuma linha selecionada",
            "1": "Selecionado 1 linha"
        }
    }
    }
    });
} );
@endsection
