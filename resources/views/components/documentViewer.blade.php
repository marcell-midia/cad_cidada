<script>
function loadIframe(url) {
    var $iframe = $('#viewer');
    if ($iframe.length) {
        $iframe.attr('src',url);
        return false;
    }
    return true;
}
</script>
<h2 class="text-center">Visualizar Documentos</h2>
        @foreach($documents as $document)
        @if($loop->index == 0)
        <button type="button" class="btn btn-sm btn-primary" onclick="loadIframe('data:{{$document->tipo}};base64,{{$document->arquivo}}')">{{$document->descricao}}</button>
        @else
        <button type="button" class="btn btn-sm btn-primary" onclick="loadIframe('data:{{$document->tipo}};base64,{{$document->arquivo}}')">{{$document->descricao}}</button>
        @endif
        @endforeach
<hr>
        <iframe id="viewer" width='100%' height='{{$height}}rem' src='data:{{$documents[0]->tipo}};base64,{{$documents[0]->arquivo}}'></iframe>